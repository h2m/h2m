# Heuristics for Heterogeneous Memory (H2M) - Runtime Library & Examples

## 1. Description

tbd.

### Funding

Joint DFG-ANR funding from 2021 to 2024
- DFG: https://gepris.dfg.de/gepris/projekt/446185093
- ANR: https://anr.fr/Project-ANR-20-CE92-0022

### Further information

**Website:**

https://h2m.gitlabpages.inria.fr/

**GitHub / GitLab:**

https://gitlab.inria.fr/h2m/h2m

## 2. Publications / How to Cite

- J. Klinkenberg, C. Foyer, P. Clouzet, B. Goglin, E. Jeannot, C.Terboven and A. Kozhokanova, **Phase-Based Data Placement Optimization in Heterogeneous Memory**, 2024 IEEE International Conference on Cluster Computing (CLUSTER), Kobe, Japan, 2024, pp. 382-393, https://doi.org/10.1109/CLUSTER59578.2024.00040
- J. Klinkenberg, A. Kozhokanova, C. Terboven, C. Foyer, B. Goglin, E. Jeannot, **H2M: Exploiting Heterogeneous Shared Memory Architectures**, Future Generation Computer Systems, Volume 148, 2023, Pages 39-55, ISSN 0167-739X, https://doi.org/10.1016/j.future.2023.05.019
- C. Foyer, B. Goglin, E. Jeannot, J. Klinkenberg, A. Kozhokanova and C. Terboven, **H2M: Towards Heuristics for Heterogeneous Memory**, 2022 IEEE International Conference on Cluster Computing (CLUSTER), Heidelberg, Germany, 2022, pp. 498-499, doi: 10.1109/CLUSTER51413.2022.00060.

## 3. Building and Installing H2M

H2M can be build from source using CMake.

### 3.1. Prerequisites

* CMake version 2.8.12 or greater
* C++ compiler supporting the C++11 standard and OpenMP
* Independent of the chosen C++ compiler, additionally a GNU compiler with version >= 4.9.0
  * regex which is used in parts of the code has first been implemented in version 4.9.0
  * Some systems have an older default GNU compiler versions installed
* hwloc 2.7.0 or higher
* libnuma

### 3.2. Getting the source 

```bash
git clone https://gitlab.inria.fr/h2m/h2m.git
```

### 3.3. Build and Installation

To build H2M using CMake execute the following:
* Create build directory
```bash
mkdir -p INSTALL
mkdir -p BUILD && cd BUILD
```
* Run CMake to configure the build
* Note: Installing to the default installation path usually requires root privileges. Changing the installation path can be done using `-DCMAKE_INSTALL_PREFIX`
```bash
cmake -DCMAKE_INSTALL_PREFIX=<install/path> ..
# Example:
cmake -DCMAKE_INSTALL_PREFIX=$(pwd)/../INSTALL ..
```
* For a list of available parameters
```bash
cmake .. -L
```
* To configure build parameters with a graphical interface use
```bash
ccmake ..
```
* To build and install run the following
```bash
make
make install
```
