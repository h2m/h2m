prefix=${CMAKE_INSTALL_PREFIX}
includedir=${PKG_CONFIG_INCLUDEDIR}
libdir=${PKG_CONFIG_LIBDIR}

Name: ${H2M_PHASE_NAME}
Description: ${PROJECT_DESCRIPTION}
Version: ${H2M_VERSION}
Requires: ${PKG_CONFIG_REQUIRES}
Requires.private: hwloc
Cflags: ${PKG_CONFIG_CFLAGS}
Libs: ${PKG_CONFIG_LIBS} -l${H2M_PHASE_NAME}
Libs.require: -lhwloc
