find_package(Threads REQUIRED)
if (NOT "${CMAKE_CXX_FLAGS}" MATCHES "\\-pthread")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread")
endif ()