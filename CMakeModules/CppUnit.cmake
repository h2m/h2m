include(FindPackageHandleStandardArgs)

find_path(CPPUNIT_ROOT_DIR
  NAMES include/cppunit/BriefTestProgressListener.h include/cppunit/CompilerOutputter.h
    include/cppunit/extensions/HelperMacros.h include/cppunit/extensions/TestFactoryRegistry.h
    include/cppunit/TestFixture.h include/cppunit/TestResultCollector.h include/cppunit/TestResult.h
    include/cppunit/TestRunner.h
  PATHS ENV CPPUNIT_ROOT
  DOC "CppUnit root path")

find_path(CPPUNIT_INCLUDE_DIR
  NAMES cppunit/BriefTestProgressListener.h cppunit/CompilerOutputter.h
    cppunit/extensions/HelperMacros.h cppunit/extensions/TestFactoryRegistry.h
    cppunit/TestFixture.h cppunit/TestResultCollector.h cppunit/TestResult.h
    cppunit/TestRunner.h
  HINTS ${CPPUNIT_ROOT_DIR}
  PATH_SUFFIXES include
  DOC "CppUnit include path")

find_library(CPPUNIT_LIBRARY
  NAMES cppunit
  HINTS ${CPPUNIT_ROOT_DIR}
  PATH_SUFFIXES lib
  DOC "CppUnit library file")

get_filename_component(CPPUNIT_LIBRARY_DIR ${CPPUNIT_LIBRARY} PATH)

find_package_handle_standard_args(CPPUNIT REQUIRED_VARS CPPUNIT_INCLUDE_DIR CPPUNIT_LIBRARY_DIR CPPUNIT_LIBRARY CPPUNIT_ROOT_DIR)

include_directories(SYSTEM ${CPPUNIT_INCLUDE_DIR})
