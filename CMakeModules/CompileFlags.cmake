# compiler specific additions
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
    # using Clang
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    # using GCC
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
    # using Intel
    if (NOT "${CMAKE_C_FLAGS}" MATCHES "\\-Wno-unknown-pragmas")
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wno-unknown-pragmas" CACHE STRING "" FORCE)
    endif ()
    if (NOT "${CMAKE_CXX_FLAGS}" MATCHES "\\-Wno-unknown-pragmas")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-unknown-pragmas" CACHE STRING "" FORCE)
    endif ()
    if (NOT "${CMAKE_Fortran_FLAGS}" MATCHES "\\-Wno-unknown-pragmas")
        set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -Wno-unknown-pragmas" CACHE STRING "" FORCE)
    endif ()
endif()