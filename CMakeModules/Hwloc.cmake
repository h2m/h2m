find_path(
    HWLOC_PREFIX
    NAMES include/hwloc.h
)

if (NOT HWLOC_PREFIX AND NOT $ENV{HWLOC_BASE} STREQUAL "")
    set(HWLOC_PREFIX $ENV{HWLOC_BASE})
endif()

find_path(
    HWLOC_LIBRARY_DIRS
    NAMES libhwloc.so
    HINTS ${HWLOC_PREFIX}/lib
)

find_library(
    HWLOC_LIBRARIES
    NAMES hwloc
    HINTS ${HWLOC_LIBRARY_DIRS}
)

find_path(
    HWLOC_INCLUDE_DIRS
    NAMES hwloc.h
    HINTS ${HWLOC_PREFIX}/include
)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
    HWLOC DEFAULT_MSG
    HWLOC_LIBRARIES
    HWLOC_INCLUDE_DIRS
)

if (HWLOC_FOUND)
    message(STATUS "HWLOC include dir:  " ${HWLOC_INCLUDE_DIRS})
    message(STATUS "HWLOC lib dir:      " ${HWLOC_LIBRARY_DIRS})
    message(STATUS "HWLOC libraries:    " ${HWLOC_LIBRARIES})

    if (NOT "${CMAKE_C_FLAGS}" MATCHES "\\${HWLOC_LIBRARY_DIRS}")
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -L${HWLOC_LIBRARY_DIRS}" CACHE STRING "" FORCE)
    endif ()
    if (NOT "${CMAKE_CXX_FLAGS}" MATCHES "\\${HWLOC_LIBRARY_DIRS}")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -L${HWLOC_LIBRARY_DIRS}" CACHE STRING "" FORCE)
    endif ()
    if (NOT "${CMAKE_Fortran_FLAGS}" MATCHES "\\${HWLOC_LIBRARY_DIRS}")
        set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -L${HWLOC_LIBRARY_DIRS}" CACHE STRING "" FORCE)
    endif ()
endif()
