# Preliminary H2M Library Documentation

## 1. H2M API

### 1.1 Functions with allocation traits

Some function have an parameter of type `const h2m_alloc_trait_t traits[]`. The data type `h2m_alloc_trait_t` consists of a **key** (`h2m_alloc_trait_key_t`) and a **value** (numeric or `h2m_alloc_trait_value_t`).

If not otherwise specifed, the semantics is as follows:
* Trait values can be combined with logical OR which makes sense for some keys. Example: An allocation / data item could be sensitive to both bandwidth and latency. The value could there be `(h2m_atv_bw_sensitive | h2m_atv_lat_sensitive)`.
* If the array contains multiple entries with the same key then the last entry will be considered to take effect.
* There might be keys for which it makes sense to have multiple key entries with different values to prioritize as a logical combination with OR does not provide that information. For those keys the semantics will be documented here.
* To reach and sustain high performance, it is the responsibility of the user / programmer to specify traits that do not lead to contradicting goals. 
  * Example:
    * `h2m_atk_sensitivity` = `h2m_atv_bw_sensitive`
    * `h2m_atk_access_mode` = `h2m_atv_access_mode_writeonly`
    * `h2m_atk_req_mem_space` = `h2m_atv_mem_space_large_cap`
  * The first two traits would fit better to DRAM or HBM whereas the last trait requests large capacity memory space which might deliver poor performance

## 2. H2M Strategy Types and Allocation Traits

A description of H2M strategy types and a list of available allocation traits can be found [here](allocation-traits.md)

## 3. H2M Environment variables

A list of all environment variables including descriptions and default values can be found [here](environment-variables.md)

