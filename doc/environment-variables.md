# H2M - Environment Variables

The following environment variables can be used to alter the behavior of the library implementation.

| Variable  | Description  | Allowed Values  | Default |
|-----------|--------------|-----------------|---------|
| `H2M_VERBOSITY_LVL` | Specifies verbosity level for debug prints. Only applicable if compiled with `-DH2M_DEBUG`. | Integer | 10 |
| `H2M_PRINT_CONFIG_VALUES` | Specifies whether to print configuration values. | 0 or 1 | 0 |
| `H2M_PRINT_MEM_CHARACTERISTICS` | Specifies whether to print memory characteristics matrices. | 0 or 1 | 0 |
| `H2M_PRINT_AFFINITY_MASKS` | Specifies whether to print affinity masks for threads during thread_init. | 0 or 1 | 0 |
| `H2M_PRINT_STATISTICS` | Specifies whether to print statistics during `h2m_finalize`. | 0 or 1 | 0 |
| `H2M_RANK` | Option to specify the rank in a hybrid application run. Used for logging and statistics output. Automatically detected if environment variables `PMI_RANK` or `OMPI_COMM_WORLD_RANK` are set. | Integer | 0 |
| `H2M_OMP_NUM_THREADS` | Internal variable to identify with how many OpenMP threads an application is executed. It will read the value from the original `OMP_NUM_THREADS` environment variable. | - | 1 |
| `H2M_MIGRATION_ENABLED` | Controls whether data migration between memory spaces is enabled or disabled | 0 or 1 | 1 |
| `H2M_NUM_MIGRATION_THREADS` | Specifies the number of threads to be used to speedup memory migrations. Each data item can only be migrated by a single thread. | Integer | 1 |
| `H2M_BACKGROUND_THREAD_ENABLED` | Controls whether background thread is enabled to handle async data migration requests | 0 or 1 | 1 |
| `H2M_BACKGROUND_THREAD_PIN_MODE` | Controls where the background thread will be pinned. 0=pin thread to last core in proccess cpuset; 1=reset mask to complete machine; 2=pin to a specific core (see also `H2M_BACKGROUND_THREAD_PIN_CORE` ) | 0, 1 or 2 | 0 |
| `H2M_BACKGROUND_THREAD_PIN_CORE` | Specifies core where background thread will be pinned. Only active if `H2M_BACKGROUND_THREAD_PIN_MODE=2` | Integer | 0 |
| `H2M_FORCED_ALLOC_MEM_SPACE` | Option to overwrite in which memory space all data will be allocated | "HBW", "LOW_LAT" or "LARGE_CAP" | `NULL` |
| `H2M_DEFAULT_MEM_SPACE` | Option to overwrite default memory space to be used for various purposes, e.g., if no traits are specified. Otherwise it is HBW | "HBW", "LOW_LAT" or "LARGE_CAP" | `NULL` |
| `H2M_NODES_HBW` | Option to specify a comma-separated list of NUMA nodes that should be used for high bandwidth memory space. Otherwise the library will automatically identify HBW memory NUMA nodes using Hwloc. | String (e.g., "0,1,2") | `NULL` |
| `H2M_NODES_LARGE_CAP` | Option to specify a comma-separated list of NUMA nodes that should be used for large capacity memory space. Otherwise the library will automatically identify LARGE_CAP memory NUMA nodes using Hwloc. | String (e.g., "0,1,2") | `NULL` |
| `H2M_NODES_LOW_LAT` | Option to specify a comma-separated list of NUMA nodes that should be used for low latency memory space. Otherwise the library will automatically identify LOW_LAT memory NUMA nodes using Hwloc. | String (e.g., "0,1,2") | `NULL` |
| `H2M_MAX_MEM_CAP_OVERALL_MB_HBW` | Specifies artificatial memory capacity limit in MB for all HBW NUMA nodes. | Integer | `INT_MAX` |
| `H2M_MAX_MEM_CAP_OVERALL_MB_LARGE_CAP` | Specifies artificatial memory capacity limit in MB for all LARGE_CAP NUMA nodes. | Integer | `INT_MAX` |
| `H2M_MAX_MEM_CAP_OVERALL_MB_LOW_LAT` | Specifies artificatial memory capacity limit in MB for all LOW_LAT NUMA nodes. | Integer | `INT_MAX` |
| `H2M_TOOL_LIBRARIES` | Specifies a full path to an external tool (that is based on the tools interface), e.g., a `tool.so`. If H2M has been compiled with tool support (`-DTOOL_SUPPORT=1`) registered callbacks will be triggered. | String | `NULL` |
| `H2M_JSON_TRAIT_FILE` | Specifies the path to a JSON file containing allocations traits per phase and allocation. Allocation abstraction can then leverage this content. This avoids heavy code refactoring and modifications. | String  | `NULL` |
| `H2M_DUMP_DIR` | Specifies directory where information about phase transitions is dumped after the application has finished. This information can be used in a post-processing step e.g. for recommending traits or optimizing data placement. | String  | `NULL` |
| `H2M_MAX_PHASE_TRANSITIONS` | Specifies the number of maximum phase transitions an application can have. | Integer  | 10240 |
| `H2M_LOOKUP_DIR` | Specifies directory that contains memory characteristic files that will be loaded at initialization and can be used within data migration strategies | String  | `NULL` |
| `H2M_STATS_FILE_PREFIX` | Specifies the location for a statistics file instead of writing stats to stdout or stderr | String | `NULL` |
| `H2M_THREAD_SLEEP_TIME_MICRO_SECS` | Specifies how many micro seconds background threads in the H2M runtime will sleep after each iteration to avoid spamming locks or atomic variables. | Integer | 2 |
