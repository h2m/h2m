# H2M - Strategies and Allocation Traits

The following allocation traits can be used by strategies/heuristics to influence where data is allocated or moved.
There are *prescriptive* traits that directly correspond to the members of the placement decision (`h2m_placement_decision_t`)
and *descriptive* traits that can be leveraged by different strategies in H2M.
Currently, there are the following three strategy types in H2M:
- **Allocation strategies** decide where to place memory considering the specified traits as well as the current capacity limits. This base strategy type can be used in different scenarios such as in a direct memory allocation.
- **Commit strategies** are responsible for data placement decisions for a set of allocations. Sometimes, the application would like to allocate multiple buffers with different access characteristics or requirements while not all fit into fast memory. This strategy type is responsible for determining the order of pyhisical allocation considering the specified traits (i.e. important allocations go first). Moreover, it can internally make use of an *allocation strategy*.
- **Mirgrations strategies** determine whether existing already allocated data objects need to moved between memory tiers. This can be useful if an application consists of multiple application phases that exhibit vastly different memory accesses or accesses to different data objects. The H2M API contains a function call that enables the programmer or libraries to update traits for existing data objects (see `h2m_update_traits`).

**Note:** The following traits are presenting possible key-value pairs. However, you need to provide a corresponding strategy that uses the specified key-value pairs for decision making. The built-in default allocation strategy only respect prescriptive traits to guide data placement.

| Trait Key | Type | Description | Allowed Values |
|-----------|------|-------------|----------------|
| `h2m_atk_req_mem_space` | prescriptive | Specifies a *required* memory space to be used for the allocation. If the allocation cannot be performed due to insufficient capacity it will return `NULL` and an error code | `h2m_atv_mem_space_hbw`<br>`h2m_atv_mem_space_low_lat`<br>`h2m_atv_mem_space_large_cap` |
| `h2m_atk_pref_mem_space` | prescriptive | Specifies a *preferred* memory space to be used for the allocation. If the allocation cannot be performed due to insufficient capacity it will fallback to the large capacity memory space `h2m_atv_mem_space_large_cap` | `h2m_atv_mem_space_hbw`<br>`h2m_atv_mem_space_low_lat`<br>`h2m_atv_mem_space_large_cap` |
| `h2m_atk_mem_alignment` | prescriptive | Desired memory alignment to be used for the allocation. Default is `getpagesize()` | Values of type `size_t` |
| `h2m_atk_mem_partition` | prescriptive | **Experimental**: Tells how to partition the memory across NUMA nodes for the desired memory kind. Default is `h2m_atv_mem_partition_first_touch`. Can be combined with `h2m_atk_mem_blocksize_kb` | `h2m_atv_mem_partition_first_touch`<br>`h2m_atv_mem_partition_close`<br>`h2m_atv_mem_partition_interleaved`<br>`h2m_atv_mem_partition_blocked` |
| `h2m_atk_mem_blocksize_kb` | prescriptive | Block size in KB that should be used if memory partition is set to `h2m_atv_mem_partition_blocked` | Values of type `size_t` |
| `h2m_atk_sensitivity` | descriptive | Describes whether an allocation will be sensitive to bandwidth, latency or both (can be combined with logical OR) | `h2m_atv_bw_sensitive`<br>`h2m_atv_lat_sensitive` |
| `h2m_atk_access_mode` | descriptive | Tells how data will be accessed, e.g. reading or writing | `h2m_atv_access_mode_readonly`<br>`h2m_atv_access_mode_writeonly`<br>`h2m_atv_access_mode_readwrite` |
| `h2m_atk_access_origin` | descriptive | Describes whether data is accessed e.g. by multiple threads or not | `h2m_atv_access_origin_single_threaded`<br>`h2m_atv_access_origin_multi_threaded`<br>`h2m_atv_access_origin_multi_threaded_different_nodes` |
| `h2m_atk_access_freq` | descriptive | Can be used to specify how frequent data is accessed | Any value type (e.g. `int` or `double`) or<br>`h2m_atv_access_freq_low`<br>`h2m_atv_access_freq_medium`<br>`h2m_atv_access_freq_high` |
| `h2m_atk_access_pattern` | descriptive | Specifies the predominant access pattern for that data item. If accesses are strided, can be combined with `h2m_atk_access_stride` | `h2m_atv_access_pattern_linear`<br>`h2m_atv_access_pattern_strided`<br>`h2m_atv_access_pattern_random` |
| `h2m_atk_access_stride` | descriptive | Describes the stride if used with `h2m_atv_access_pattern_strided` | Values of type `int` |
| `h2m_atk_access_prio` | descriptive | Can be used to express the priority or importance of the corresponding allocation | Any value type (e.g. `int` or `double`) |
| `h2m_atk_structure_type` | descriptive | **Experimental:** Specifies what data structure is represented by the allocation. Can indicate how data is used. | `h2m_atv_structure_type_array`<br>`h2m_atv_structure_type_matrix`<br>`h2m_atv_structure_type_tree` |
| `h2m_atk_custom1` | descriptive | Custom field that can be used with arbitrary custom information | Any value |
| `h2m_atk_custom2` | descriptive | Custom field that can be used with arbitrary custom information | Any value |
| `h2m_atk_custom3` | descriptive | Custom field that can be used with arbitrary custom information | Any value |
| `h2m_atk_custom4` | descriptive | Custom field that can be used with arbitrary custom information | Any value |
| `h2m_atk_custom5` | descriptive | Custom field that can be used with arbitrary custom information | Any value |
| `h2m_atk_custom6` | descriptive | Custom field that can be used with arbitrary custom information | Any value |
