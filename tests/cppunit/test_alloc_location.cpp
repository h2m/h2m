#include "test_common.h"
#include "test_alloc_location.h"
#include "h2m.h"
#include "h2m_utils_numa.h"
#include <numa.h>
#include <omp.h>
#include <bitset>
#include <vector>

CPPUNIT_TEST_SUITE_REGISTRATION (test_alloc_location);

void test_alloc_location :: setUp (void) {
    h2m_init();
}

void test_alloc_location :: tearDown (void) {
    h2m_finalize();
}

void test_alloc_location :: alloc_on_single_node (void) {
    // variables
    int err         = H2M_SUCCESS;
    double* ptr     = nullptr;
    size_t arr_size = sizeof(double) * 10000000;
    unsigned long nodemask;
    int n_nodes;

    h2m_alloc_trait_value_t mem_spaces[3] = {
        h2m_atv_mem_space_hbw,
        h2m_atv_mem_space_large_cap,
        h2m_atv_mem_space_low_lat
    };

    int n_all_nodes = numa_num_configured_nodes();
    struct bitmask *cpu_mask_empty = numa_allocate_cpumask();
    cpu_mask_empty = numa_bitmask_clearall(cpu_mask_empty);

    #if PRINT_MASKS || DBG_PRINTS
    fprintf(stderr, "\n");
    #endif

    #if PRINT_MASKS
    fprintf(stderr, "  Original affinity mask for thread: ");
    get_and_print_affinity_mask();
    #endif // PRINT_MASKS

    for(int i = 0; i < n_all_nodes; i++) {
        // set affinity mask of current thread to corresponding core on NUMA node (bind to that core)
        struct bitmask *cpu_bit_mask = numa_allocate_cpumask();
        numa_node_to_cpus(i, cpu_bit_mask);
        // check whether bitmask is empty (memory only NUMA domain)
        if(numa_bitmask_equal(cpu_bit_mask, cpu_mask_empty)) {
            #if DBG_PRINTS
            fprintf(stderr, "  Node %d is memory only -- Skipping\n", i);
            #endif // DBG_PRINTS
            continue;
        } else {
            #if DBG_PRINTS
            fprintf(stderr, "  Thread pinned to NUMA node %d\n", i);
            #endif // DBG_PRINTS
        }
        
        cpu_set_t node_cpu_set = get_cpuset_for_bitmask(cpu_bit_mask);
        numa_free_cpumask(cpu_bit_mask);
        if (sched_setaffinity(gettid(), sizeof(cpu_set_t), &node_cpu_set) == -1) {
            perror("sched_setaffinity");
        }
        
        #if PRINT_MASKS
        fprintf(stderr, "  Affinity mask for thread on NUMA node %d: ", i);
        get_and_print_affinity_mask();
        #endif // PRINT_MASKS

        for(int j = 0; j < 3; j++) {
            // define traits to be used
            h2m_alloc_trait_value_t ms = mem_spaces[j];
            h2m_alloc_trait_t tmp_traits[3] = {
                h2m_atk_req_mem_space, ms, 
                h2m_atk_mem_alignment, 4096,
                h2m_atk_mem_partition, h2m_atv_mem_partition_close
            };

            // allocate data now
            ptr = (double*) h2m_alloc_w_traits(arr_size, &err, 3, tmp_traits);
            if (err != H2M_SUCCESS) assert_h2m_err("h2m_alloc_w_traits", err);
            memset(ptr, 0, arr_size); // and init data with zeros
            
            // validation placement of physical pages
            int* nodes  = h2m_get_nodes_for_mem_space(ms, &n_nodes);
            nodemask    = 1 << nodes[0]; // select the closest node here
            err         = verify_page_locations((void**)&ptr, 1, arr_size, nodemask, "h2m_alloc_w_traits", 1);
            if (err != H2M_SUCCESS) assert_h2m_err("h2m_alloc_w_traits:verify_page_locations", err);
            
            // cleanup
            err = h2m_free(ptr);
            if (err != H2M_SUCCESS) assert_h2m_err("h2m_free", err);
        }
    }
}

void test_alloc_location :: alloc_interleaved (void) {
    // variables
    int err         = H2M_SUCCESS;
    double* ptr     = nullptr;
    size_t arr_size = sizeof(double) * 10000000;
    unsigned long nodemask;
    int n_nodes;

    h2m_alloc_trait_value_t mem_spaces[3] = {
        h2m_atv_mem_space_hbw,
        h2m_atv_mem_space_large_cap,
        h2m_atv_mem_space_low_lat
    };

    int n_all_nodes = numa_num_configured_nodes();
    struct bitmask *cpu_mask_empty = numa_allocate_cpumask();
    cpu_mask_empty = numa_bitmask_clearall(cpu_mask_empty);

    #if PRINT_MASKS || DBG_PRINTS
    fprintf(stderr, "\n");
    #endif

    #if PRINT_MASKS
    fprintf(stderr, "  Original affinity mask for thread: ");
    get_and_print_affinity_mask();
    #endif // PRINT_MASKS

    for(int i = 0; i < n_all_nodes; i++) {
        // set affinity mask of current thread to corresponding core on NUMA node (bind to that core)
        struct bitmask *cpu_bit_mask = numa_allocate_cpumask();
        numa_node_to_cpus(i, cpu_bit_mask);
        if(numa_bitmask_equal(cpu_bit_mask, cpu_mask_empty)) {
            #if DBG_PRINTS
            fprintf(stderr, "  Node %d is memory only -- Skipping\n", i);
            #endif // DBG_PRINTS
            continue;
        } else {
            #if DBG_PRINTS
            fprintf(stderr, "  Thread pinned to NUMA node %d\n", i);
            #endif // DBG_PRINTS
        }

        cpu_set_t node_cpu_set = get_cpuset_for_bitmask(cpu_bit_mask);
        numa_free_cpumask(cpu_bit_mask);
        if (sched_setaffinity(gettid(), sizeof(cpu_set_t), &node_cpu_set) == -1) {
            perror("sched_setaffinity");
        }
        
        #if PRINT_MASKS
        fprintf(stderr, "  Affinity mask for thread on NUMA node %d: ", i);
        get_and_print_affinity_mask();
        #endif // PRINT_MASKS

        for(int j = 0; j < 3; j++) {
            // define traits to be used
            h2m_alloc_trait_value_t ms = mem_spaces[j];
            h2m_alloc_trait_t tmp_traits[3] = {
                h2m_atk_req_mem_space, ms, 
                h2m_atk_mem_alignment, 4096,
                h2m_atk_mem_partition, h2m_atv_mem_partition_interleaved
            };

            // allocate data now
            std::map<int, size_t> self_usage_before = get_self_numa_memory_usage();
            ptr = (double*) h2m_alloc_w_traits(arr_size, &err, 3, tmp_traits);
            if (err != H2M_SUCCESS) assert_h2m_err("h2m_alloc_w_traits", err);
            memset(ptr, 0, arr_size); // and init data with zeros
            std::map<int, size_t> self_usage_after = get_self_numa_memory_usage();
            
            // validation placement of physical pages
            int* nodes  = h2m_get_nodes_for_mem_space(ms, &n_nodes);
            nodemask    = h2m_get_nodemask_for_mem_space(ms);
            err         = verify_page_locations((void**)&ptr, 1, arr_size, nodemask, "h2m_alloc_w_traits", 1);
            if (err != H2M_SUCCESS) assert_h2m_err("h2m_alloc_w_traits:verify_page_locations", err);

            // verify that amount of pages is interleaved in an acceptable form
            size_t exp_min = (arr_size / n_nodes) * 0.99;
            size_t exp_max = (arr_size / n_nodes) * 1.01;
            for(int k = 0; k < n_nodes; k++) {
                int tmp_used = self_usage_after[nodes[k]] - self_usage_before[nodes[k]];
                if(tmp_used < exp_min || tmp_used > exp_max) {
                    std::stringstream ss;
                    ss << "Data not nicely interleaved for nodemask " << nodemask << ". Here: node=" << nodes[k] << " Expected: min=" << exp_min << " - max=" << exp_max << " -> Actual:" << tmp_used;
                    CPPUNIT_FAIL(ss.str());
                }
            }
            
            // cleanup
            err = h2m_free(ptr);
            if (err != H2M_SUCCESS) assert_h2m_err("h2m_free", err);
        }
    }
}

void test_alloc_location :: alloc_first_touch (void) {
    // variables
    int err         = H2M_SUCCESS;
    double* ptr     = nullptr;
    size_t arr_size = 10000000;
    size_t buf_size = sizeof(double) * arr_size;
    unsigned long nodemask;
    int n_nodes;

    h2m_alloc_trait_value_t mem_spaces[3] = {
        h2m_atv_mem_space_hbw,
        h2m_atv_mem_space_large_cap,
        h2m_atv_mem_space_low_lat
    };

    #if PRINT_MASKS || DBG_PRINTS
    fprintf(stderr, "\n");
    #endif

    #if PRINT_MASKS
    fprintf(stderr, "  Original affinity mask for thread: ");
    get_and_print_affinity_mask();
    #endif // PRINT_MASKS

    // set affinity mask of current (master OpenMP) thread to core 0 as I was messing with cpusets before already
    struct bitmask *cpu_bit_mask = numa_allocate_cpumask();
    cpu_bit_mask = numa_bitmask_setbit(cpu_bit_mask, 0);
    cpu_set_t cpu_set = get_cpuset_for_bitmask(cpu_bit_mask);
    numa_free_cpumask(cpu_bit_mask);
    if (sched_setaffinity(gettid(), sizeof(cpu_set_t), &cpu_set) == -1) {
        perror("sched_setaffinity");
    }
        
    #if PRINT_MASKS
    fprintf(stderr, "  Affinity mask for thread ");
    get_and_print_affinity_mask();
    #endif // PRINT_MASKS

    for(int j = 0; j < 3; j++) {
        // define traits to be used
        h2m_alloc_trait_value_t ms = mem_spaces[j];
        h2m_alloc_trait_t tmp_traits[3] = {
            h2m_atk_req_mem_space, ms, 
            h2m_atk_mem_alignment, 4096,
            h2m_atk_mem_partition, h2m_atv_mem_partition_first_touch
        };

        // allocate data now
        std::map<int, size_t> self_usage_before = get_self_numa_memory_usage();
        ptr = (double*) h2m_alloc_w_traits(buf_size, &err, 3, tmp_traits);
        if (err != H2M_SUCCESS) assert_h2m_err("h2m_alloc_w_traits", err);

        // parallel init to test first touch
        #pragma omp parallel
        {
            #if PRINT_MASKS
            #pragma omp critical
            {
                fprintf(stderr, "  Affinity mask for thread %d: ", omp_get_thread_num());
                get_and_print_affinity_mask();
            }
            #endif // PRINT_MASKS

            #pragma omp for schedule(static)
            for(int i = 0; i < arr_size; i++) {
                ptr[i] = 0.42;
            }
        }

        std::map<int, size_t> self_usage_after = get_self_numa_memory_usage();
        
        // validation placement of physical pages
        int* nodes  = h2m_get_nodes_for_mem_space(ms, &n_nodes);
        nodemask    = h2m_get_nodemask_for_mem_space(ms);
        err         = verify_page_locations((void**)&ptr, 1, buf_size, nodemask, "h2m_alloc_w_traits", 1);
        if (err != H2M_SUCCESS) assert_h2m_err("h2m_alloc_w_traits:verify_page_locations", err);

        // verify that amount of pages is interleaved in an acceptable form
        size_t exp_min = (buf_size / n_nodes) * 0.99;
        size_t exp_max = (buf_size / n_nodes) * 1.01;
        for(int k = 0; k < n_nodes; k++) {
            int tmp_used = self_usage_after[nodes[k]] - self_usage_before[nodes[k]];
            if(tmp_used < exp_min || tmp_used > exp_max) {
                std::stringstream ss;
                ss << "Data not nicely interleaved for nodemask " << nodemask << ". Here: node=" << nodes[k] << " Expected: min=" << exp_min << " - max=" << exp_max << " -> Actual:" << tmp_used;
                CPPUNIT_FAIL(ss.str());
            }
        }
        
        // cleanup
        err = h2m_free(ptr);
        if (err != H2M_SUCCESS) assert_h2m_err("h2m_free", err);
    }
}

void test_alloc_location :: alloc_blocked (void) {
    // variables
    int err         = H2M_SUCCESS;
    double* ptr     = nullptr;
    size_t arr_size = 10000000;

    std::vector<size_t> block_sizes_kb;
    block_sizes_kb.push_back(1000); // 1 MB:    evenly divisible (80)
    block_sizes_kb.push_back(1300); // 1.3 MB:  not evenly divisible (61.53)
    block_sizes_kb.push_back(4300); // 4.3 MB:  not evenly divisible (18.60)

    size_t buf_size = sizeof(double) * arr_size;
    unsigned long nodemask;
    int n_nodes;

    h2m_alloc_trait_value_t mem_spaces[3] = {
        h2m_atv_mem_space_hbw,
        h2m_atv_mem_space_large_cap,
        h2m_atv_mem_space_low_lat
    };

    #if PRINT_MASKS || DBG_PRINTS
    fprintf(stderr, "\n");
    #endif

    #if PRINT_MASKS
    fprintf(stderr, "  Original affinity mask for thread: ");
    get_and_print_affinity_mask();
    #endif // PRINT_MASKS

    // set affinity mask of current (master OpenMP) thread to core 0 as I was messing with cpusets before already
    struct bitmask *cpu_bit_mask = numa_allocate_cpumask();
    cpu_bit_mask = numa_bitmask_setbit(cpu_bit_mask, 0);
    cpu_set_t cpu_set = get_cpuset_for_bitmask(cpu_bit_mask);
    numa_free_cpumask(cpu_bit_mask);
    if (sched_setaffinity(gettid(), sizeof(cpu_set_t), &cpu_set) == -1) {
        perror("sched_setaffinity");
    }
        
    #if PRINT_MASKS
    fprintf(stderr, "  Affinity mask for thread ");
    get_and_print_affinity_mask();
    #endif // PRINT_MASKS

    for(int j = 0; j < 3; j++) {
        for (int idx_bs = 0; idx_bs < block_sizes_kb.size(); idx_bs++) {
            size_t bs = block_sizes_kb[idx_bs];

            #if DBG_PRINTS
            fprintf(stderr, "  Running for mem_space %d and block size %ld\n", j, bs);
            #endif // DBG_PRINTS

            // define traits to be used
            h2m_alloc_trait_value_t ms = mem_spaces[j];
            h2m_alloc_trait_t tmp_traits[4] = {
                { h2m_atk_req_mem_space, ms },
                { h2m_atk_mem_alignment, 4096 },
                { h2m_atk_mem_partition, h2m_atv_mem_partition_blocked },
                { .key = h2m_atk_mem_blocksize_kb, .value = { .sz = bs } }
            };

            // allocate data now
            ptr = (double*) h2m_alloc_w_traits(buf_size, &err, 4, tmp_traits);
            if (err != H2M_SUCCESS) assert_h2m_err("h2m_alloc_w_traits", err);
            void* ptr_end = (void*)((char*)ptr + buf_size);

            // parallel init of data
            #pragma omp parallel
            {
                #if PRINT_MASKS
                #pragma omp critical
                {
                    fprintf(stderr, "  Affinity mask for thread %d: ", omp_get_thread_num());
                    get_and_print_affinity_mask();
                }
                #endif // PRINT_MASKS

                #pragma omp for schedule(static)
                for(int i = 0; i < arr_size; i++) {
                    ptr[i] = 0.42;
                }
            }

            // validation placement of physical pages
            nodemask    = h2m_get_nodemask_for_mem_space(ms);
            err         = verify_page_locations((void**)&ptr, 1, buf_size, nodemask, "h2m_alloc_w_traits", 1);
            if (err != H2M_SUCCESS) assert_h2m_err("h2m_alloc_w_traits:verify_page_locations", err);
            
            // validation that placement is blocked round robin
            int* nodes  = h2m_get_nodes_for_mem_space(ms, &n_nodes);

            // temp variables
            int ctr_nodes           = 0;
            bool is_first           = true;
            size_t step_bytes       = bs * 1000;
            void* cur_addr          = ptr;
            void* last_pagestart    = nullptr;
            size_t stdps            = getpagesize();

            while (cur_addr < ptr_end) {
                // determine end
                void* range_end = (void*) ((char*)cur_addr + step_bytes);        
                if (range_end > ptr_end) {
                    range_end = ptr_end;
                }
                // determine page start for mbind (needs to be multiple of page size)
                void* cur_start_pagestart = cur_addr;
                if ((long)cur_addr % stdps != 0 && !is_first) {
                    // select next page as start for current operation
                    cur_start_pagestart = (void*)((char*)cur_addr + stdps - ((long)cur_addr % stdps));
                }
                // calculate difference
                size_t cur_step = (long)range_end - (long)cur_start_pagestart;
                // get nodemask
                int cur_node = nodes[ctr_nodes];
                unsigned long tmp_mask = 1 << cur_node;
                ctr_nodes = (ctr_nodes + 1) % n_nodes;
                // check
                err = verify_page_locations((void**)&cur_start_pagestart, 1, cur_step, tmp_mask, "h2m_alloc_w_traits", 1);
                if (err != H2M_SUCCESS) assert_h2m_err("h2m_alloc_w_traits:verify_page_locations", err);
                cur_addr = range_end;
                is_first = false;
            }

            // cleanup
            err = h2m_free(ptr);
            if (err != H2M_SUCCESS) assert_h2m_err("h2m_free", err);
        }
    }
}
