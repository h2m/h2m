# Test Instructions

## Run tests
```bash
# choose the number of threads depending on the number of NUMA domains the system has (or the number of NUMA domains with cores)
$ OMP_PLACES=cores OMP_PROC_BIND=spread OMP_NUM_THREADS=4 ./suite.exe
```
