#ifndef __TEST_COMMON_H__
#define __TEST_COMMON_H__

#include <string>

#ifndef PRINT_MASKS
#define PRINT_MASKS 0
#endif

#ifndef DBG_PRINTS
#define DBG_PRINTS 0
#endif

void assert_h2m_err(std::string msg, int h2m_err_code);

#endif // __TEST_COMMON_H__