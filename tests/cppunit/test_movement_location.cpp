#include "test_common.h"
#include "test_movement_location.h"
#include "h2m.h"
#include "h2m_utils_numa.h"

#include <bitset>
#include <iostream>
#include <numa.h>
#include <sstream>
#include <string>

CPPUNIT_TEST_SUITE_REGISTRATION (test_movement_location);

void test_movement_location :: setUp (void) {
    h2m_init();
}

void test_movement_location :: tearDown (void) {
    h2m_finalize();
}

void test_movement_location :: movement_to_single_node (void) {
    // variables
    int err         = H2M_SUCCESS;
    double* ptr     = nullptr;
    size_t arr_size = sizeof(double) * 10000000;
    unsigned long nodemask;
    int n_nodes;

    h2m_alloc_trait_value_t mem_spaces[3] = {
        h2m_atv_mem_space_hbw,
        h2m_atv_mem_space_large_cap,
        h2m_atv_mem_space_low_lat
    };

    int n_all_nodes = numa_num_configured_nodes();
    struct bitmask *cpu_mask_empty = numa_allocate_cpumask();
    cpu_mask_empty = numa_bitmask_clearall(cpu_mask_empty);

    #if PRINT_MASKS || DBG_PRINTS
    fprintf(stderr, "\n");
    #endif

    #if PRINT_MASKS
    fprintf(stderr, "  Original affinity mask for thread: ");
    get_and_print_affinity_mask();
    #endif // PRINT_MASKS

    for(int i = 0; i < n_all_nodes; i++) {
        // set affinity mask of current thread to corresponding core on NUMA node (bind to that core)
        struct bitmask *cpu_bit_mask = numa_allocate_cpumask();
        numa_node_to_cpus(i, cpu_bit_mask);
        // check whether bitmask is empty (memory only NUMA domain)
        if(numa_bitmask_equal(cpu_bit_mask, cpu_mask_empty)) {
            #if DBG_PRINTS
            fprintf(stderr, "  Node %d is memory only -- Skipping\n", i);
            #endif // DBG_PRINTS
            continue;
        } else {
            #if DBG_PRINTS
            fprintf(stderr, "  Thread pinned to NUMA node %d\n", i);
            #endif // DBG_PRINTS
        }

        // set new cpu affinity
        cpu_set_t node_cpu_set = get_cpuset_for_bitmask(cpu_bit_mask);
        numa_free_cpumask(cpu_bit_mask);
        if (sched_setaffinity(gettid(), sizeof(cpu_set_t), &node_cpu_set) == -1) {
            perror("sched_setaffinity");
        }
        
        #if PRINT_MASKS
        fprintf(stderr, "  Affinity mask for thread on NUMA node %d: ", i);
        get_and_print_affinity_mask();
        #endif // PRINT_MASKS

        // Iterate over target mem space
        for(int t = 0; t < 3; t++) {
            // determine target mem space
            h2m_alloc_trait_value_t target_ms = mem_spaces[t];

            int* nodes_tgt = h2m_get_nodes_for_mem_space(target_ms, &n_nodes);
            unsigned long nodemask_tgt = 1 << nodes_tgt[0]; // select the closest node here

            #if DBG_PRINTS
            char tmp_ms_tgt[50];
            h2m_alloc_trait_value_t_str(target_ms, tmp_ms_tgt);
            #endif // DBG_PRINTS

            // Iterate over source mem spaces
            for(int j = 0; j < 3; j++) {
                // define traits to be used
                h2m_alloc_trait_value_t ms = mem_spaces[j];
                h2m_alloc_trait_t tmp_traits[3] = {
                    h2m_atk_req_mem_space, ms, 
                    h2m_atk_mem_alignment, 4096,
                    h2m_atk_mem_partition, h2m_atv_mem_partition_close
                };

                #if DBG_PRINTS
                char tmp_ms[50];
                char tmp_par[50];
                h2m_alloc_trait_value_t_str(ms, tmp_ms);
                h2m_alloc_trait_value_t_str(h2m_atv_mem_partition_close, tmp_par);
                fprintf(stderr, "    Allocating on memory space\t%s\twith partition\t%s\n", tmp_ms, tmp_par);
                #endif // DBG_PRINTS

                // allocate data now
                ptr = (double*) h2m_alloc_w_traits(arr_size, &err, 3, tmp_traits);
                if (err != H2M_SUCCESS) assert_h2m_err("h2m_alloc_w_traits", err);
                memset(ptr, 0, arr_size); // and init data with zeros
                
                // validate placement of physical pages
                int* nodes  = h2m_get_nodes_for_mem_space(ms, &n_nodes);
                nodemask    = 1 << nodes[0]; // select the closest node here
                err         = verify_page_locations((void**)&ptr, 1, arr_size, nodemask, "h2m_alloc_w_traits", 1);
                if (err != H2M_SUCCESS) assert_h2m_err("h2m_alloc_w_traits:verify_page_locations", err);

                // prepare movements
                h2m_placement_decision_t dec_src    = h2m_get_default_placement();
                dec_src.mem_partition               = h2m_atv_mem_partition_close;
                dec_src.mem_space                   = ms;
                dec_src.mem_prio                    = h2m_atk_req_mem_space;
                h2m_placement_decision_t dec_tgt    = h2m_get_default_placement();
                dec_tgt.mem_partition               = h2m_atv_mem_partition_close;
                dec_tgt.mem_space                   = target_ms;
                dec_tgt.mem_prio                    = h2m_atk_req_mem_space;

                h2m_movement_info_t mov_src;
                mov_src.ptr             = ptr;
                mov_src.size            = arr_size;
                mov_src.pl              = &dec_src;
                mov_src.cpu_numa_node   = numa_node_of_cpu(sched_getcpu());
                mov_src.nodemask        = nodemask;
                h2m_movement_info_t mov_tgt;
                mov_tgt.ptr             = ptr;
                mov_tgt.size            = arr_size;
                mov_tgt.pl              = &dec_tgt;
                mov_tgt.cpu_numa_node   = numa_node_of_cpu(sched_getcpu());
                mov_tgt.nodemask        = nodemask_tgt;

                // move with mbind
                #if DBG_PRINTS
                fprintf(stderr, "      Moving with mbind: to mem space\t%s\t\t==> nodemask_src = %s, nodemask_tgt = %s\n", 
                    tmp_ms_tgt, 
                    std::bitset<8>(nodemask).to_string().c_str(), 
                    std::bitset<8>(nodemask_tgt).to_string().c_str());
                #endif // DBG_PRINTS
                err = h2m_move_memory_mbind(&mov_tgt, 1);
                if (err != H2M_SUCCESS) assert_h2m_err("h2m_move_memory_mbind", err);
                err = verify_page_locations((void**)&ptr, 1, arr_size, nodemask_tgt, "h2m_move_memory_mbind", 1);
                if (err != H2M_SUCCESS) assert_h2m_err("h2m_move_memory_mbind:verify_page_locations", err);
                // move back
                err = h2m_move_memory_mbind(&mov_src, 1);
                if (err != H2M_SUCCESS) assert_h2m_err("h2m_move_memory_mbind", err);
                err = verify_page_locations((void**)&ptr, 1, arr_size, nodemask, "h2m_move_memory_mbind", 1);
                if (err != H2M_SUCCESS) assert_h2m_err("h2m_move_memory_mbind:verify_page_locations", err);

                // move with movepages
                #if DBG_PRINTS
                fprintf(stderr, "      Moving with move_pages: to mem space\t%s\t==> nodemask_src = %s, nodemask_tgt = %s\n", 
                    tmp_ms_tgt, 
                    std::bitset<8>(nodemask).to_string().c_str(), 
                    std::bitset<8>(nodemask_tgt).to_string().c_str());
                #endif // DBG_PRINTS
                err = h2m_move_memory_movepages(&mov_tgt, 1);
                if (err != H2M_SUCCESS) assert_h2m_err("h2m_move_memory_movepages", err);
                err = verify_page_locations((void**)&ptr, 1, arr_size, nodemask_tgt, "h2m_move_memory_movepages", 1);
                if (err != H2M_SUCCESS) assert_h2m_err("h2m_move_memory_movepages:verify_page_locations", err);
                // move back
                err = h2m_move_memory_movepages(&mov_src, 1);
                if (err != H2M_SUCCESS) assert_h2m_err("h2m_move_memory_movepages", err);
                err = verify_page_locations((void**)&ptr, 1, arr_size, nodemask, "h2m_move_memory_movepages", 1);
                if (err != H2M_SUCCESS) assert_h2m_err("h2m_move_memory_movepages:verify_page_locations", err);
                
                // cleanup
                err = h2m_free(ptr);
                if (err != H2M_SUCCESS) assert_h2m_err("h2m_free", err);
            }
        }
    }
}

void test_movement_location :: movement_interleaved (void) {
    // TODO/FIXME: implement
}
