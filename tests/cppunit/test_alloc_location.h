#ifndef __TEST_ALLOC_LOCALTION_H__
#define __TEST_ALLOC_LOCALTION_H__

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

class test_alloc_location : public CPPUNIT_NS :: TestFixture {
    CPPUNIT_TEST_SUITE (test_alloc_location);
    CPPUNIT_TEST (alloc_on_single_node);
    CPPUNIT_TEST (alloc_interleaved);
    CPPUNIT_TEST (alloc_first_touch);
    CPPUNIT_TEST (alloc_blocked);
    CPPUNIT_TEST_SUITE_END ();

    public:
        void setUp (void);
        void tearDown (void);

    protected:
        void alloc_on_single_node (void);
        void alloc_interleaved (void);
        void alloc_first_touch (void);
        void alloc_blocked (void);
};

#endif // __TEST_ALLOC_LOCALTION_H__