#ifndef __TEST_MOVEMENT_LOCALTION_H__
#define __TEST_MOVEMENT_LOCALTION_H__

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

class test_movement_location : public CPPUNIT_NS :: TestFixture {
    CPPUNIT_TEST_SUITE (test_movement_location);
    CPPUNIT_TEST (movement_to_single_node);
    CPPUNIT_TEST (movement_interleaved);
    CPPUNIT_TEST_SUITE_END ();

    public:
        void setUp (void);
        void tearDown (void);

    protected:
        void movement_to_single_node (void);
        void movement_interleaved (void);
};

#endif // __TEST_MOVEMENT_LOCALTION_H__