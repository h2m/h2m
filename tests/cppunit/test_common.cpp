
#include "test_common.h"
#include "h2m.h"
#include <string>
#include <iostream>
#include <sstream>
#include <cppunit/extensions/HelperMacros.h>

void assert_h2m_err(std::string msg, int h2m_err_code) {
    std::stringstream ss;
    ss << msg << " failed with error code " << h2m_result_types_t_str[h2m_err_code];
    CPPUNIT_ASSERT_EQUAL_MESSAGE(ss.str(), (int)H2M_SUCCESS, h2m_err_code);
}