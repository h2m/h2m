#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

int main(int argc, char* argv[]) {
    printf("Data type sizes:\n");
    printf("int\t%zu\n", sizeof(int));
    printf("int32_t\t%zu\n", sizeof(int32_t));
    printf("float\t%zu\n", sizeof(float));
    printf("double\t%zu\n", sizeof(double));
    printf("char\t%zu\n", sizeof(char));
    printf("void*\t%zu\n", sizeof(void*));
    printf("double*\t%zu\n", sizeof(double*));
    printf("char*\t%zu\n", sizeof(char*));
    printf("uintptr_t\t%zu\n", sizeof(uintptr_t));
    printf("size_t\t%zu\n", sizeof(size_t));
    printf("uint64_t\t%zu\n", sizeof(uint64_t));
    printf("cpu_set_t\t%zu\n", sizeof(cpu_set_t));
    // printf("\t%zu\n", sizeof());
    return 0;
}
