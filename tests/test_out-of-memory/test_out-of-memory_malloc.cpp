#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <vector>

int main(int argc, char* argv[]) {
    size_t size_per_step_mb    = 2000;
    size_t size_per_step_bytes = size_per_step_mb * 1000 * 1000;
    size_t size_allocated_mb    = 0;

    // collect single allocations here
    std::vector<void*> list_allocs;
    long iter = 0;
    while(true) {
        printf("=== Step %03ld: Currently allocated: %10ld MB, allocating next chunk ...\n", iter++, size_allocated_mb);
        void *ptr = malloc(size_per_step_bytes);
        if (!ptr) {
			// this will actually never be reached on our systems as they allow overcommitting virtual memory
            printf("FAILED!\n");
            break;
        }
        printf("SUCCESS!\n");
        memset(ptr, 0, size_per_step_bytes);
        size_allocated_mb += size_per_step_mb;
        list_allocs.push_back(ptr);
    }

    // cleanup again
    for(int i = 0; i < list_allocs.size(); i++) {
        free(list_allocs[i]);
    }
    return 0;
}