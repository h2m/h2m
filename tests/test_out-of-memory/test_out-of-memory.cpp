#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <vector>

#include "h2m.h"
#include "h2m_utils_numa.h"

int main(int argc, char* argv[]) {
    // initialize library
    h2m_init();

    int err;
    size_t size_per_step_mb     = 5000;
    size_t size_per_step_bytes  = size_per_step_mb * 1000 * 1000;
    size_t size_allocated_mb    = 0;

    h2m_alloc_trait_value_t ms  = h2m_atv_mem_space_hbw;
    // h2m_alloc_trait_value_t ms = h2m_atv_mem_space_large_cap;

    // trait that explicitly requests memory with highest bandwidth
    h2m_alloc_trait_t traits_req_hbm[2] = {
        h2m_atk_req_mem_space, ms,
        h2m_atk_mem_alignment, 4096
    };

    int n_nodes;
    int *l_nodes = h2m_get_nodes_for_mem_space(ms, &n_nodes);

    for(int i = 0; i < n_nodes; i++) {
        printf("H2M HBW Node --> %d\n", l_nodes[i]);
    }
    printf("H2M HBW nodesmaks --> %lu\n", h2m_get_nodemask_for_mem_space(ms));

    // collect single allocations here
    std::vector<void*> list_allocs;
    long iter = 0;
    while(true) {
        printf("=== Step %03ld: Currently allocated: %10zu MB, allocating next chunk ...\n", iter++, size_allocated_mb);
        // print_numa_capacities();
        void *ptr = h2m_alloc_w_traits(size_per_step_bytes, &err, 2, traits_req_hbm);
        if (!ptr) {
            // this will actually be reached as we keep track of how much virtual memory has been allocated per memory space
            printf("FAILED! Err: %s\n", h2m_result_types_t_str[err]);
            break;
        }
        printf("SUCCESS!\n");
        // initialize data
        memset(ptr, 0, size_per_step_bytes);
        // book keeping
        size_allocated_mb += size_per_step_mb;
        list_allocs.push_back(ptr);
    }

    // cleanup again
    for(int i = 0; i < list_allocs.size(); i++) {
        h2m_free(list_allocs[i]);
    }

    // finialize library
    h2m_finalize();
    return 0;
}
