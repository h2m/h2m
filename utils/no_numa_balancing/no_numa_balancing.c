#include <stdlib.h>
#include <stdio.h>

#include <errno.h>
#include <numaif.h>

__attribute__((constructor))
void __no_numa_balancing(void)
{
	int res = set_mempolicy(MPOL_PREFERRED, NULL, 0);
	if (res) {
		fprintf(stderr, "Could not disable NUMA Balancing (returned %d, errno=%d)\n", res, errno);
		exit(EXIT_FAILURE);
	}
}
