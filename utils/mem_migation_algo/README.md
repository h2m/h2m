# Memory Migration Optimization

## Goal
Compute a new assignment to fast memory according to :

* fast memory capacity
* The object currently in fast mem
* value of each object (the higher, the better it is to map into fast mem)
* object size (weight)
* The cost of migration (from fast to slow or slow to fast)

It use the object IDs as specified by the caller

## Input
The input should look like the following: 

	max_capacity: 1000
	array_currently_in_fast: [1, 1, 0, 0]
	array_values: [1000, 100, 1000, 100]
	array_weights: [600, 400, 400, 600]
	array_copy_costs_to_fast: [300, 500, 300, 500]
	array_copy_costs_to_slow: [400, 200, 200, 400]
	array_alloc_ids: [10, 11, 12, 13]`

## Usage 
This mem\_migration.pl tool generate an lp_solve program and could be used this way: 

	mem_migration.pl simple.txt | lp_solve
	
## Output 
The variable with 1 should be assigned to fast memory and with a 0 to the slow memory

## Example 
With the above example we get : 

	mem_migration.pl simple.txt | lp_solve

	Value of objective function: 1500.00000000

	Actual values of the variables:
	x_10                            1
	x_11                            0
	x_12                            1
	x_13                            0
	
Meaning that : 

* x_10 stays in fast mem
* x_11 move to slow mem
* x_12 move to fast mem
* x_13 stays in slow mem
