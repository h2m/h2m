#!/usr/bin/perl -w

if ($#ARGV!=0){
   die "usage: $0 <Input file>\n";
}

$input_file = $ARGV[0];

open IN, $input_file;


# Parse the file. There shoulfd not be any carriage retunr in the array (only bewteen each array).  
foreach $line (<IN>){
   if($line =~ /max_capacity/){
      ($max_capacity) = ($line =~ /max_capacity:\s+(\d+)/);
   } elsif($line =~ /array_values/){
      @values = extract_array($line);
   } elsif($line =~ /array_weights/){
      @weights = extract_array($line);
   } elsif($line =~ /array_copy_costs_to_fast/){
      @copy_costs_to_fast = extract_array($line);
   }elsif($line =~ /array_copy_costs_to_slow/){
      @copy_costs_to_slow = extract_array($line);
   }elsif($line =~ /array_currently_in_fast/){
      @currently_in_fast = extract_array($line);
   }elsif($line =~ /array_alloc_ids/){
      @ids = extract_array($line);
   }
   
}

$nb_objects = @values;


# Objective
print "max: ";
# maximize value^T X
$op = "";
foreach $i (0..$nb_objects -1){
   print "$op $values[$i] x_$ids[$i] ";
   $op = "+";
}


foreach $i (0..$nb_objects -1){
   print "-";
   if ($currently_in_fast[$i]){
   # if object allready in fast mem then substract  the cost to move to slow  only in the case it is not assigned in fast mem 
      print " $copy_costs_to_slow[$i] +  $copy_costs_to_slow[$i] x_$ids[$i]";  
   } else {
      # Otherwise substract  the cost to copy in fast memory if assigned to fast memory
      print " $copy_costs_to_fast[$i] x_$ids[$i]";  
   }
}
print ";\n";

# Add the constaint that the sulm of the wieight of teh objkect assigned to fast mem cannot exceed the limit 
$op = "+";  
foreach $i (0..$nb_objects -1){
   print "$weights[$i] x_$ids[$i] $op ";
   if ($i == $nb_objects -2){
      $op = "";
   }
}
print " <= $max_capacity;\n";

# We need binary valiable (0->to Slow and 1 -> to fast)
print "binary ";
foreach $i (0..$nb_objects -1){
   print "x_$ids[$i] ";
}
print ";\n";


# par a line in the form somthing [val_1, val_2, ..., val_n] and return an array of the val_i
sub extract_array{
   my $line = shift;
   $line =~ s/\s+//g; #suppress spaces. 
   ($array) = ($line =~ /\[(.*)\]/);
   @res  = split ",",$array;
   return @res;
}
