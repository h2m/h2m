# H2M Utility Applications
Provides applications that can be used in the context of experiments and tests

## 1. `no_numa_balancing`
* Disables NUMA balancing on Linux OS: Avoid that pages that have initially be allocated on a certain NUMA node get dynamically moved by the OS
* Especially necessary for experiments with multiple types of memory that are exposed as separate NUMA nodes to the OS. Here, we need to rely on the fact that unless we explicitly migrate data, the data stays on the corresponding NUMA nodes
* **Note:** If build and placed on a system you need to modify the path in the `no_numa_balancing` file that is pointing to the `no_numa_balancing.so`


## 2. Memory Migration Optimization
* Provides a tool for computing a new memory allocation
* see:  `mem_migation_algo/README.md` for the details