#ifndef __H2M_COMMON_H__
#define __H2M_COMMON_H__

// common includes or definintion of internal data structures

#include "h2m.h"

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "h2m_tools.h"

// ================================================================================
// Functions & Function Defines
// ================================================================================

int   h2m_get_gtid();
void  h2m_dbg_print_help(int print_prefix, const char * prefix, int rank, int lvl, va_list args);
void  h2m_dbg_print(int rank, int lvl, ... );

// Memory capacity management
h2m_capacity_usage_info_t h2m_get_capacity_usage_info();
h2m_memory_characteristics_t h2m_get_mem_characteristics();

#endif // __H2M_COMMON_H__
