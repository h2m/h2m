#include "h2m.h"
#include "h2m_utils_numa.h"
#include "h2m_common_internal.h"
#include "h2m_common.h"
#include "h2m_config.h"

#include <fstream>
#include <iostream>
#include <regex>
#include <sstream>
#include <sys/types.h>
#include <bitset>
// #include <backtrace.h>
// #include <backtrace-supported.h>
#include <execinfo.h>

void* alloc(size_t size, size_t alignment, int *err) {
    *err = H2M_SUCCESS;
    size_t stdps = getpagesize();
    if (alignment < stdps) {
        alignment = stdps;
    }
    bool usehp = alignment > stdps;

    void* ptr;
    // ptr = memalign(alignment, size);
    // posix_memalign(&ptr, alignment, size);
    ptr = FCN_ALIGNED_ALLOC(alignment, size);
    // ptr = FCN_MALLOC(size);
    if (!ptr) {
        #if H2M_ENABLE_ALLOC_ERR_OUTPUT
        RELP("ERROR: memalign could not allocate memory\n");
        #endif // H2M_ENABLE_ALLOC_ERR_OUTPUT

        *err = H2M_COULD_NOT_ALLOCATE;
        return nullptr;
    }

    // NOTE: disabled madvise, as manipulating hugepages can lead to
    //       performance declines of up to 20% when using the data
    //       in an application.

    // if (usehp) {
    //     *err = madvise(ptr, size, MADV_HUGEPAGE);
    // } else {
    //     *err = madvise(ptr, size, MADV_NOHUGEPAGE);
    // }
    // if (*err != 0 && *err != -1) {
    //     #if H2M_ENABLE_ALLOC_ERR_OUTPUT
    //     RELP("ERROR: madvise failed with err code %d ==> %s\n", err, strerror(errno));
    //     #endif // H2M_ENABLE_ALLOC_ERR_OUTPUT

    //     return nullptr;
    // }

    return ptr;
}

void* get_page_start_for_address(void* ptr) {
    size_t stdps        = getpagesize();
    void* page_start    = ptr;
    int tmp_mod         = (long)page_start % stdps;

    if (tmp_mod != 0) {
        page_start = (void*)((char*)page_start - tmp_mod);
    }
    return page_start;
}

long run_blocked_partitioning(void *ptr, size_t size, h2m_placement_decision_t pl, bool is_new_alloc) {
    // get NUMA nodes for mem space
    long err2;
    int n_nodes;
    int* nodes  = h2m_get_nodes_for_mem_space(pl.mem_space, &n_nodes);

    // temporay variables
    void* ptr_end           = (void*) ((char*)ptr + size);
    int cur_ctr             = 0;
    bool is_first           = true;
    size_t step_bytes       = pl.mem_blocksize_kb * 1000;
    void* cur_addr          = ptr;
    size_t stdps            = getpagesize();
    assert(step_bytes >= stdps);
    
    while (cur_addr < ptr_end) {
        // determine end
        void* range_end = (void*) ((char*)cur_addr + step_bytes);        
        if (range_end > ptr_end) {
            range_end = ptr_end;
        }
        // determine page start for mbind (needs to be multiple of page size)
        void* cur_start_pagestart = cur_addr;
        if ((long)cur_addr % stdps != 0 && !is_first) {
            // select next page as start for current operation
            cur_start_pagestart = (void*)((char*)cur_addr + stdps - ((long)cur_addr % stdps));
        }
        // calculate difference
        size_t cur_step = (long)range_end - (long)cur_start_pagestart;
        // choose node and mask, increment round robin
        int cur_node = nodes[cur_ctr];
        unsigned long tmp_mask = 1 << cur_node;
        cur_ctr = (cur_ctr + 1) % n_nodes;
        // bind or move memory
        if(is_new_alloc) {
            err2 = mbind(cur_start_pagestart, cur_step, MPOL_BIND, &tmp_mask, sizeof(tmp_mask) * 8, /* new allocation = empty flags */ 0);
        } else {
            err2 = mbind(cur_start_pagestart, cur_step, MPOL_BIND, &tmp_mask, sizeof(tmp_mask) * 8, MPOL_MF_MOVE);
        }

        if (err2 != 0) {
            #if H2M_ENABLE_ALLOC_ERR_OUTPUT
            RELP("ERROR: mbind failed with err code %d ==> %s\n", errno, strerror(errno));
            const char* nodemask_str = std::bitset<12>(tmp_mask).to_string().c_str();
            RELP("ERROR: ptr=" DPxMOD ", size=%ld, ptr_end=" DPxMOD ", step_bytes=%ld, cur_step=%ld, cur_addr=" DPxMOD ", cur_start_pagestart=" DPxMOD ", range_end=" DPxMOD ", cur_node=%d, mask=%s\n", DPxPTR(ptr), size, DPxPTR(ptr_end), step_bytes, cur_step, DPxPTR(cur_addr), DPxPTR(cur_start_pagestart), DPxPTR(range_end), cur_node, nodemask_str);
            #endif // H2M_ENABLE_ALLOC_ERR_OUTPUT
            return err2;
        }
        // update offset
        cur_addr = range_end;
        is_first = false;
    }
    return err2;
}

void* alloc_on_nodes(size_t size, h2m_placement_decision_t pl, unsigned long nodemask, int *err) {
    void* ptr = alloc(size, pl.alignment, err);
    if (!ptr) {
        return nullptr;
    }

    long err2 = 0;
    if (pl.mem_partition == h2m_atv_mem_partition_blocked) {
        err2 = run_blocked_partitioning(ptr, size, pl, true);
    } else if (pl.mem_partition == h2m_atv_mem_partition_interleaved) {
        err2 = mbind(ptr, size, MPOL_INTERLEAVE, &nodemask, sizeof(nodemask) * 8, /* new allocation = empty flags */ 0);
    } else { // first-touch and close
        err2 = mbind(ptr, size, MPOL_BIND, &nodemask, sizeof(nodemask) * 8, /* new allocation = empty flags */ 0);
    }

    if (err2 != 0) {
        #if H2M_ENABLE_ALLOC_ERR_OUTPUT
        RELP("ERROR: mbind failed with err code %d ==> %s\n", errno, strerror(errno));
        #endif // H2M_ENABLE_ALLOC_ERR_OUTPUT

        // cleanup again
        if(ptr) {
            FCN_FREE(ptr);
        }

        *err = H2M_COULD_NOT_BIND;
        return nullptr;
    }
    return ptr;
}

void check_page_nodes(void* ptr, size_t size, unsigned long nodemask, int* correct_pages, int* wrong_pages, int output) {
    void *tmp_pointer;
    int current_data_node, ret_code;
    *wrong_pages                = 0;
    *correct_pages              = 0;
    int printed                 = 0;
    int stdps                   = getpagesize();
    const char* nodemask_str    = std::bitset<12>(nodemask).to_string().c_str();
    unsigned long tmp_mask;
    
    // always check in multiple of getpagesize()
    for(size_t i = 0; i < size; i += stdps) {
        tmp_pointer = (void*)((char*)ptr + i);
        ret_code = move_pages(0 /*self memory */, 1, &tmp_pointer, NULL, &current_data_node, 0);
        tmp_mask = 1 << current_data_node;
        if ((tmp_mask & nodemask) != 0) {
            (*correct_pages)++;
        } else {
            (*wrong_pages)++;
            if (output && !printed) {
                if (current_data_node < 0) {
                    RELP(">>>> Arr[%5d]: Ret=%ld; nodemask=%s; current_data_node=%d ==> %s\n", i, ret_code, nodemask_str, current_data_node, strerror(current_data_node));
                } else {
                    RELP(">>>> Arr[%5d]: Ret=%ld; nodemask=%s; current_data_node=%d\n", i, ret_code, nodemask_str, current_data_node);
                }                
                printed = 1;
            }
        }
    }
}

int move_memory_mbind(void* ptr, size_t size, h2m_placement_decision_t *pl, unsigned long nodemask) {
    long err;
    
    if (pl->mem_partition == h2m_atv_mem_partition_blocked) {
        err = run_blocked_partitioning(ptr, size, *pl, false);
    } else if (pl->mem_partition == h2m_atv_mem_partition_interleaved) {
        err = mbind(ptr, size, MPOL_INTERLEAVE, &nodemask, sizeof(nodemask) * 8, MPOL_MF_MOVE);
    } else { // first-touch and close
        err = mbind(ptr, size, MPOL_BIND, &nodemask, sizeof(nodemask) * 8, MPOL_MF_MOVE);
    }

    if (err != 0) {
        #if H2M_ENABLE_ALLOC_ERR_OUTPUT
        RELP("ERROR: mbind failed with err code %d ==> %s\n", errno, strerror(errno));
        #endif // H2M_ENABLE_ALLOC_ERR_OUTPUT
        return H2M_COULD_NOT_MOVE;
    }
    return H2M_SUCCESS;
}

int move_memory_movepages(int num_pages, void** pages, int *nodes) {
    int *status = (int*) FCN_MALLOC(num_pages * sizeof(int));
    if (status == nullptr) {
        #if H2M_ENABLE_ALLOC_ERR_OUTPUT
        RELP("ERROR: Could not allocate new array for status. Err code %d ==> %s\n", errno, strerror(errno));
        print_numa_capacities();
        #endif // H2M_ENABLE_ALLOC_ERR_OUTPUT
        return H2M_COULD_NOT_ALLOCATE;
    }

    // Format: long move_pages(int pid, unsigned long count, void **pages, const int *nodes, int *status, int flags);
    long err = move_pages(0 /*self memory */, num_pages, pages, nodes, status, MPOL_MF_MOVE);
    if (err == -1) {
        #if H2M_ENABLE_ALLOC_ERR_OUTPUT
        RELP("ERROR: move_pages failed with err code %d ==> %s\n", errno, strerror(errno));
        #endif // H2M_ENABLE_ALLOC_ERR_OUTPUT
    } else if (err != 0) {
        #if H2M_ENABLE_ALLOC_ERR_OUTPUT
        RELP("ERROR: move_pages failed to migrate %d pages\n", err);
        for (int i = 0; i < num_pages; i++) {
            if(status[i] < 0) {
                RELP("ERROR: move_pages --> page %p, Err code %d ==> %s\n", pages[i], status[i], strerror(status[i]));
            }
        }
        #endif // H2M_ENABLE_ALLOC_ERR_OUTPUT
    }

    FCN_FREE(status);
    return (int) err;
}

void print_numa_capacities() {
    long long bytes_size, bytes_free;
    for(int n = 0; n < numa_num_configured_nodes(); n++) {
        bytes_size = numa_node_size64(n, &bytes_free);
        printf("\tnuma_node_size (node %d): %f MB, free: %f MB\n", n, bytes_size / 1000.0 / 1000.0, bytes_free / 1000.0 / 1000.0);
    }
}

std::map<int, size_t> get_numa_capacities() {
    DBP(__VLVL_ENTRY_EXIT_VERBOSE, "get_numa_capacities (enter)\n");
    long long bytes_size, bytes_free;
    std::map<int, size_t> ret;
    for(int n = 0; n < numa_num_configured_nodes(); n++) {
        bytes_size = numa_node_size64(n, &bytes_free);
        ret[n] = (size_t) bytes_free;
    }
    DBP(__VLVL_ENTRY_EXIT_VERBOSE, "get_numa_capacities (exit)\n");
    return ret;
}

int verify_page_locations(void** pointers, size_t num_ptrs, size_t size_per_pointer, unsigned long nodemask, const char* str, int output) {
    // checks by function check_page_nodes will be performned in steps of 4KB pages
    size_t n_correct_pages      = 0;
    size_t n_wrong_pages        = 0;
    const char* nodemask_str    = std::bitset<12>(nodemask).to_string().c_str();

    int tmp_correct = 0;
    int tmp_wrong   = 0;
    for(int k = 0; k < num_ptrs; k++ ) {
        check_page_nodes(pointers[k], size_per_pointer, nodemask, &tmp_correct, &tmp_wrong, output);
        n_correct_pages += tmp_correct;
        n_wrong_pages   += tmp_wrong;
    }
    size_t n_all_pages = n_correct_pages + n_wrong_pages;
    if (n_wrong_pages > 0) {
        double prc_wrong = ((double)n_wrong_pages) / ((double)(n_all_pages)) * 100.0;
        fprintf(stderr, "\tVerification for \"%s\" failed;nodemask;%s;n_all_pages;%zu;n_correct;%zu;n_wrong;%zu;pct_wrong;%f\n", str, nodemask_str, n_all_pages, n_correct_pages, n_wrong_pages, prc_wrong);
        return H2M_WRONG_PAGE_LOCATION;
    }
    return H2M_SUCCESS;
}

std::vector<std::string> split( std::string const& original, char separator ) {
    std::vector<std::string> results;
    std::string::const_iterator start   = original.begin();
    std::string::const_iterator end     = original.end();
    std::string::const_iterator next    = std::find( start, end, separator );
    while ( next != end ) {
        results.push_back( std::string( start, next ) );
        start = next + 1;
        next = std::find( start, end, separator );
    }
    results.push_back( std::string( start, next ) );
    return results;
}

std::map<int, size_t> get_self_numa_memory_usage() {
    // init empty map
    std::map<int, size_t> usage;
    for(int i = 0; i < numa_num_configured_nodes(); i++) {
        usage[i] = 0;
    }

    std::string file_name = "/proc/" + std::to_string(getpid()) + "/numa_maps";
    std::regex reg("N\\d*=\\d*");

    std::ifstream infile(file_name);
    if (infile.is_open()) {
        std::string line;
        while (std::getline(infile, line))
        {
            auto l_begin = std::sregex_iterator(line.begin(), line.end(), reg);
            auto l_end   = std::sregex_iterator();
            for (std::sregex_iterator i = l_begin; i != l_end; ++i) {
                std::smatch match = *i;
                std::vector<std::string> spl = split(match.str().substr(1), '=');
                int key  = std::stoi(spl[0]);
                long val = std::stol(spl[1]);
                usage[key] = usage[key] + val * 4096;
            }
        }
    }
    return usage;
}

void print_affinity_mask(cpu_set_t mask) {
    long nproc, i;
    bool first = true;

    // get the number of processors/mts
    nproc = sysconf(_SC_NPROCESSORS_ONLN);

    // build + output mask string
    std::string mask_str("");
    for (i = 0; i < nproc; i++) {
        if (CPU_ISSET(i, &mask)) {
            if (first) {
                mask_str = mask_str + std::to_string(i);
                first = false;
            } else {
                mask_str = mask_str + "," + std::to_string(i);
            }
        //     mask_str = mask_str + " X";
        // } else {
        //     mask_str = mask_str + " .";
        }
    }
    RELP("CPUSET ==> %s\n", mask_str.c_str());
}

void get_and_print_affinity_mask() {
    cpu_set_t mask;
    // get the affinity mask from the current thread
    if (sched_getaffinity(gettid(), sizeof(cpu_set_t), &mask) == -1) {
        perror("sched_getaffinity");
        return;
    }
    return print_affinity_mask(mask);
}

cpu_set_t get_cpuset_for_bitmask(struct bitmask * mask) {
    cpu_set_t ret;
    CPU_ZERO(&ret);
    for(int i = 0; i < numa_num_configured_cpus(); i++) {
        if (numa_bitmask_isbitset(mask, i)) {
            CPU_SET(i, &ret);
        }
    }
    return ret;
}

int get_node_from_nodemask(unsigned long nodemask) {
    for(int i = 0; i < numa_num_configured_cpus(); i++) {
        unsigned long tmp = 1 << i;
        if (nodemask & tmp) {
            return i;
        }
    }
    return -1;
}

void** get_caller_rip(int depth, int* size_callstack, void** caller_rip) {
    static int max_depth = 20;
    int backtrace_depth = max_depth + 1;
    void** buffer = (void**) FCN_MALLOC(sizeof(void*)*backtrace_depth);

    int nb_calls = backtrace(buffer, backtrace_depth);
    if(nb_calls < depth) {
        *size_callstack = 0;
        *caller_rip = NULL;
        FCN_FREE(buffer);
        return NULL;
    }

    *size_callstack = nb_calls;
    *caller_rip = buffer[depth];
    return buffer;
}

std::string get_callstack_rip_str(int additional_idx_start) {
    // get callstack
    void* caller_rip;
    int callstack_size;
    int effective_idx = 3 + additional_idx_start;
    void** callstack_rip = get_caller_rip(effective_idx, &callstack_size, &caller_rip);

    std::string ret;
    for(int i = effective_idx; i < callstack_size; i++) {
        // get information about base address of executable or shared library for code location
        Dl_info info;
        int rc = dladdr(callstack_rip[i], &info);

        // calculate offset to where shared library / executable has been loaded into memory
        ptrdiff_t offset = (uintptr_t)callstack_rip[i] - (uintptr_t)info.dli_fbase;

        if(i > effective_idx) {
            ret.append(",");
        }
        char cur_str[16];
        sprintf(cur_str, "%p", callstack_rip[i]);
        ret.append(std::string(cur_str));
    }
    return ret;
}

std::string get_callstack_offsets(int additional_idx_start) {
    // get callstack
    void* caller_rip;
    int callstack_size;
    int effective_idx = 3 + additional_idx_start;
    void** callstack_rip = get_caller_rip(effective_idx, &callstack_size, &caller_rip);

    std::string ret;
    for(int i = effective_idx; i < callstack_size; i++) {
        // get information about base address of executable or shared library for code location
        Dl_info info;
        int rc = dladdr(callstack_rip[i], &info);

        // calculate offset to where shared library / executable has been loaded into memory
        ptrdiff_t offset = (uintptr_t)callstack_rip[i] - (uintptr_t)info.dli_fbase;

        if(i > effective_idx) {
            ret.append(",");
        }
        ret.append(info.dli_fname);
        ret.append(":");
        ret.append(std::to_string(offset));
    }
    return ret;
}

std::string get_clean_callsite_offsets(std::string callstack_offsets) {
    std::vector<std::string> spl;
    string_split(callstack_offsets, spl, ',');
    std::string ret = "";

    for(size_t i = 0; i < spl.size(); i++) {
        std::vector<std::string> spl2;
        string_split(spl[i], spl2, ':');
        std::vector<std::string> spl3;
        string_split(spl2[0], spl3, '/');
        if(i > 0) {
            ret.append(",");
        }
        ret.append(spl3[spl3.size()-1]);
        ret.append(":");
        ret.append(spl2[1]);
    }
    return ret;
}
