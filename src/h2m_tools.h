#ifndef __H2M_TOOLS_H__
#define __H2M_TOOLS_H__

#include "h2m.h"
#include <stddef.h>
#include <stdint.h>
#include <dlfcn.h>

//================================================================================
// Enums & Type Definitions
//================================================================================

typedef enum h2m_tool_callback_types_t {
    h2m_callback_thread_init            = 0,
    h2m_callback_post_init_serial       = 1,
    h2m_callback_thread_finalize        = 2,
    h2m_callback_declare_alloc          = 3,
    h2m_callback_commit_allocs          = 4,
    h2m_callback_alloc                  = 5,
    h2m_callback_alloc_on_nodes         = 6,
    h2m_callback_free                   = 7,
    h2m_callback_update_traits          = 8,
    h2m_callback_apply_migration        = 9,
    h2m_callback_apply_migration_alloc  = 10,
    h2m_callback_allocation_strategy    = 11,
    h2m_callback_commit_strategy        = 12,
    h2m_callback_migration_strategy     = 13,
    h2m_callback_move_memory_mbind      = 14,
    h2m_callback_move_memory_movepages  = 15,
    h2m_callback_verify_traits          = 16,
    h2m_callback_apply_migration_async  = 17,
    h2m_callback_apply_migration_alloc_async = 18,
    h2m_callback_wait                   = 19,
} h2m_tool_callback_types_t;

typedef enum h2m_tool_set_result_t {
    h2m_tool_set_error              = 0,
    h2m_tool_set_never              = 1,
    h2m_tool_set_impossible         = 2,
    h2m_tool_set_sometimes          = 3,
    h2m_tool_set_sometimes_paired   = 4,
    h2m_tool_set_always             = 5
} h2m_tool_set_result_t;

typedef enum h2m_tool_migration_status_t {
    h2m_tool_migration_start    = 1,
    h2m_tool_migration_end      = 2
} h2m_tool_migration_status_t;

static const char* h2m_tool_migration_status_t_str[] = {
    NULL,
    "h2m_tool_migration_start",     // 1
    "h2m_tool_migration_end"        // 2
};

typedef enum h2m_tool_movement_status_t {
    h2m_tool_movement_start     = 1,
    h2m_tool_movement_end       = 2
} h2m_tool_movement_status_t;

static const char* h2m_tool_movement_status_t_str[] = {
    NULL,
    "h2m_tool_movement_start",     // 1
    "h2m_tool_movement_end"        // 2
};

typedef enum h2m_tool_wait_status_t {
    h2m_tool_wait_start    = 1,
    h2m_tool_wait_end      = 2
} h2m_tool_wait_status_t;

static const char* h2m_tool_wait_status_t_str[] = {
    NULL,
    "h2m_tool_wait_start",     // 1
    "h2m_tool_wait_end"        // 2
};

typedef void (*h2m_tool_interface_fn_t) (void);

typedef h2m_tool_interface_fn_t (*h2m_tool_function_lookup_t) (
    const char *interface_function_name
);

typedef void (*h2m_tool_callback_t) (void);

// either interprete data type as value or pointer
typedef union h2m_tool_data_t {
    uint64_t value;
    void *ptr;
} h2m_tool_data_t;

typedef struct h2m_migration_decision_t {
    h2m_alloc_info_t            alloc_info;
    h2m_placement_decision_t    placement;
} h2m_migration_decision_t;

// overview of capacity used and limits for each memory space (only covering buffers allocated with H2M)
typedef struct h2m_capacity_usage_info_t {
    size_t mem_bytes_used_hbw;
    size_t mem_bytes_used_low_lat;
    size_t mem_bytes_used_large_cap;

    // Note: include information about nodemasks to see if memory spaces are the same

    unsigned long nodemask_hbw;
    unsigned long nodemask_low_lat;
    unsigned long nodemask_large_cap;

    size_t max_mem_cap_bytes_hbw;
    size_t max_mem_cap_bytes_low_lat;
    size_t max_mem_cap_bytes_large_cap;
    int max_mem_cap_MB_hbw;
    int max_mem_cap_MB_low_lat;
    int max_mem_cap_MB_large_cap;
} h2m_capacity_usage_info_t;

// representation of allocation declaration
typedef struct h2m_declaration_instance_t {
    void** ptr;
    size_t size;
    h2m_fcn_data_init_t fcn_data_init;
    void *args_data_init;
    int n_traits;
    h2m_alloc_trait_t *traits;
} h2m_declaration_instance_t;

// contains placement decision for allocation declaration entry
typedef struct h2m_alloc_order_entry_t {
    h2m_declaration_instance_t* decl;
    h2m_placement_decision_t pl;
} h2m_alloc_order_entry_t;

// representation of migration request
typedef struct h2m_migration_req_instance_t {
    void** allocations;
    int n_allocs;
    volatile int is_completed;
    int return_code;
    double elapsed_sec;
} h2m_migration_req_instance_t;

// memory access characteristics between numa nodes
typedef struct h2m_memory_characteristics_t {
    int      num_cpu_nodes;
    int      num_mem_nodes;
    int      num_threads;
    double** matrix_lat_read_only;  // dimensions: [num_cpu_nodes][num_threads x num_mem_nodes]
    double** matrix_lat_read_write; // dimensions: [num_cpu_nodes][num_threads x num_mem_nodes]
    double** matrix_bw_read_only;   // dimensions: [num_cpu_nodes][num_threads x num_mem_nodes]
    double** matrix_bw_read_write;  // dimensions: [num_cpu_nodes][num_threads x num_mem_nodes]
} h2m_memory_characteristics_t;

//================================================================================
// Init / Finalize / Start Tool
//================================================================================

typedef int (*h2m_tool_initialize_t) (
    h2m_tool_function_lookup_t lookup,
    h2m_tool_data_t *tool_data
);

typedef void (*h2m_tool_finalize_t) (
    h2m_tool_data_t *tool_data
);

typedef struct h2m_tool_start_result_t {
    h2m_tool_initialize_t   initialize;
    h2m_tool_finalize_t     finalize;
    h2m_tool_data_t         tool_data;
} h2m_tool_start_result_t;

//================================================================================
// Getter / Setter
//================================================================================

typedef h2m_tool_set_result_t (*h2m_tool_set_callback_t) (
    h2m_tool_callback_types_t event,
    h2m_tool_callback_t callback
);

typedef int (*h2m_tool_get_callback_t) (
    h2m_tool_callback_types_t event,
    h2m_tool_callback_t *callback
);

typedef h2m_tool_data_t *(*h2m_tool_get_thread_data_t) (void);

typedef h2m_capacity_usage_info_t (*h2m_tool_get_capacity_usage_info_t) (void);

typedef h2m_memory_characteristics_t (*h2m_tool_get_mem_characteristics_t) (void);

//================================================================================
// List of callbacks
//================================================================================

typedef void (*h2m_tool_callback_thread_init_t) (
    h2m_tool_data_t *thread_data
);

typedef void (*h2m_tool_callback_post_init_serial_t) (
    h2m_tool_data_t *thread_data
);

typedef void (*h2m_tool_callback_thread_finalize_t) (
    h2m_tool_data_t *thread_data
);

typedef int (*h2m_tool_callback_verify_traits_t) (
    int n_traits, 
    const h2m_alloc_trait_t traits[]
);

typedef void (*h2m_tool_callback_declare_alloc_t) (
    void** ptr, 
    size_t size, 
    h2m_fcn_data_init_t fcn_data_init, 
    void *fcn_args, 
    int n_traits, 
    const h2m_alloc_trait_t traits[]
);

typedef void (*h2m_tool_callback_commit_allocs_t) (
    const h2m_declaration_t handles[], 
    size_t n_handles
);

typedef void (*h2m_tool_callback_alloc_t) (
    size_t size, 
    int n_traits, 
    const h2m_alloc_trait_t traits[]
);

typedef void (*h2m_tool_callback_alloc_on_nodes_t) (
    size_t size, 
    h2m_placement_decision_t pl,
    unsigned long nodemask,
    int n_traits, 
    const h2m_alloc_trait_t traits[]
);

typedef void (*h2m_tool_callback_free_t) (
    void* ptr
);

typedef void (*h2m_tool_callback_update_traits_t) (
    void *ptr, 
    size_t size, 
    int n_traits, 
    const h2m_alloc_trait_t traits[]
);

typedef void (*h2m_tool_callback_apply_migration_t) (
    h2m_tool_migration_status_t status
);

typedef void (*h2m_tool_callback_apply_migration_async_t) (
    h2m_migration_req_t *out_req
);

typedef void (*h2m_tool_callback_apply_migration_alloc_t) (
    void** allocations, 
    int n_allocs,
    h2m_tool_migration_status_t status
);

typedef void (*h2m_tool_callback_apply_migration_alloc_async_t) (
    void** allocations, 
    int n_allocs,
    h2m_migration_req_t *out_req
);

typedef void (*h2m_tool_callback_wait_t) (
    h2m_migration_req_t req,
    h2m_tool_wait_status_t status
);

typedef h2m_placement_decision_t (*h2m_tool_callback_allocation_strategy_t) (
    size_t size, 
    int n_traits, 
    const h2m_alloc_trait_t traits[],
    int *out_err
);

typedef h2m_alloc_order_entry_t* (*h2m_tool_callback_commit_strategy_t) (
    const h2m_declaration_t handles[],
    size_t n_handles,
    int *out_err
);

typedef h2m_migration_decision_t* (*h2m_tool_callback_migration_strategy_t) (
    int n_ele, 
    const h2m_alloc_info_t *elements, 
    int* out_n_decisions
);

typedef void (*h2m_tool_callback_move_memory_mbind_t) (
    const h2m_movement_info_t *m_info,
    int n_movements,
    h2m_tool_movement_status_t status
);

typedef void (*h2m_tool_callback_move_memory_movepages_t) (
    const h2m_movement_info_t *m_info,
    int n_movements,
    h2m_tool_movement_status_t status
);

#endif // __H2M_TOOLS_H__
