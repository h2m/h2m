#ifndef __H2M_H__
#define __H2M_H__

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <errno.h>
#include <inttypes.h>
#include <sched.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#ifdef __linux__
// #include <sys/syscall.h>
#endif
#include <unistd.h>

#ifndef DPxMOD
#define DPxMOD "0x%0*" PRIxPTR
#endif

#ifndef DPxPTR
#define DPxPTR(ptr) ((int)(2*sizeof(uintptr_t))), ((uintptr_t) (ptr))
#endif

//================================================================================
// Type definitions
//================================================================================

typedef enum h2m_result_types_t {
    H2M_SUCCESS                 = 0,
    H2M_FAILURE                 = 1,
    H2M_COULD_NOT_ALLOCATE      = 2,
    H2M_COULD_NOT_MOVE          = 3,
    H2M_MEM_LIMIT_EXCEEDED      = 4,
    H2M_MEM_NO_SPACE_ON_NODES   = 5,
    H2M_NOT_COVERED             = 6,
    H2M_NOT_IMPLEMENTED         = 7,
    H2M_WRONG_PAGE_LOCATION     = 8,
    H2M_INVALID_MEM_SPACE       = 9,
    H2M_COULD_NOT_BIND          = 10,
    H2M_SOME_PAGES_NOT_MOVED    = 11
} h2m_result_types_t;

static const char* h2m_result_types_t_str[] = {
    "H2M_SUCCESS",                  // 0
    "H2M_FAILURE",                  // 1
    "H2M_COULD_NOT_ALLOCATE",       // 2
    "H2M_COULD_NOT_MOVE",           // 3
    "H2M_MEM_LIMIT_EXCEEDED",       // 4
    "H2M_MEM_NO_SPACE_ON_NODES",    // 5
    "H2M_NOT_COVERED",              // 6
    "H2M_NOT_IMPLEMENTED",          // 7
    "H2M_WRONG_PAGE_LOCATION",      // 8
    "H2M_INVALID_MEM_SPACE",        // 9
    "H2M_COULD_NOT_BIND",           // 10
    "H2M_SOME_PAGES_NOT_MOVED"      // 11
};

typedef enum h2m_alloc_trait_key_t { 
    h2m_atk_req_mem_space       = 0,
    h2m_atk_pref_mem_space      = 1,
    h2m_atk_mem_alignment       = 2,
    h2m_atk_mem_partition       = 3,
    h2m_atk_mem_blocksize_kb    = 4,
    h2m_atk_sensitivity         = 5,
    h2m_atk_access_mode         = 6,
    h2m_atk_access_origin       = 7,
    h2m_atk_access_freq         = 8,
    h2m_atk_access_prio         = 9,
    h2m_atk_access_pattern      = 10,
    h2m_atk_access_stride       = 11,
    h2m_atk_structure_type      = 12,
    h2m_atk_custom1             = 13, // custom field that can be used for arbitrary data
    h2m_atk_custom2             = 14, // custom field that can be used for arbitrary data
    h2m_atk_custom3             = 15, // custom field that can be used for arbitrary data
    h2m_atk_custom4             = 16, // custom field that can be used for arbitrary data
    h2m_atk_custom5             = 17, // custom field that can be used for arbitrary data
    h2m_atk_custom6             = 18, // custom field that can be used for arbitrary data
} h2m_alloc_trait_key_t;

static const char* h2m_alloc_trait_key_t_str[] = {
    "h2m_atk_req_mem_space",        // 0
    "h2m_atk_pref_mem_space",       // 1
    "h2m_atk_mem_alignment",        // 2
    "h2m_atk_mem_partition",        // 3
    "h2m_atk_mem_blocksize_kb",     // 4
    "h2m_atk_sensitivity",          // 5
    "h2m_atk_access_mode",          // 6
    "h2m_atk_access_origin",        // 7
    "h2m_atk_access_freq",          // 8
    "h2m_atk_access_prio",          // 9
    "h2m_atk_access_pattern",       // 10
    "h2m_atk_access_stride",        // 11
    "h2m_atk_structure_type",       // 12
    "h2m_atk_custom1",              // 13
    "h2m_atk_custom2",              // 14
    "h2m_atk_custom3",              // 15
    "h2m_atk_custom4",              // 16
    "h2m_atk_custom5",              // 17
    "h2m_atk_custom6",              // 18
};

typedef enum h2m_alloc_trait_value_t { 
    h2m_atv_mem_space_hbw                                   = 0x000001,
    h2m_atv_mem_space_low_lat                               = 0x000002,
    h2m_atv_mem_space_large_cap                             = 0x000004,

    h2m_atv_bw_sensitive                                    = 0x000008,
    h2m_atv_lat_sensitive                                   = 0x000010,

    h2m_atv_access_mode_readonly                            = 0x000020,
    h2m_atv_access_mode_writeonly                           = 0x000040,
    h2m_atv_access_mode_readwrite                           = 0x000080,

    h2m_atv_access_freq_low                                 = 0x000100,
    h2m_atv_access_freq_medium                              = 0x000200,
    h2m_atv_access_freq_high                                = 0x000400,

    h2m_atv_access_pattern_linear                           = 0x000800,
    h2m_atv_access_pattern_strided                          = 0x001000,
    h2m_atv_access_pattern_random                           = 0x002000,
    h2m_atv_access_pattern_stencil                          = 0x004000,

    h2m_atv_structure_type_array                            = 0x008000,
    h2m_atv_structure_type_matrix                           = 0x010000,
    h2m_atv_structure_type_tree                             = 0x020000,

    h2m_atv_access_origin_single_threaded                   = 0x040000,
    h2m_atv_access_origin_multi_threaded                    = 0x080000,
    h2m_atv_access_origin_multi_threaded_different_nodes    = 0x100000,

    h2m_atv_mem_partition_first_touch                       = 0x200000,
    h2m_atv_mem_partition_close                             = 0x400000,
    h2m_atv_mem_partition_interleaved                       = 0x800000,
    h2m_atv_mem_partition_blocked                           = 0x1000000,

} h2m_alloc_trait_value_t;

static void h2m_alloc_trait_value_t_str(h2m_alloc_trait_value_t type, char *buffer) {
    char *ret = buffer;
    char const *sep = "";
    if (type & h2m_atv_mem_space_hbw)                   {ret += sprintf(ret, "%sh2m_atv_mem_space_hbw", sep); sep = "|";}
    if (type & h2m_atv_mem_space_low_lat)               {ret += sprintf(ret, "%sh2m_atv_mem_space_low_lat", sep); sep = "|";}
    if (type & h2m_atv_mem_space_large_cap)             {ret += sprintf(ret, "%sh2m_atv_mem_space_large_cap", sep); sep = "|";}
    if (type & h2m_atv_bw_sensitive)                    {ret += sprintf(ret, "%sh2m_atv_bw_sensitive", sep); sep = "|";}
    if (type & h2m_atv_lat_sensitive)                   {ret += sprintf(ret, "%sh2m_atv_lat_sensitive", sep); sep = "|";}
    if (type & h2m_atv_access_mode_readonly)            {ret += sprintf(ret, "%sh2m_atv_access_mode_readonly", sep); sep = "|";}
    if (type & h2m_atv_access_mode_writeonly)           {ret += sprintf(ret, "%sh2m_atv_access_mode_writeonly", sep); sep = "|";}
    if (type & h2m_atv_access_mode_readwrite)           {ret += sprintf(ret, "%sh2m_atv_access_mode_readwrite", sep); sep = "|";}
    if (type & h2m_atv_access_freq_low)                 {ret += sprintf(ret, "%sh2m_atv_access_freq_low", sep); sep = "|";}
    if (type & h2m_atv_access_freq_medium)              {ret += sprintf(ret, "%sh2m_atv_access_freq_medium", sep); sep = "|";}
    if (type & h2m_atv_access_freq_high)                {ret += sprintf(ret, "%sh2m_atv_access_freq_high", sep); sep = "|";}
    if (type & h2m_atv_access_pattern_linear)           {ret += sprintf(ret, "%sh2m_atv_access_pattern_linear", sep); sep = "|";}
    if (type & h2m_atv_access_pattern_strided)          {ret += sprintf(ret, "%sh2m_atv_access_pattern_strided", sep); sep = "|";}
    if (type & h2m_atv_access_pattern_random)           {ret += sprintf(ret, "%sh2m_atv_access_pattern_random", sep); sep = "|";}
    if (type & h2m_atv_access_pattern_stencil)          {ret += sprintf(ret, "%sh2m_atv_access_pattern_stencil", sep); sep = "|";}
    if (type & h2m_atv_structure_type_array)            {ret += sprintf(ret, "%sh2m_atv_structure_type_array", sep); sep = "|";}
    if (type & h2m_atv_structure_type_matrix)           {ret += sprintf(ret, "%sh2m_atv_structure_type_matrix", sep); sep = "|";}
    if (type & h2m_atv_structure_type_tree)             {ret += sprintf(ret, "%sh2m_atv_structure_type_tree", sep); sep = "|";}
    if (type & h2m_atv_access_origin_single_threaded)   {ret += sprintf(ret, "%sh2m_atv_access_origin_single_threaded", sep); sep = "|";}
    if (type & h2m_atv_access_origin_multi_threaded)    {ret += sprintf(ret, "%sh2m_atv_access_origin_multi_threaded", sep); sep = "|";}
    if (type & h2m_atv_access_origin_multi_threaded_different_nodes) {ret += sprintf(ret, "%sh2m_atv_access_origin_multi_threaded_different_nodes", sep); sep = "|";}
    if (type & h2m_atv_mem_partition_first_touch)       {ret += sprintf(ret, "%sh2m_atv_mem_partition_first_touch", sep); sep = "|";}
    if (type & h2m_atv_mem_partition_close)             {ret += sprintf(ret, "%sh2m_atv_mem_partition_close", sep); sep = "|";}
    if (type & h2m_atv_mem_partition_interleaved)       {ret += sprintf(ret, "%sh2m_atv_mem_partition_interleaved", sep); sep = "|";}
    if (type & h2m_atv_mem_partition_blocked)           {ret += sprintf(ret, "%sh2m_atv_mem_partition_blocked", sep); sep = "|";}
}

typedef struct h2m_alloc_trait_t {
    h2m_alloc_trait_key_t key;
    union h2m_atv_union_t {
        int i;
        unsigned int ui;
        double dbl;
        long l;
        size_t sz;
        h2m_alloc_trait_value_t atv;
        void *ptr;
    } value;
} h2m_alloc_trait_t;

typedef struct h2m_placement_decision_t {
    size_t                  alignment;
    h2m_alloc_trait_value_t mem_space;
    h2m_alloc_trait_key_t   mem_prio;
    h2m_alloc_trait_value_t mem_partition;
    size_t                  mem_blocksize_kb;
} h2m_placement_decision_t;

typedef struct h2m_movement_info_t {
    void *ptr;
    size_t size;
    h2m_placement_decision_t *pl;
    unsigned long nodemask;
    int cpu_numa_node;
} h2m_movement_info_t;

typedef struct h2m_alloc_info_t {
    void *ptr;
    size_t size;
    h2m_alloc_trait_value_t cur_mem_space;
    h2m_alloc_trait_value_t cur_mem_partition;
    unsigned long nodemask;
    int allocated_from_cpu_node;
    int n_traits;
    h2m_alloc_trait_t *traits;
    double last_updated;
    int json_alloc_id;
} h2m_alloc_info_t;

// opaque type representing a declared allocation
typedef struct h2m_declaration_instance_t* h2m_declaration_t;

// function type that can be used for data initialization of a buffer
typedef void (*h2m_fcn_data_init_t) (
    void *ptr,
    void *args
);

// opaque type representing a migration request
typedef struct h2m_migration_req_instance_t* h2m_migration_req_t;

#ifdef __cplusplus
extern "C" {
#endif

//================================================================================
// Init / Finalize
//================================================================================

int h2m_init();
int h2m_thread_init();
int h2m_post_init_serial();
int h2m_thread_finalize();
int h2m_finalize();

//================================================================================
// High-level memory allocation & migration
//================================================================================

int   h2m_verify_traits(int n_traits, const h2m_alloc_trait_t traits[]);
void* h2m_alloc(size_t size, int *err);
void* h2m_alloc_w_traits(size_t size, int *err, int n_traits, const h2m_alloc_trait_t traits[]);
void* h2m_alloc_w_traits_file(size_t size, int callsite_id, int *err);
int   h2m_free(void* ptr);
int   h2m_update_traits(void *ptr, size_t size, int n_traits, const h2m_alloc_trait_t traits[], int replace_complete);
int   h2m_apply_migration();
int   h2m_apply_migration_for_allocations(void** allocations, int n_allocs);
int   h2m_apply_migration_async(h2m_migration_req_t *out_req);
int   h2m_apply_migration_for_allocations_async(void** allocations, int n_allocs, h2m_migration_req_t *out_req);
int   h2m_wait(h2m_migration_req_t req);
int   h2m_is_req_completed(h2m_migration_req_t req);
int   h2m_migration_req_free(h2m_migration_req_t req);
int   h2m_phase_transition(const char* name);

h2m_declaration_t h2m_declare_alloc(void** ptr, size_t size, h2m_fcn_data_init_t fcn_data_init, void *fcn_args, int *err);
h2m_declaration_t h2m_declare_alloc_w_traits(void** ptr, size_t size, h2m_fcn_data_init_t fcn_data_init, void *fcn_args, int *err, int n_traits, const h2m_alloc_trait_t traits[]);
int               h2m_commit_allocs(const h2m_declaration_t handles[], size_t n_handles);
int               h2m_declaration_free(h2m_declaration_t handle);

h2m_placement_decision_t h2m_call_allocation_strategy(size_t size, int n_traits, const h2m_alloc_trait_t traits[], int *err);

//================================================================================
// Low-level memory allocation & migration
//================================================================================

void* h2m_alloc_on_nodes(size_t size, h2m_placement_decision_t pl, unsigned long nodemask, int *err);
void* h2m_alloc_on_nodes_w_traits(size_t size, h2m_placement_decision_t pl, unsigned long nodemask, int *err, int n_traits, const h2m_alloc_trait_t traits[]);
int   h2m_move_memory_mbind(const h2m_movement_info_t* m_info, int n_movements);
int   h2m_move_memory_movepages(const h2m_movement_info_t* m_info, int n_movements);
h2m_placement_decision_t h2m_apply_prescriptive_traits(h2m_placement_decision_t orig, int n_traits, const h2m_alloc_trait_t traits[]);

//================================================================================
// Getters
//================================================================================

const char*   h2m_get_version();
int*          h2m_get_nodes_for_mem_space(h2m_alloc_trait_value_t mem_space, int *num_nodes);
unsigned long h2m_get_nodemask_for_mem_space(h2m_alloc_trait_value_t mem_space);
h2m_placement_decision_t h2m_get_default_placement();
h2m_alloc_info_t         h2m_get_alloc_info(void *ptr, int *out_found);

//================================================================================
// Util functions
//================================================================================

void        h2m_print(int print_prefix, const char *prefix, int rank, int lvl, ... );
void        h2m_set_proc_cpuset(cpu_set_t mask);
void        h2m_set_tracing_enabled(int enabled);

#ifdef __cplusplus
}
#endif

#endif // __H2M_H__
