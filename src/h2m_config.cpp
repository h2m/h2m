#include "h2m.h"
#include "h2m_common_internal.h"
#include "h2m_common.h"
#include "h2m_config.h"

#include <bitset>
#include <cstdlib>

// ============================================================ 
// Config values defined through environment variables
// ============================================================
std::atomic<int>     H2M_VERBOSITY_LVL(10);
std::atomic<int>     H2M_PRINT_CONFIG_VALUES(0);
std::atomic<int>     H2M_PRINT_MEM_CHARACTERISTICS(0);
std::atomic<int>     H2M_PRINT_AFFINITY_MASKS(0);
std::atomic<int>     H2M_PRINT_STATISTICS(0);
std::atomic<int>     H2M_PRINT_PHASE_TIMES(0);
std::atomic<int>     H2M_PRINT_ALLOCATION_ERRORS(0);
std::atomic<int>     H2M_PRINT_CALLSTACK_OFFSETS(0);
std::atomic<int>     H2M_PRINT_CALLSTACK_OFFSETS_MIN_SIZE_BYTES(2000000);
std::atomic<int>     H2M_PRINT_JSON_ALLOCATION_WARNINGS(1);

std::atomic<char*>   H2M_JSON_TRAIT_FILE(nullptr);
std::atomic<int>     H2M_JSON_USE_ID(1);
std::atomic<char*>   H2M_DUMP_DIR(nullptr);
std::atomic<int>     H2M_MAX_PHASE_TRANSITIONS(10240);
std::atomic<char*>   H2M_STATS_FILE_PREFIX(nullptr);
std::atomic<int>     H2M_THREAD_SLEEP_TIME_MICRO_SECS(2);
std::atomic<int>     H2M_OMP_NUM_THREADS(1);
std::atomic<int>     H2M_MIGRATION_ENABLED(1);
std::atomic<int>     H2M_NUM_MIGRATION_THREADS(1);
std::atomic<int>     H2M_RELAXED_JSON_SEARCH_ENABLED(1);
std::atomic<int>     H2M_BACKGROUND_THREAD_ENABLED(1);
std::atomic<int>     H2M_BACKGROUND_THREAD_PIN_MODE(0);
std::atomic<int>     H2M_BACKGROUND_THREAD_PIN_CORE(0);
std::atomic<char*>   H2M_FORCED_ALLOC_MEM_SPACE(nullptr);
std::atomic<char*>   H2M_DEFAULT_MEM_SPACE(nullptr);

std::atomic<int>     H2M_MAX_MEM_CAP_OVERALL_MB_HBW(INT_MAX);
std::atomic<int>     H2M_MAX_MEM_CAP_OVERALL_MB_LOW_LAT(INT_MAX);
std::atomic<int>     H2M_MAX_MEM_CAP_OVERALL_MB_LARGE_CAP(INT_MAX);

// used to be able to specify / overwrite mapping
std::atomic<char*>   H2M_NODES_HBW(nullptr);
std::atomic<char*>   H2M_NODES_LOW_LAT(nullptr);
std::atomic<char*>   H2M_NODES_LARGE_CAP(nullptr);

std::atomic<int>     H2M_FORCED_ALLOC_MEM_SPACE_INT(-1);
std::atomic<int>     H2M_DEFAULT_MEM_SPACE_INT(-1);

// path to directory with config files that contain lookup tables for system's memory characterstics
std::atomic<char*>   H2M_LOOKUP_DIR(nullptr);

// if MPI is used get the rank
std::atomic<int>     H2M_RANK(0);

// ================================================================================
// Functions
// ================================================================================
void load_config_values() {
    char *tmp = nullptr;
    
    // Intel MPI
    tmp = nullptr;
    tmp = std::getenv("PMI_RANK");
    if(tmp) {
        H2M_RANK = std::atoi(tmp);
    }

    // OpenMPI
    tmp = nullptr;
    tmp = std::getenv("OMPI_COMM_WORLD_RANK");
    if(tmp) {
        H2M_RANK = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("OMP_NUM_THREADS");
    if(tmp) {
        H2M_OMP_NUM_THREADS = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_MIGRATION_ENABLED");
    if(tmp) {
        H2M_MIGRATION_ENABLED = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_NUM_MIGRATION_THREADS");
    if(tmp) {
        H2M_NUM_MIGRATION_THREADS = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_RELAXED_JSON_SEARCH_ENABLED");
    if(tmp) {
        H2M_RELAXED_JSON_SEARCH_ENABLED = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_BACKGROUND_THREAD_ENABLED");
    if(tmp) {
        H2M_BACKGROUND_THREAD_ENABLED = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_BACKGROUND_THREAD_PIN_MODE");
    if(tmp) {
        H2M_BACKGROUND_THREAD_PIN_MODE = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_BACKGROUND_THREAD_PIN_CORE");
    if(tmp) {
        H2M_BACKGROUND_THREAD_PIN_CORE = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_VERBOSITY_LVL");
    if(tmp) {
        H2M_VERBOSITY_LVL = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_PRINT_CONFIG_VALUES");
    if(tmp) {
        H2M_PRINT_CONFIG_VALUES = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_PRINT_MEM_CHARACTERISTICS");
    if(tmp) {
        H2M_PRINT_MEM_CHARACTERISTICS = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_PRINT_AFFINITY_MASKS");
    if(tmp) {
        H2M_PRINT_AFFINITY_MASKS = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_PRINT_STATISTICS");
    if(tmp) {
        H2M_PRINT_STATISTICS = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_PRINT_PHASE_TIMES");
    if(tmp) {
        H2M_PRINT_PHASE_TIMES = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_PRINT_ALLOCATION_ERRORS");
    if(tmp) {
        H2M_PRINT_ALLOCATION_ERRORS = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_PRINT_CALLSTACK_OFFSETS");
    if(tmp) {
        H2M_PRINT_CALLSTACK_OFFSETS = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_PRINT_CALLSTACK_OFFSETS_MIN_SIZE_BYTES");
    if(tmp) {
        H2M_PRINT_CALLSTACK_OFFSETS_MIN_SIZE_BYTES = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_PRINT_JSON_ALLOCATION_WARNINGS");
    if(tmp) {
        H2M_PRINT_JSON_ALLOCATION_WARNINGS = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_FORCED_ALLOC_MEM_SPACE");
    if(tmp) {
        H2M_FORCED_ALLOC_MEM_SPACE = tmp;
        if (strcmp(tmp, "HBW") == 0) {
            H2M_FORCED_ALLOC_MEM_SPACE_INT = h2m_atv_mem_space_hbw;
        } else if (strcmp(tmp, "LOW_LAT") == 0) {
            H2M_FORCED_ALLOC_MEM_SPACE_INT = h2m_atv_mem_space_low_lat;
        } else if (strcmp(tmp, "LARGE_CAP") == 0) {
            H2M_FORCED_ALLOC_MEM_SPACE_INT = h2m_atv_mem_space_large_cap;
        } else {
            RELP("WARNING: Invalid value for H2M_FORCED_ALLOC_MEM_SPACE: \"%s\". Skipping ...\n", tmp);
            H2M_FORCED_ALLOC_MEM_SPACE = nullptr;
        }
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_DEFAULT_MEM_SPACE");
    if(tmp) {
        H2M_DEFAULT_MEM_SPACE = tmp;
        if (strcmp(tmp, "HBW") == 0) {
            H2M_DEFAULT_MEM_SPACE_INT = h2m_atv_mem_space_hbw;
        } else if (strcmp(tmp, "LOW_LAT") == 0) {
            H2M_DEFAULT_MEM_SPACE_INT = h2m_atv_mem_space_low_lat;
        } else if (strcmp(tmp, "LARGE_CAP") == 0) {
            H2M_DEFAULT_MEM_SPACE_INT = h2m_atv_mem_space_large_cap;
        } else {
            RELP("WARNING: Invalid value for H2M_DEFAULT_MEM_SPACE: \"%s\". Skipping ...\n", tmp);
            H2M_DEFAULT_MEM_SPACE = nullptr;
        }
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_THREAD_SLEEP_TIME_MICRO_SECS");
    if(tmp) {
        H2M_THREAD_SLEEP_TIME_MICRO_SECS = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_JSON_TRAIT_FILE");
    if(tmp) {
        H2M_JSON_TRAIT_FILE = tmp;
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_JSON_USE_ID");
    if(tmp) {
        H2M_JSON_USE_ID = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_DUMP_DIR");
    if(tmp) {
        H2M_DUMP_DIR = tmp;
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_MAX_PHASE_TRANSITIONS");
    if(tmp) {
        H2M_MAX_PHASE_TRANSITIONS = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_STATS_FILE_PREFIX");
    if(tmp) {
        H2M_STATS_FILE_PREFIX = tmp;
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_MAX_MEM_CAP_OVERALL_MB_HBW");
    if(tmp) {
        H2M_MAX_MEM_CAP_OVERALL_MB_HBW = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_MAX_MEM_CAP_OVERALL_MB_LOW_LAT");
    if(tmp) {
        H2M_MAX_MEM_CAP_OVERALL_MB_LOW_LAT = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_MAX_MEM_CAP_OVERALL_MB_LARGE_CAP");
    if(tmp) {
        H2M_MAX_MEM_CAP_OVERALL_MB_LARGE_CAP = std::atoi(tmp);
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_NODES_HBW");
    if(tmp) {
        H2M_NODES_HBW = tmp;
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_NODES_LOW_LAT");
    if(tmp) {
        H2M_NODES_LOW_LAT = tmp;
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_NODES_LARGE_CAP");
    if(tmp) {
        H2M_NODES_LARGE_CAP = tmp;
    }

    tmp = nullptr;
    tmp = std::getenv("H2M_LOOKUP_DIR");
    if(tmp) {
        H2M_LOOKUP_DIR = tmp;
    }
}

void print_config_values() {
    RELP("H2M_VERBOSITY_LVL=%d\n", H2M_VERBOSITY_LVL.load());
    RELP("H2M_PRINT_CONFIG_VALUES=%d\n", H2M_PRINT_CONFIG_VALUES.load());
    RELP("H2M_PRINT_MEM_CHARACTERISTICS=%d\n", H2M_PRINT_MEM_CHARACTERISTICS.load());
    RELP("H2M_PRINT_AFFINITY_MASKS=%d\n", H2M_PRINT_AFFINITY_MASKS.load());
    RELP("H2M_PRINT_STATISTICS=%d\n", H2M_PRINT_STATISTICS.load());
    RELP("H2M_PRINT_PHASE_TIMES=%d\n", H2M_PRINT_PHASE_TIMES.load());
    RELP("H2M_PRINT_ALLOCATION_ERRORS=%d\n", H2M_PRINT_ALLOCATION_ERRORS.load());
    RELP("H2M_PRINT_CALLSTACK_OFFSETS=%d\n", H2M_PRINT_CALLSTACK_OFFSETS.load());
    RELP("H2M_PRINT_CALLSTACK_OFFSETS_MIN_SIZE_BYTES=%d\n", H2M_PRINT_CALLSTACK_OFFSETS_MIN_SIZE_BYTES.load());
    RELP("H2M_PRINT_JSON_ALLOCATION_WARNINGS=%d\n", H2M_PRINT_JSON_ALLOCATION_WARNINGS.load());
    RELP("H2M_RANK=%d\n", H2M_RANK.load());
    RELP("H2M_OMP_NUM_THREADS=%d\n", H2M_OMP_NUM_THREADS.load());
    RELP("H2M_THREAD_SLEEP_TIME_MICRO_SECS=%d\n", H2M_THREAD_SLEEP_TIME_MICRO_SECS.load());
    RELP("H2M_MIGRATION_ENABLED=%d\n", H2M_MIGRATION_ENABLED.load());
    RELP("H2M_NUM_MIGRATION_THREADS=%d\n", H2M_NUM_MIGRATION_THREADS.load());
    RELP("H2M_RELAXED_JSON_SEARCH_ENABLED=%d\n", H2M_RELAXED_JSON_SEARCH_ENABLED.load());
    RELP("H2M_BACKGROUND_THREAD_ENABLED=%d\n", H2M_BACKGROUND_THREAD_ENABLED.load());
    RELP("H2M_BACKGROUND_THREAD_PIN_MODE=%d\n", H2M_BACKGROUND_THREAD_PIN_MODE.load());
    RELP("H2M_BACKGROUND_THREAD_PIN_CORE=%d\n", H2M_BACKGROUND_THREAD_PIN_CORE.load());
    RELP("H2M_MAX_PHASE_TRANSITIONS=%d\n", H2M_MAX_PHASE_TRANSITIONS.load());

    if(H2M_JSON_TRAIT_FILE.load()) {
        RELP("H2M_JSON_TRAIT_FILE=%s\n", H2M_JSON_TRAIT_FILE.load());
    }
    RELP("H2M_JSON_USE_ID=%d\n", H2M_JSON_USE_ID.load());
    if(H2M_DUMP_DIR.load()) {
        RELP("H2M_DUMP_DIR=%s\n", H2M_DUMP_DIR.load());
    }

    if(H2M_STATS_FILE_PREFIX.load()) {
        RELP("H2M_STATS_FILE_PREFIX=%s\n", H2M_STATS_FILE_PREFIX.load());
    }

    RELP("H2M_MAX_MEM_CAP_OVERALL_MB_HBW=%d\n", H2M_MAX_MEM_CAP_OVERALL_MB_HBW.load());
    RELP("H2M_MAX_MEM_CAP_OVERALL_MB_LOW_LAT=%d\n", H2M_MAX_MEM_CAP_OVERALL_MB_LOW_LAT.load());
    RELP("H2M_MAX_MEM_CAP_OVERALL_MB_LARGE_CAP=%d\n", H2M_MAX_MEM_CAP_OVERALL_MB_LARGE_CAP.load());

    h2m_alloc_trait_value_t mem_spaces[3] = {
        h2m_atv_mem_space_hbw,
        h2m_atv_mem_space_large_cap,
        h2m_atv_mem_space_low_lat
    };

    for(int i = 0; i < 3; i++) {
        h2m_alloc_trait_value_t cur_ms = mem_spaces[i];
        if(__mapping_mem_space_nodemask.count(cur_ms)) {
            unsigned long nodemask = __mapping_mem_space_nodemask[cur_ms];
            size_t tmp_max_cap = __mapping_nodemask_max_mem_cap[nodemask];
            char tmp_str[64];
            h2m_alloc_trait_value_t_str(cur_ms, tmp_str);
            RELP("Max Capacity: mem_space=%s (nodemask=%s) -> max_capacity_bytes=%zu\n", tmp_str, std::bitset<MAX_NUMA_NODES+2>(nodemask).to_string().c_str(), tmp_max_cap);
        }
    }

    if(H2M_NODES_HBW.load()) {
        RELP("H2M_NODES_HBW=%s\n", H2M_NODES_HBW.load());
    }
    if(H2M_NODES_LOW_LAT.load()) {
        RELP("H2M_NODES_LOW_LAT=%s\n", H2M_NODES_LOW_LAT.load());
    }
    if(H2M_NODES_LARGE_CAP.load()) {
        RELP("H2M_NODES_LARGE_CAP=%s\n", H2M_NODES_LARGE_CAP.load());
    }
    if(H2M_FORCED_ALLOC_MEM_SPACE.load()) {
        RELP("H2M_FORCED_ALLOC_MEM_SPACE=%s\n", H2M_FORCED_ALLOC_MEM_SPACE.load());
        char tmp[50];
        h2m_alloc_trait_value_t_str((h2m_alloc_trait_value_t)H2M_FORCED_ALLOC_MEM_SPACE_INT.load(), tmp);
        RELP("H2M_FORCED_ALLOC_MEM_SPACE_INT=%s\n", tmp);
    }
    if(H2M_DEFAULT_MEM_SPACE.load()) {
        RELP("H2M_DEFAULT_MEM_SPACE=%s\n", H2M_DEFAULT_MEM_SPACE.load());
        char tmp[50];
        h2m_alloc_trait_value_t_str((h2m_alloc_trait_value_t)H2M_DEFAULT_MEM_SPACE_INT.load(), tmp);
        RELP("H2M_DEFAULT_MEM_SPACE_INT=%s\n", tmp);
    }
    if(H2M_LOOKUP_DIR.load()) {
        RELP("H2M_LOOKUP_DIR=%s\n", H2M_LOOKUP_DIR.load());
    }
}
