#ifndef __H2M_PRE_INIT_H__
#define __H2M_PRE_INIT_H__

#include <h2m.h>

#ifdef __GNUG__
// remeber original cpuset for the complete process
cpu_set_t proc_mask;
void h2m_preinit_hook(int argc, char **argv, char **envp) {
    sched_getaffinity(getpid(), sizeof(cpu_set_t), &proc_mask);
}
__attribute__((section(".preinit_array"))) __typeof__(h2m_preinit_hook) *__preinit = h2m_preinit_hook;

void h2m_pre_init() {
    h2m_set_proc_cpuset(proc_mask);
}
#else // __GNUG__
void h2m_pre_init();
#endif // __GNUG__

#endif // __H2M_PRE_INIT_H__
