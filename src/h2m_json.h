#ifndef __H2M_JSON_H__
#define __H2M_JSON_H__

#include "h2m.h"

#include <atomic>
#include <map>
#include <mutex>
#include <string>
#include <vector>
#include <nlohmann/json.hpp>
#include "iostream"
using json = nlohmann::json;

// ========================================
// === Classes and type definitions
// ========================================
// class MemorySpec{
//     public :
//     JsonConfigMemorySpace* memory_space_;
//     JsonConfigCopyBandwidth* copy_bandwidth_;

//     public :
//     MemorySpec(const json& data);

// };
class JsonConfigMemorySpace {
    
    public:    
    // Constructor that parses data from json file.
    JsonConfigMemorySpace(const json& data);
    
    // Emtpy construcor.
    JsonConfigMemorySpace()
    :
        idx_(-1),
        name_(" "),
        numa_domain_(),
        is_mem_space_low_lat_(false),
        is_mem_space_hbw_(false),
        is_mem_space_large_cap_(false),
        capacity_MB_(0.0),
        scaling_threads_(),
        scaling_bandwidth_read_only_MBs_(),
        scaling_bandwidth_read_write_MBs_(),
        scaling_latency_read_only_ns_(),
        scaling_latency_read_only_bandwidth_disturbance_MBs_(),
        scaling_latency_read_write_ns_(),
        scaling_latency_read_write_bandwidth_disturbance_MBs_()
    { }
    // Internal functionalities
    void Print() const;

        private:

        int idx_;
        std::string name_;
        std::vector<int> numa_domain_;
        bool is_mem_space_low_lat_;
        bool is_mem_space_hbw_;
        bool is_mem_space_large_cap_;
        double capacity_MB_;
        std::vector<int> scaling_threads_;
        std::vector<double> scaling_bandwidth_read_only_MBs_;
        std::vector<double> scaling_bandwidth_read_write_MBs_;
        std::vector<double> scaling_latency_read_only_ns_;
        std::vector<double> scaling_latency_read_only_bandwidth_disturbance_MBs_;
        std::vector<double> scaling_latency_read_write_ns_;
        std::vector<double> scaling_latency_read_write_bandwidth_disturbance_MBs_;
};

class JsonConfigCopyBandwidth {
    public:

    int idx_from_;
    int idx_to_;
    std::vector<int> size_bytes_;
    std::vector<double> bandwidth_MBs_;

    public:
    // Constructor that parses data from json file.
    JsonConfigCopyBandwidth(const json& data);  

    // Emtpy construcor.
    JsonConfigCopyBandwidth() :
        idx_from_(-1),
        idx_to_(-1),
        size_bytes_(),
        bandwidth_MBs_()
    { }
}; 

class JsonAllocation {
    public:
        int id;
        int group_id;
        double size_mb;
        double date_alloc_ms;
        double date_dealloc_ms;

        // empty constructor
        JsonAllocation(): id(-1), group_id(-1), size_mb(0), date_alloc_ms(0), date_dealloc_ms(0) { }
        // constructor that is parsing object from json
        JsonAllocation(const json& data);
};

class JsonAllocationGroup {
    public:
        int id;
        int callsite_group_id;
        int callstack_group_id;
        std::string callstack_rip;
        std::string callstack_offsets;
        std::string callstack_offsets_clean;
        std::string callsite_rip;
        std::string callsite_str;
        size_t sum_size_mb;

        // potentiall not interesting -> not parsed right now
        // double min_alloc_date_ms;
        // double max_dealloc_date_ms;
        
        // selecting allocation traits based on allocation order
        std::mutex mtx;
        int ctr;
        
        // list of all associated allocations (sorted by allocation data)
        std::vector<JsonAllocation*> allocations;

        // empty constructor
        JsonAllocationGroup(): id(-1), callsite_group_id(-1), callstack_group_id(-1),
            callstack_rip(""), callstack_offsets(""), callstack_offsets_clean(""), callsite_rip(""),
            callsite_str(""), ctr(0), sum_size_mb(0) { }
        // constructor that is parsing object from json
        JsonAllocationGroup(const json& data);
};

class JsonTraitEntry {
    public:
        int allocation_id;
        bool prefetch;
        std::vector<h2m_alloc_trait_t> traits;

        // for now: simpler accessibility to check current space assignment
        h2m_alloc_trait_value_t desired_mem_space;

        // empty constructor
        JsonTraitEntry(): allocation_id(-1), prefetch(false) { }
        // constructor that is parsing object from json
        JsonTraitEntry(const json& data);
};

class JsonPhase {
    public:
        int id;
        std::string name;
        double ts_ms_start;
        double ts_ms_end;
        double duration_ms;
        int id_trans_start;
        int id_trans_end;

        std::vector<JsonTraitEntry> traits;

        // empty constructor
        JsonPhase(): id(-1), name(""), ts_ms_start(0), ts_ms_end(0), duration_ms(0), id_trans_start(-1), id_trans_end(-1) { }
        // constructor that is parsing object from json
        JsonPhase(const json& data);
};

// ========================================
// === Variables
// ========================================
extern std::atomic<bool>                    json_flag_available;
extern std::map<int, JsonPhase*>            json_phases;
extern std::map<int, JsonAllocationGroup*>  json_allocation_groups;
extern std::map<int, JsonAllocation*>       json_allocations;

// ========================================
// === Functions
// ========================================

// read string true or false from json and return as boolean
static bool json_get(const json& j, const char* entry, const bool fallback) {
    if (j.contains(entry)) {
        auto& e = j[entry];
        if (e.is_string() && e == "true") {
            return true;
        }
        else if (e.is_string() && e == "false") {
            return false;
        }
    }
    return fallback;
}

static std::string json_get(const json& j, const char* entry, const char* fallback) {
    if (j.contains(entry)) {
        auto& e = j[entry];
        if (e.is_string()) {
            return e.get<std::string>();
        }
    }
    return fallback;
}

static double json_get(const json& j, const char* entry, const double fallback) {
    if (j.contains(entry)) {
        auto& e = j[entry];
        if (e.is_number()) {
            return e.get<double>();
        } else {
            printf("!!! Double: e is not a number %s\n", entry);
        }
    }
    return fallback;
}

static int json_get(const json& j, const char* entry, const int fallback) {
    if (j.contains(entry)) {
        auto& e = j[entry];
        if (e.is_number_integer()) {
            return e.get<int>();
        }
    }
    return fallback;
}

static std::vector<int> json_get(const json& j, const char* entry, std::vector<int> fallback) {
    if (j.contains(entry)) {
        std::vector<int> out_vect;
        auto& e = j[entry];
        if (e.is_array()) {
            for (int i = 0; i < e.size(); i++) {
                out_vect.push_back(e[i]);
            }
            return out_vect;
        }
    }
    return fallback;
}

static std::vector<double> json_get(const json& j, const char* entry, const std::vector<double> fallback) {
    if (j.contains(entry)) {
        std::vector<double> out_vect;
        auto& e = j[entry];
        if (e.is_array()) {
            for (int i = 0; i < e.size(); i++) {
                out_vect.push_back(e[i]);
            }
            return out_vect;
        }
    }
    return fallback;
}

// Load the json containing Bandwidth, Latency of the Memory device.
int parse_json_config(const char* file_path,
    std::vector<JsonConfigMemorySpace*>&,
    std::vector<JsonConfigCopyBandwidth*>&);

void parse_json_traits(const char* file_path);

void cleanup_json_traits();

void cleanup_json_config(
    std::vector<JsonConfigMemorySpace*>&,
    std::vector<JsonConfigCopyBandwidth*>&);

h2m_alloc_trait_t* get_json_traits_for_allocation(int phase_id, int callsite_id, 
    std::string callstack_offsets_clean, bool use_id,
    int* json_alloc_id, int* n_traits, bool* prefetch);
#endif // __H2M_JSON_H__