#include "h2m_statistics.h"
#include "h2m_common_internal.h"
#include "h2m_common.h"
#include "h2m_config.h"

#include <string>
#include <sstream>
#include <float.h>

MinMaxAvgStats::MinMaxAvgStats(std::string name, std::string unit) {
    stat_name = name;
    unit_str = unit;
    reset();
}

void MinMaxAvgStats::reset() {
    mtx_priv.lock();
    count = 0;
    has_values = false;
    val_sum = 0.0;
    val_min = 0.0;
    val_max = 0.0;
    val_avg = 0.0;
    mtx_priv.unlock();
}

void MinMaxAvgStats::add_stat_value(double val) {
    mtx_priv.lock();
    if (!has_values.load()) {
        // set mix max at first call
        val_min = DBL_MAX;
        val_max = DBL_MIN;
        has_values = true;
    }

    if(val > val_max) {
        val_max = val;
    }
    if(val < val_min) {
        val_min = val;
    }

    // add up
    val_sum = val_sum + val;

    if(count == 0)
    {
        val_avg = val;
    } else {
        // calculate avg throughput
        double cur_num = (double) count.load();
        val_avg = (val_avg.load() * cur_num + (double) val) / (cur_num+1);
    }
    count++;
    mtx_priv.unlock();
}

void MinMaxAvgStats::print_stats(FILE *cur_file) {
    fprintf(cur_file, "Stats R#%d:\t%s_sum (%s)\t%.12f\n", H2M_RANK.load(), stat_name.c_str(), unit_str.c_str(), val_sum.load());
    fprintf(cur_file, "Stats R#%d:\t%s_count\t%d\n", H2M_RANK.load(), stat_name.c_str(), count.load());
    fprintf(cur_file, "Stats R#%d:\t%s_min (%s)\t%.12f\n", H2M_RANK.load(), stat_name.c_str(), unit_str.c_str(), val_min.load());
    fprintf(cur_file, "Stats R#%d:\t%s_max (%s)\t%.12f\n", H2M_RANK.load(), stat_name.c_str(), unit_str.c_str(), val_max.load());
    fprintf(cur_file, "Stats R#%d:\t%s_avg (%s)\t%.12f\n", H2M_RANK.load(), stat_name.c_str(), unit_str.c_str(), val_avg.load());
}

MinMaxAvgStats       __stats_time_init_finalize("time_init_finalize", "sec");
MinMaxAvgStats       __stats_time_get_traits_from_file("time_get_traits_from_file", "sec");
MinMaxAvgStats       __stats_time_get_callstack_info("time_get_callstack_info", "sec");
MinMaxAvgStats       __stats_time_allocate("time_allocate", "sec");
MinMaxAvgStats       __stats_time_declare_allocations("time_declare_allocations", "sec");
MinMaxAvgStats       __stats_time_commit_allocations("time_commit_allocations", "sec");
MinMaxAvgStats       __stats_time_apply_migration_data_collection("time_apply_migration_data_collection", "sec");
MinMaxAvgStats       __stats_time_apply_migration_alloc_data_collection("time_apply_migration_alloc_data_collection", "sec");
MinMaxAvgStats       __stats_time_allocation_strategy("time_allocation_strategy", "sec");
MinMaxAvgStats       __stats_time_commit_strategy("time_commit_strategy", "sec");
MinMaxAvgStats       __stats_time_migration_strategy("time_migration_strategy", "sec");
MinMaxAvgStats       __stats_time_migration_inner("time_migration_inner", "sec");
MinMaxAvgStats       __stats_time_move_mbind("time_move_mbind", "sec");
MinMaxAvgStats       __stats_time_move_movepages("time_move_movepages", "sec");
MinMaxAvgStats       __stats_time_select_nodemask("time_select_nodemask", "sec");
MinMaxAvgStats       __stats_time_wait("time_wait", "sec");

// Wrapper for C++
void h2m_stats_print_w_mean(FILE *cur_file, std::string name, double sum, int count, bool cummulative) {
    h2m_stats_print_w_mean(cur_file, name.c_str(), sum, count, cummulative);
}

extern "C" {

void h2m_stats_print_w_mean(FILE *cur_file, const char* name, double sum, int count, bool cummulative) {
    std::string prefix = "Stats";
    if(cummulative)
        prefix = "Cumulative Stats";    

    if(count <= 0) {
        fprintf(cur_file, "%s R#%d:\t%s\tsum=\t%.10f\tcount=\t%d\tmean=\t%d\n", prefix.c_str(), 0, name, sum, count, 0);
    } else {
        fprintf(cur_file, "%s R#%d:\t%s\tsum=\t%.10f\tcount=\t%d\tmean=\t%.10f\n", prefix.c_str(), 0, name, sum, count, (sum / (double)count));
    }
}

void h2m_stats_print() {
    FILE *cur_file = stderr;
    char *file_prefix = H2M_STATS_FILE_PREFIX.load();
    char tmp_file_name[255];

    if(file_prefix) {
        // initial string
        strcpy(tmp_file_name, "");
        // append
        strcat(tmp_file_name, file_prefix);
        strcat(tmp_file_name, "_R");
        strcat(tmp_file_name, std::to_string(H2M_RANK.load()).c_str());
        cur_file = fopen(tmp_file_name, "a");
    }

    __stats_time_init_finalize.print_stats(cur_file);
    __stats_time_get_traits_from_file.print_stats(cur_file);
    __stats_time_get_callstack_info.print_stats(cur_file);
    __stats_time_allocate.print_stats(cur_file);
    __stats_time_declare_allocations.print_stats(cur_file);
    __stats_time_commit_allocations.print_stats(cur_file);
    __stats_time_apply_migration_data_collection.print_stats(cur_file);
    __stats_time_apply_migration_alloc_data_collection.print_stats(cur_file);
    __stats_time_allocation_strategy.print_stats(cur_file);
    __stats_time_commit_strategy.print_stats(cur_file);
    __stats_time_migration_strategy.print_stats(cur_file);
    __stats_time_migration_inner.print_stats(cur_file);
    __stats_time_move_mbind.print_stats(cur_file);
    // __stats_time_move_movepages.print_stats(cur_file);
    __stats_time_select_nodemask.print_stats(cur_file);
    __stats_time_wait.print_stats(cur_file);

    if(file_prefix) {
        fclose(cur_file);
    }
}

}
