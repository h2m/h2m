#ifndef __H2M_CONFIG_H__
#define __H2M_CONFIG_H__

#include <atomic>

// ============================================================ 
// Config values defined through environment variables
// ============================================================
extern std::atomic<int>     H2M_VERBOSITY_LVL;
extern std::atomic<int>     H2M_PRINT_CONFIG_VALUES;
extern std::atomic<int>     H2M_PRINT_MEM_CHARACTERISTICS;
extern std::atomic<int>     H2M_PRINT_AFFINITY_MASKS;
extern std::atomic<int>     H2M_PRINT_STATISTICS;
extern std::atomic<int>     H2M_PRINT_PHASE_TIMES;
extern std::atomic<int>     H2M_PRINT_ALLOCATION_ERRORS;
extern std::atomic<int>     H2M_PRINT_CALLSTACK_OFFSETS;
extern std::atomic<int>     H2M_PRINT_CALLSTACK_OFFSETS_MIN_SIZE_BYTES;
extern std::atomic<int>     H2M_PRINT_JSON_ALLOCATION_WARNINGS;

extern std::atomic<char*>   H2M_JSON_TRAIT_FILE;
extern std::atomic<int>     H2M_JSON_USE_ID;
extern std::atomic<char*>   H2M_DUMP_DIR;
extern std::atomic<int>     H2M_MAX_PHASE_TRANSITIONS;
extern std::atomic<char*>   H2M_STATS_FILE_PREFIX;
extern std::atomic<int>     H2M_THREAD_SLEEP_TIME_MICRO_SECS;
extern std::atomic<int>     H2M_OMP_NUM_THREADS;
extern std::atomic<int>     H2M_MIGRATION_ENABLED;
extern std::atomic<int>     H2M_NUM_MIGRATION_THREADS;
extern std::atomic<int>     H2M_RELAXED_JSON_SEARCH_ENABLED;
extern std::atomic<int>     H2M_BACKGROUND_THREAD_ENABLED;
extern std::atomic<int>     H2M_BACKGROUND_THREAD_PIN_MODE;
extern std::atomic<int>     H2M_BACKGROUND_THREAD_PIN_CORE;
extern std::atomic<char*>   H2M_FORCED_ALLOC_MEM_SPACE;
extern std::atomic<char*>   H2M_DEFAULT_MEM_SPACE;

extern std::atomic<int>     H2M_MAX_MEM_CAP_OVERALL_MB_HBW;
extern std::atomic<int>     H2M_MAX_MEM_CAP_OVERALL_MB_LOW_LAT;
extern std::atomic<int>     H2M_MAX_MEM_CAP_OVERALL_MB_LARGE_CAP;

// used to be able to specify / overwrite mapping
extern std::atomic<char*>   H2M_NODES_HBW;
extern std::atomic<char*>   H2M_NODES_LOW_LAT;
extern std::atomic<char*>   H2M_NODES_LARGE_CAP;

extern std::atomic<int>     H2M_FORCED_ALLOC_MEM_SPACE_INT;
extern std::atomic<int>     H2M_DEFAULT_MEM_SPACE_INT;

// path to directory with config files that contain lookup tables for system's memory characterstics
extern std::atomic<char*>   H2M_LOOKUP_DIR;

// if MPI is used get the rank
extern std::atomic<int>     H2M_RANK;

// ================================================================================
// Functions
// ================================================================================
void  load_config_values();
void  print_config_values();

#endif // __H2M_CONFIG_H__
