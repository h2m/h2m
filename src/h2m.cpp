#include "h2m.h"
#include "h2m_common_internal.h"
#include "h2m_common.h"
#include "h2m_config.h"
#include "h2m_heuristics.h"
#include "h2m_json.h"
#include "h2m_statistics.h"
#include "h2m_tools.h"
#include "h2m_tools_internal.h"
#include "h2m_utils_numa.h"
#include "h2m_version.h"

#ifdef TRACE
#include "VT.h"
#endif

#pragma region Variables
//================================================================================
// Variables
//================================================================================

inline namespace { // ensure variable are not visible outside the file

// flag that tells whether library has already been initialized
std::mutex          __mtx_is_initialized;
std::atomic<bool>   __is_initialized(false);

// migration requests
thread_safe_list_t<h2m_migration_req_t> __migration_requests;

// variables associated with bg thread worklfow
pthread_t           __bg_thread;
std::mutex          __mtx_bg_thread_create;
std::atomic<bool>   __bg_thread_created(false);
std::atomic<bool>   __flag_abort_bg_thread(false);
pthread_cond_t      __bg_thread_cond = PTHREAD_COND_INITIALIZER;
std::atomic<bool>   __bg_thread_running(false);
pthread_mutex_t     __mtx_bg_thread_start = PTHREAD_MUTEX_INITIALIZER;

// phase transitions
h2m_phase_transition_t* __phase_transitions;
std::atomic<int>        __phase_transition_id(0);
std::mutex              __mtx_phase_transition;
h2m_migration_req_t     __req_last_prefetch;

}

#pragma endregion Variables

#ifdef __cplusplus
extern "C" {
#endif

#pragma region Forward declaration
//================================================================================
// Forward declaration of internal functions (just called inside shared library)
//================================================================================
int           __start_bg_thread();
int           __stop_bg_thread();
#pragma endregion Forward declaration

#pragma region Util functions
//================================================================================
// Util functions
//================================================================================

/* 
 * Function verify_initialized
 * Verifies whether library has already been initialized or not.
 * Otherwise it will throw an error. 
 */
static inline void verify_initialized() {
    if(!__is_initialized.load()) {
        throw std::runtime_error("H2M runtime has not been initilized before.");
    }
}

inline bool check_hwloc_version() {
    unsigned version = hwloc_get_api_version();
    if ((version >> 16) != (HWLOC_API_VERSION >> 16)) {
        fprintf(stderr,
                "%s compiled for hwloc API 0x%x but running on library API 0x%x.\n"
                "You may need to point LD_LIBRARY_PATH to the right hwloc library.\n"
                "Aborting since the new ABI is not backward compatible.\n",
                "libh2m", HWLOC_API_VERSION, version);
        return false;
    }
    return true;
}

/* 
 * Function h2m_preinit_ctor
 * Runs before library is initialized.
 * NOTE: Might not work with libgomp: might not run before libgomp constructor 
 *       is executed which already binds threads if OpenMP affinity policy set.
 */
static void __attribute__((constructor (102))) h2m_preinit_ctor(void) {
    // fprintf (stderr,"==> h2m_preinit_ctor\n");
    // first thing: remember original full cpuset of complete process
    sched_getaffinity(getpid(), sizeof(cpu_set_t), &__pid_mask);
}

/* 
 * Function h2m_set_proc_cpuset
 * Possibility to set process cpuset from outside
 * NOTE: Necessary when using GNU compiler + libgomp
 */
void h2m_set_proc_cpuset(cpu_set_t mask) {
    __pid_mask = mask;
}

void h2m_print(int print_prefix, const char *prefix, int rank, int lvl, ... ) {
    va_list args;
    va_start(args, lvl);
    h2m_dbg_print_help(print_prefix, prefix, rank, lvl, args);
    va_end (args);
}

void h2m_set_tracing_enabled(int enabled) {
    #ifdef TRACE
    __tracing_enabled = enabled;
    #endif // TRACE
}

unsigned long select_nodes_for_mem_space(h2m_placement_decision_t *pl, size_t req_size, int cpu_numa_node, bool check_allowed, int *err) {
    DBP(__VLVL_ENTRY_EXIT_VERBOSE, "select_nodes_for_mem_space (enter)\n");
    unsigned long nodemask  = 0;
    
    // NUMA-aware view from corresponding cpu numa domain
    if(cpu_numa_node == -1) {
        cpu_numa_node = numa_node_of_cpu(sched_getcpu());
    }

    // get nodes for desired memory space
    std::vector<int> &l_nodes = __numa_data[cpu_numa_node].mem_space_mapping[pl->mem_space];

    // check whether allocation is allowed (resprecting mem space limits set by user)
    if (check_allowed) {
        bool allowed = check_overall_space_restriction(l_nodes, pl->mem_space, req_size);
        if(!allowed) {
            if (pl->mem_prio == h2m_atk_req_mem_space) {
                // request cannot be fulfilled
                *err = H2M_MEM_LIMIT_EXCEEDED;
                DBP(__VLVL_ENTRY_EXIT_VERBOSE, "select_nodes_for_mem_space (exit)\n");
                return 0;
            } else {
                // fallback to large capactity mem space again
                // char tmp[50];
                // h2m_alloc_trait_value_t_str((h2m_alloc_trait_value_t) pl->mem_space, tmp);
                // RELP("Warning: Request to allocate on %s with size %ld could not be fulfilled due to exceeding limits. Falling back to h2m_atv_mem_space_large_cap.\n", tmp, req_size);
                l_nodes = __numa_data[cpu_numa_node].mem_space_mapping[h2m_atv_mem_space_large_cap];
                pl->mem_space = h2m_atv_mem_space_large_cap;
            }
        }
    }

    // cover memory interelaving
    if(pl->mem_partition == h2m_atv_mem_partition_interleaved || 
       pl->mem_partition == h2m_atv_mem_partition_blocked ||
       pl->mem_partition == h2m_atv_mem_partition_first_touch) {
        nodemask = __mapping_mem_space_nodemask[pl->mem_space];
        *err = H2M_SUCCESS;
        DBP(__VLVL_ENTRY_EXIT_VERBOSE, "select_nodes_for_mem_space (exit)\n");
        return nodemask;
    } else if(pl->mem_partition == h2m_atv_mem_partition_close) {
        // allocate on the closest node (h2m_atv_mem_partition_close)
        // Note: this call is really slow if done several times. Use with caution!!
        std::map<int, size_t> avail_space = get_numa_capacities();
        int node = -1;
        for (int i = 0; i < l_nodes.size(); i++) {
            int cur_node = l_nodes[i];
            if (avail_space[cur_node] > req_size) {
                node = cur_node;
                break;
            }
        }

        if (node == -1) {
            if (pl->mem_prio == h2m_atk_req_mem_space) {
                // request cannot be fulfilled
                *err = H2M_MEM_NO_SPACE_ON_NODES;
                DBP(__VLVL_ENTRY_EXIT_VERBOSE, "select_nodes_for_mem_space (exit)\n");
                return 0;
            } else {
                RELP("WARNING: No single node found fulfilling the requirements: fallback to first touch mode\n");
                nodemask = __mapping_mem_space_nodemask[pl->mem_space];
                pl->mem_partition = h2m_atv_mem_partition_first_touch;
                *err = H2M_SUCCESS;
                DBP(__VLVL_ENTRY_EXIT_VERBOSE, "select_nodes_for_mem_space (exit)\n");
                return nodemask;
            }
        } else {
            nodemask = 1 << node;
            *err = H2M_SUCCESS;
            DBP(__VLVL_ENTRY_EXIT_VERBOSE, "select_nodes_for_mem_space (exit)\n");
            return nodemask;
        }
    }

    // case: identify if situation has not been covered
    RELP("Unsupported/uncovered case detected: mem_partition=%d\n", pl->mem_partition);
    *err = H2M_NOT_COVERED;
    DBP(__VLVL_ENTRY_EXIT_VERBOSE, "select_nodes_for_mem_space (exit)\n");
    return 0;
}

void print_traits(const char* prefix, int n_traits, const h2m_alloc_trait_t traits[]) {
    if (traits) {
        DBP(__VLVL_PRINT_TRAITS, "%s - %d traits:\n", prefix, n_traits);
        for(int i = 0; i < n_traits; i++) {
            h2m_alloc_trait_key_t key = traits[i].key;
            const char *key_str = h2m_alloc_trait_key_t_str[key];
            
            if (key == h2m_atk_mem_alignment || key == h2m_atk_access_prio || key == h2m_atk_access_stride) {
                int val = traits[i].value.i;
                DBP(__VLVL_PRINT_TRAITS, "%s = %d\n", key_str, val);
                // RELP("%d = %d\n", key, val);
            } else {
                char val_str[255];
                h2m_alloc_trait_value_t_str(traits[i].value.atv, val_str);
                DBP(__VLVL_PRINT_TRAITS, "%s = %s\n", key_str, val_str);
            }
        }
    }
}

h2m_migration_req_t __create_default_request() {
    h2m_migration_req_t inst = (h2m_migration_req_instance_t*) FCN_MALLOC(sizeof(h2m_migration_req_instance_t));
    inst->is_completed = 0;
    inst->return_code = H2M_SUCCESS;
    inst->allocations = nullptr;
    inst->n_allocs = -1;
    inst->elapsed_sec = 0;
    return inst;
}

void dump_phase_transitions() {
    char *dump_dir = H2M_DUMP_DIR.load();
    if (!dump_dir) {
        return;
    }

    char tmp_file_name[255];
    strcpy(tmp_file_name, ""); // initial string
    strcat(tmp_file_name, dump_dir);
    strcat(tmp_file_name, PATH_SEPARATOR);
    strcat(tmp_file_name, "h2m_dump_phase_transitions.csv");
    FILE* cur_file = fopen(tmp_file_name, "w+");

    // print entries
    fprintf(cur_file,"ID;Name;TimeStamp\n");
    __mtx_phase_transition.lock();
    for(int i = 0; i < __phase_transition_id.load(); i++) {
        h2m_phase_transition_t pt = __phase_transitions[i];
        fprintf(cur_file,"%d;%s;%" PRIu64 "\n", pt.id, pt.name, pt.ts);
    }
    __mtx_phase_transition.unlock();
    fclose(cur_file);
}

void print_phase_transitions() {
    fprintf(stderr, "Phase;ID;Name;TimeStamp;Duration;NumberMigrations;ElapsedSecMigrations\n");
    __mtx_phase_transition.lock();
    double cur_duration_sec = 0.0;
    int n_transitions = __phase_transition_id.load();
    for(int i = 0; i < n_transitions; i++) {
        h2m_phase_transition_t pt = __phase_transitions[i];
        cur_duration_sec = 0.0;
        if(i < n_transitions-1 ) {
            h2m_phase_transition_t pt_next = __phase_transitions[i+1];
            cur_duration_sec = (double)(pt_next.ts - pt.ts) / 1000.0 / 1000.0 / 1000.0;
        }
        fprintf(stderr, "Phase;%d;%s;%" PRIu64 ";%f;%d;%f\n", pt.id, pt.name, pt.ts, cur_duration_sec, pt.n_migrations, pt.elapsed_sec_migrations);
    }
    __mtx_phase_transition.unlock();
}

void cleanup_phase_transitions() {
    for(int i = 0; i < __phase_transition_id.load(); i++) {
        h2m_phase_transition_t pt   = __phase_transitions[i];
        if(pt.name) {
            // FCN_FREE(pt.name);
            pt.name = nullptr;
        }
    }
    FCN_FREE(__phase_transitions);
}

void init_data_structures() {
    // ensure that base allocation function references are loaded before using them
    if (orig_malloc == nullptr) {
        dl_iterate_phdr(callback_iterate, NULL);
    }

    // init numa specific data structure
    int num_numa = numa_num_configured_nodes();
    __numa_data = new h2m_numa_node_data_t[num_numa];

    init_mem_characteristics();

    // need +X for safty measure to cover also migration threads per NUMA node
    __thread_data = (h2m_thread_data_t*) FCN_MALLOC((4 + omp_get_max_threads()) * sizeof(h2m_thread_data_t));
    
    // init tmp json id to -1
    for(int i = 0; i < 4 + omp_get_max_threads(); i++) {
        __thread_data[i].tmp_json_thread_id = -1;
    }

    // clear list of phase transitions
    __phase_transitions = (h2m_phase_transition_t*) FCN_MALLOC(sizeof(h2m_phase_transition_t)*H2M_MAX_PHASE_TRANSITIONS.load());
    for(int i = 0; i < H2M_MAX_PHASE_TRANSITIONS; i++) {
        __phase_transitions[i].name = nullptr;
        __phase_transitions[i].n_migrations = 0;
        __phase_transitions[i].elapsed_sec_migrations = 0.0;
    }
    __phase_transition_id = 0;
    __req_last_prefetch = nullptr;
}

void cleanup_data_structures() {
    // mark last implicit phase transition
    if(__req_last_prefetch != nullptr) {
        h2m_migration_req_free(__req_last_prefetch);
    }

    cleanup_phase_transitions();

    // cleanup
    if(__numa_data) {
        delete[] __numa_data;
        __numa_data = nullptr;
    }

    cleanup_mem_characteristics();

    if(__thread_data) {
        FCN_FREE(__thread_data);
        __thread_data = nullptr;
    }

    cleanup_json_traits();
}

void* __h2m_alloc_w_placement(size_t size, int *err, int n_traits, const h2m_alloc_trait_t traits[], h2m_placement_decision_t pl) {
    DBP(__VLVL_ENTRY_EXIT_VERBOSE, "__h2m_alloc_w_placement (enter)\n");
    // over-write memory space if configured
    if (H2M_FORCED_ALLOC_MEM_SPACE_INT.load() != -1) {
        pl.mem_space = (h2m_alloc_trait_value_t) H2M_FORCED_ALLOC_MEM_SPACE_INT.load();
    }

    unsigned long nodemask = 0;
    { // extra scope for time recorder
        h2m_stat_recorder_t select_nodemask_time_recorder(__stats_time_select_nodemask);
        nodemask = select_nodes_for_mem_space(&pl, size, -1, true, err);
    }

    if (*err != H2M_SUCCESS) {
        DBP(__VLVL_ENTRY_EXIT_VERBOSE, "__h2m_alloc_w_placement (exit)\n");
        return nullptr;
    }

    // allocate the memory
    void* ret = h2m_alloc_on_nodes_w_traits(size, pl, nodemask, err, n_traits, traits);

    // also get JSON allocation id if available
    int json_alloc_id = -1;
    if(ret) {
        int found = 0;
        h2m_alloc_info_t* inf = get_alloc_info_ref(ret, &found);
        json_alloc_id = inf->json_alloc_id;
    }
    DBP(__VLVL_MIGRATION_DECISIONS, "Allocation ptr=" DPxMOD ": size=%zu, json_id=%d, effective mem_space => %d\n", DPxPTR(ret), size, json_alloc_id, pl.mem_space);
    DBP(__VLVL_ENTRY_EXIT_VERBOSE, "__h2m_alloc_w_placement (exit)\n");
    return ret;
}

h2m_alloc_info_t* __h2m_gather_migration_alloc_info(h2m_migration_req_t req, int *n_ele) {
    if(req->allocations) {
        // case: migration request for specific allocations
        h2m_stat_recorder_t apply_migration_alloc(__stats_time_apply_migration_alloc_data_collection);

        *n_ele = 0;
        h2m_alloc_info_t* alloc_info = (h2m_alloc_info_t*) FCN_MALLOC(sizeof(h2m_alloc_info_t) * req->n_allocs);

        __mtx_book_keeping.rdlock();

        std::map<void*, h2m_alloc_info_t*>::iterator it;
        for(int i = 0; i < req->n_allocs; i++) {
            it = __book_keeping_allocations.find(req->allocations[i]);
            if(it != __book_keeping_allocations.end()) {
                alloc_info[*n_ele] = *(it->second); // does that create a copy?
                (*n_ele)++;
            }
        }

        __mtx_book_keeping.unlock();

        return alloc_info;
    } else {
        // case: general migration request for all migrations
        h2m_stat_recorder_t apply_migration_alloc(__stats_time_apply_migration_data_collection);

        *n_ele                        = -1;
        h2m_alloc_info_t* alloc_info  = nullptr;

        __mtx_book_keeping.rdlock();

        *n_ele      = __book_keeping_allocations.size();
        alloc_info  = (h2m_alloc_info_t*) FCN_MALLOC(sizeof(h2m_alloc_info_t) * (*n_ele));
        std::map<void*, h2m_alloc_info_t*>::iterator it;
        int ctr = 0;
        for (it = __book_keeping_allocations.begin(); it != __book_keeping_allocations.end(); it++) {
            alloc_info[ctr] = *(it->second); // does that create a copy?
            ctr++;
        }

        __mtx_book_keeping.unlock();

        return alloc_info;
    }
}

int __h2m_apply_migration_inner(h2m_migration_req_t req) {
    int final_err                           = H2M_SUCCESS;
    int err                                 = 0;
    int n_decisions                         = -1;
    h2m_migration_decision_t* decisions     = nullptr;

    int n_ele;
    { // extra scope for time recorder
        h2m_stat_recorder_t migration_strat_time_recorder(__stats_time_migration_strategy);
        h2m_alloc_info_t* alloc_info = __h2m_gather_migration_alloc_info(req, &n_ele);

        #if TOOL_SUPPORT
        if(tool_status.enabled && tool_status.callback_migration_strategy) {
            decisions = tool_status.callback_migration_strategy(n_ele, alloc_info, &n_decisions);
        } else
        #endif // TOOL_SUPPORT
        {
            decisions = h2m_default_migration_strategy(n_ele, alloc_info, &n_decisions);
        }
        FCN_FREE(alloc_info);
    }

    if (decisions && n_decisions > 0) {
        h2m_explicit_stat_recorder_t migration_inner_time_recorder(__stats_time_migration_inner);

        h2m_movement_info_t *m_info = (h2m_movement_info_t *) FCN_MALLOC(n_decisions * sizeof(h2m_movement_info_t));
        int n_movements = 0;

        for(int i = 0; i < n_decisions; i++) {
            h2m_migration_decision_t *dec = &(decisions[i]);

            int tmp_cpu_numa_node = -1;
            unsigned long nodemask_dst = 0;
            { // extra scope for time recorder
                h2m_stat_recorder_t select_nodemask_time_recorder(__stats_time_select_nodemask);

                // get desired nodemask
                if(dec->alloc_info.allocated_from_cpu_node != -1) {
                    tmp_cpu_numa_node = dec->alloc_info.allocated_from_cpu_node;
                }

                nodemask_dst = select_nodes_for_mem_space(&(dec->placement), dec->alloc_info.size, tmp_cpu_numa_node, false, &err);
            }

            if (err != H2M_SUCCESS) {
                RELP("ERROR: __h2m_apply_migration_inner: Could not select nodes for ptr = " DPxMOD ". Returned %s\n", DPxPTR(dec->alloc_info.ptr), h2m_result_types_t_str[err]);
                FCN_FREE(m_info);
                req->return_code = err;
                return err;
            }

            // check whether migration is necessary
            unsigned long mask_cur = __mapping_mem_space_nodemask[dec->alloc_info.cur_mem_space];
            unsigned long mask_dst = __mapping_mem_space_nodemask[dec->placement.mem_space];
            if (mask_cur == mask_dst) {
                // TODO: difference in memory partitioning currently not supported for data migration
                // DBP(__VLVL_MIGRATION_DECISIONS, "Don't migrate data for ptr = " DPxMOD " as masks are equal -> %zu \n", DPxPTR(dec->alloc_info.ptr), mask_cur);
                continue;
            }

            m_info[n_movements].ptr             = dec->alloc_info.ptr;
            m_info[n_movements].size            = dec->alloc_info.size;
            m_info[n_movements].pl              = &(dec->placement);
            m_info[n_movements].nodemask        = nodemask_dst;
            m_info[n_movements].cpu_numa_node   = tmp_cpu_numa_node;
            n_movements++;
        }

        if(n_movements > 0) {
            DBP(__VLVL_MIGRATION_DECISIONS, "Running migration with %d movement requests\n", n_movements);

            double tmp_time = omp_get_wtime();
            // Case 1: mbind
            err = h2m_move_memory_mbind(m_info, n_movements);
            // Case 2: move_pages
            // err = h2m_move_memory_movepages(m_info, n_movements);
            tmp_time = omp_get_wtime() - tmp_time;

            // set information for phase transition
            int cur_ph_id = __phase_transition_id.load();
            __phase_transitions[cur_ph_id-1].n_migrations += n_movements;
            __phase_transitions[cur_ph_id-1].elapsed_sec_migrations += tmp_time;
            DBP(__VLVL_MIGRATION_DECISIONS, "Finished migration with %d movement requests in phase %d in %f sec\n", n_movements, cur_ph_id, tmp_time);
        }

        FCN_FREE(m_info);
        FCN_FREE(decisions);


        req->elapsed_sec = migration_inner_time_recorder.get_elapsed_time();
    }
    req->return_code = final_err;
    return final_err;
}
#pragma endregion Util functions

#pragma region Getters
//================================================================================
// Getters
//================================================================================

const char* h2m_get_version() {
    return H2M_VERSION_STRING;
}

int* h2m_get_nodes_for_mem_space(h2m_alloc_trait_value_t mem_space, int *num_nodes) {
    DBP(__VLVL_ENTRY_EXIT, "h2m_get_nodes_for_mem_space (enter)\n");
    verify_initialized();
    
    int cpu_numa_node = numa_node_of_cpu(sched_getcpu());
    std::vector<int> &node_list = __numa_data[cpu_numa_node].mem_space_mapping[mem_space];
    *num_nodes = node_list.size();
    
    DBP(__VLVL_ENTRY_EXIT, "h2m_get_nodes_for_mem_space (exit)\n");
    return &(node_list[0]);
}

unsigned long h2m_get_nodemask_for_mem_space(h2m_alloc_trait_value_t mem_space) {
    DBP(__VLVL_ENTRY_EXIT_VERBOSE, "h2m_get_nodemask_for_mem_space (mem_space=%d) (enter)\n", mem_space);
    verify_initialized();
    unsigned long ret = __mapping_mem_space_nodemask[mem_space];
    DBP(__VLVL_ENTRY_EXIT_VERBOSE, "h2m_get_nodemask_for_mem_space (mem_space=%d) (exit)\n", mem_space);
    return ret;
}

h2m_placement_decision_t h2m_get_default_placement() {
    h2m_placement_decision_t ret;
    ret.alignment           = getpagesize();
    ret.mem_space           = h2m_atv_mem_space_hbw;
    if (H2M_DEFAULT_MEM_SPACE_INT.load() != -1) {
        ret.mem_space       = (h2m_alloc_trait_value_t)H2M_DEFAULT_MEM_SPACE_INT.load();
    }
    ret.mem_partition       = h2m_atv_mem_partition_first_touch;
    ret.mem_prio            = h2m_atk_pref_mem_space;
    ret.mem_blocksize_kb    = 0;
    return ret;
}

h2m_alloc_info_t h2m_get_alloc_info(void *ptr, int *out_found) {
    DBP(__VLVL_ENTRY_EXIT, "h2m_get_alloc_info (enter)\n");
    verify_initialized();
    *out_found = 0;
    h2m_alloc_info_t ret;

    __mtx_book_keeping.rdlock();

    std::map<void*, h2m_alloc_info_t*>::iterator it = __book_keeping_allocations.find(ptr);
    if(it != __book_keeping_allocations.end()) {
        h2m_alloc_info_t *cur_val = it->second;
        memcpy(&ret, cur_val, sizeof(h2m_alloc_info_t));
        *out_found = 1;
    }

    __mtx_book_keeping.unlock();

    DBP(__VLVL_ENTRY_EXIT, "h2m_get_alloc_info (exit)\n");
    return ret;
}
#pragma endregion Getters

#pragma region Initialize and Finalize
//================================================================================
// Init / Finalize
//================================================================================

/*
 * Function h2m_init
 * Initializes library and all what's necessary.
 */
int h2m_init() {
    DBP(__VLVL_ENTRY_EXIT, "h2m_init (enter)\n");
    if(__is_initialized.load()) {
        DBP(__VLVL_ENTRY_EXIT, "h2m_init (exit)\n");
        return H2M_SUCCESS;
    }

    { // extra scope for time recorder
        h2m_stat_recorder_t init_time_recorder(__stats_time_init_finalize);
    
        __mtx_is_initialized.lock();
        // need to check again
        if(__is_initialized.load()) {
            __mtx_is_initialized.unlock();
            return H2M_SUCCESS;
        }

        // load config values that were speicified by environment variables
        load_config_values();

        // Verify hwloc version at runtime
        if (!check_hwloc_version()) {
            return H2M_FAILURE;
        }

        // allocate and initialize relevant data structures for the runtime
        init_data_structures();

        if(H2M_PRINT_AFFINITY_MASKS.load()) {
            get_and_print_affinity_mask();
        }

        // determine types of memory
        build_mem_space_mapping();

        // parse lookup tables for bw and latency
        parse_mem_characteristics();    

        // we need to check the max memory capacity per memory space once in the beginning
        determine_max_mem_for_mem_spaces();

        if(H2M_PRINT_CONFIG_VALUES.load()) {
            RELP("h2m_init: VERSION %s\n", H2M_VERSION_STRING);
            print_config_values();
        }

        if(H2M_PRINT_MEM_CHARACTERISTICS.load()) {
            print_mem_characteristics();
        }

        if(H2M_BACKGROUND_THREAD_ENABLED.load()) {
            // start background thread for data migration
            __start_bg_thread();
        }

        if(H2M_JSON_TRAIT_FILE.load()) {
            // load traits from file once that can be used in ...
            parse_json_traits(H2M_JSON_TRAIT_FILE.load());
        }

        #if TOOL_SUPPORT
        h2m_tool_init();
        #endif // TOOL_SUPPORT

        // set flag to ensure that only a single thread is initializing
        __is_initialized = true;
        
        // mark first implicit phase transition
        h2m_phase_transition("h2m_init");
        
        __mtx_is_initialized.unlock();
    }

    DBP(__VLVL_ENTRY_EXIT, "h2m_init (exit)\n");
    return H2M_SUCCESS;
}

int h2m_thread_init() {
    DBP(__VLVL_ENTRY_EXIT, "h2m_thread_init (enter)\n");
    if(!__is_initialized.load()) {
        h2m_init();
    }

    { // extra scope for time recorder
        h2m_stat_recorder_t init_time_recorder(__stats_time_init_finalize);

        // make sure basic stuff is initialized
        int gtid = h2m_get_gtid();

        #if TOOL_SUPPORT
        if(tool_status.enabled && tool_status.callback_thread_init) {
            tool_status.callback_thread_init(&(__thread_data[gtid].thread_tool_data));
        }
        #endif // TOOL_SUPPORT
        if(H2M_PRINT_AFFINITY_MASKS.load()) {
            get_and_print_affinity_mask();
        }
    }

    DBP(__VLVL_ENTRY_EXIT, "h2m_thread_init (exit)\n");
    return H2M_SUCCESS;
}

/* 
 * Function h2m_post_init_serial
 * Can run after init and thread init in a serial region
 */
int h2m_post_init_serial() {
    DBP(__VLVL_ENTRY_EXIT, "h2m_post_init_serial (enter)\n");
    verify_initialized();
    int gtid = h2m_get_gtid();
    
    #if TOOL_SUPPORT
    if(tool_status.enabled && tool_status.callback_post_init_serial) {
        tool_status.callback_post_init_serial(&(__thread_data[gtid].thread_tool_data));
    }
    #endif // TOOL_SUPPORT
    
    DBP(__VLVL_ENTRY_EXIT, "h2m_post_init_serial (exit)\n");
    return H2M_SUCCESS;
}

int h2m_thread_finalize() {
    DBP(__VLVL_ENTRY_EXIT, "h2m_thread_finalize (enter)\n");

    { // extra scope for time recorder
        h2m_stat_recorder_t init_time_recorder(__stats_time_init_finalize);

        int gtid = h2m_get_gtid();

        #if TOOL_SUPPORT
        if(tool_status.enabled && tool_status.callback_thread_finalize) {
            tool_status.callback_thread_finalize(&(__thread_data[gtid].thread_tool_data));
        }
        #endif // TOOL_SUPPORT
    }

    DBP(__VLVL_ENTRY_EXIT, "h2m_thread_finalize (exit)\n");
    return H2M_SUCCESS;
}

/* 
 * Function h2m_finalize
 * Finalizing and cleaning up library before the program ends.
 */
int h2m_finalize() {
    DBP(__VLVL_ENTRY_EXIT, "h2m_finalize (enter)\n");
    verify_initialized();

    { // extra scope for time recorder
        h2m_stat_recorder_t init_time_recorder(__stats_time_init_finalize);

        if(H2M_BACKGROUND_THREAD_ENABLED.load()) {
            __stop_bg_thread();
        }

        #if TOOL_SUPPORT
        h2m_tool_fini();
        #endif // TOOL_SUPPORT

        h2m_phase_transition("h2m_finalize");

        if(H2M_DUMP_DIR.load()) {
            dump_phase_transitions();
        }

        if(H2M_PRINT_PHASE_TIMES.load()) {
            print_phase_transitions();
        }
    }

    #if H2M_RECORD_STATS
    if(H2M_PRINT_STATISTICS.load()) {
        h2m_stats_print();
    }
    #endif

    // cleanup relevant data structures for the runtime
    cleanup_data_structures();

    __is_initialized = false;

    DBP(__VLVL_ENTRY_EXIT, "h2m_finalize (exit)\n");
    return H2M_SUCCESS;
}
#pragma endregion Initialize and Finalize

#pragma region High level interface
//================================================================================
// High-level memory allocation & migration
//================================================================================

int h2m_verify_traits(int n_traits, const h2m_alloc_trait_t traits[]) {
    DBP(__VLVL_ENTRY_EXIT, "h2m_verify_traits (enter)\n");
    verify_initialized();

    #if TOOL_SUPPORT
    if(tool_status.enabled && tool_status.callback_verify_traits) {
        int ret_val = tool_status.callback_verify_traits(n_traits, traits);
        DBP(__VLVL_ENTRY_EXIT, "h2m_verify_traits (exit)\n");
        return ret_val;
    }
    #endif // TOOL_SUPPORT

    DBP(__VLVL_ENTRY_EXIT, "h2m_verify_traits (exit)\n");
    return H2M_SUCCESS;
}

void* h2m_alloc(size_t size, int *err) {
    DBP(__VLVL_ENTRY_EXIT, "h2m_alloc (enter)\n");
    void* ret = h2m_alloc_w_traits(size, err, 0, nullptr);
    DBP(__VLVL_ENTRY_EXIT, "h2m_alloc (exit)\n");
    return ret;
}

void* h2m_alloc_w_traits(size_t size, int *err, int n_traits, const h2m_alloc_trait_t traits[]) {
    DBP(__VLVL_ENTRY_EXIT, "h2m_alloc_w_traits (enter)\n");
    verify_initialized();

    h2m_placement_decision_t pl;
    void *ret = nullptr;
    { // extra scope for time recorder
        h2m_stat_recorder_t allocate_time_recorder(__stats_time_allocate);

        #if TOOL_SUPPORT
        if(tool_status.enabled && tool_status.callback_alloc) {
            tool_status.callback_alloc(size, n_traits, traits);
        }
        #endif // TOOL_SUPPORT

        pl = h2m_call_allocation_strategy(size, n_traits, traits, err);

        if (*err != H2M_SUCCESS) {
            DBP(__VLVL_ENTRY_EXIT, "h2m_alloc_w_traits (exit)\n");
            return nullptr;
        }

        // allocate the memory
        ret = __h2m_alloc_w_placement(size, err, n_traits, traits, pl);
    }

    if (!ret && H2M_PRINT_ALLOCATION_ERRORS.load()) {
        RELP("ERROR: h2m_alloc_w_traits: Could not allocate memory in desired memory space %d. Returned %s\n", pl.mem_space, h2m_result_types_t_str[*err]);
    }

    DBP(__VLVL_ENTRY_EXIT, "h2m_alloc_w_traits (exit)\n");
    return ret;
}

void* h2m_alloc_w_traits_file(size_t size, int callsite_id, int *err) {
    DBP(__VLVL_ENTRY_EXIT, "h2m_alloc_w_traits_file (enter)\n");

    bool prefetch; // irrelevant here -> will be ignored
    int n_traits = 0;
    int json_alloc_id = -1;
    h2m_alloc_trait_t* traits = nullptr;

    { // extra scope for time recorder
        h2m_stat_recorder_t get_traits_time_recorder(__stats_time_get_traits_from_file);

        if(H2M_JSON_USE_ID.load()) {
            // match based on id that has been provided to call
            traits = get_json_traits_for_allocation(__phase_transition_id.load(), callsite_id, "", true, &json_alloc_id, &n_traits, &prefetch);
        } else {
            // match against full callstack
            std::string cso, cso_clean, csrs;

            { // extra scope for time recorder
                h2m_stat_recorder_t callstack_time_recorder(__stats_time_get_callstack_info);

                cso = get_callstack_offsets(2);
                cso_clean = get_clean_callsite_offsets(cso);
                csrs = get_callstack_rip_str(2);
            }

            if (H2M_PRINT_CALLSTACK_OFFSETS.load() && size > H2M_PRINT_CALLSTACK_OFFSETS_MIN_SIZE_BYTES.load()) {
                // RELP("Allocation with size %lu bytes has callstack_offset %s and rip_str %s\n", size, cso.c_str(), csrs.c_str());
                RELP("Allocation with size %lu bytes has clean callstack_offset %s\n", size, cso_clean.c_str());
            }
            traits = get_json_traits_for_allocation(__phase_transition_id.load(), -1, cso_clean, false, &json_alloc_id, &n_traits, &prefetch);
        }
    }

    // if(n_traits > 0) {
    //     RELP("key=%s, value=%d\n", h2m_alloc_trait_key_t_str[traits[0].key], traits[0].value);
    // }

    // json allocation id that should be saved in the bookkeeping information to match it with later phases
    int gtid = h2m_get_gtid();
    __thread_data[gtid].tmp_json_thread_id = json_alloc_id;

    // finally allocate memory
    void* ret = h2m_alloc_w_traits(size, err, n_traits, traits);

    DBP(__VLVL_ENTRY_EXIT, "h2m_alloc_w_traits_file (exit)\n");
    return ret;
}

int h2m_free(void* ptr) {
    DBP(__VLVL_ENTRY_EXIT, "h2m_free (enter)\n");
    verify_initialized();

    int ret = H2M_SUCCESS;

    #if TOOL_SUPPORT
    if(tool_status.enabled && tool_status.callback_free) {
        tool_status.callback_free(ptr);
    }
    #endif // TOOL_SUPPORT
    
    // book keeping
    __mtx_book_keeping.wrlock();

    std::map<void*, h2m_alloc_info_t*>::iterator it = __book_keeping_allocations.find(ptr);
    if(it != __book_keeping_allocations.end()) {
        h2m_alloc_info_t *cur_val = it->second;

        DBP(__VLVL_MEM_SPACE_CAPACITY, "h2m_free: deallocating ptr=" DPxMOD ", json_id=%d, size=%zu\n", DPxPTR(ptr), cur_val->json_alloc_id, cur_val->size);
        
        // decrement used memory
        ret = decrement_used_capacity(cur_val->cur_mem_space, cur_val->size);

        // free book-keeping data structure again
        if (cur_val->traits) {
            FCN_FREE(cur_val->traits);
        }
        FCN_FREE(cur_val);
        __book_keeping_allocations.erase(it);
    }

    __mtx_book_keeping.unlock();

    FCN_FREE(ptr);
    DBP(__VLVL_ENTRY_EXIT, "h2m_free (exit)\n");
    return ret;
}

int h2m_update_traits(void *ptr, size_t size, int n_traits, const h2m_alloc_trait_t traits[], int replace_complete) {
    DBP(__VLVL_ENTRY_EXIT, "h2m_update_traits (ptr=" DPxMOD ") (enter)\n", DPxPTR(ptr));
    verify_initialized();

    #if TOOL_SUPPORT
    if(tool_status.enabled && tool_status.callback_update_traits) {
        tool_status.callback_update_traits(ptr, size, n_traits, traits);
    }
    #endif // TOOL_SUPPORT

    // if (H2M_MIGRATION_ENABLED.load()) {
        h2m_alloc_info_t *cur_val = nullptr;

        __mtx_book_keeping.rdlock();

        std::map<void*, h2m_alloc_info_t*>::iterator it = __book_keeping_allocations.find(ptr);
        if(it != __book_keeping_allocations.end()) {
            cur_val = it->second;
        }

        __mtx_book_keeping.unlock();

        // only of mapping for pointer has been found
        if (cur_val) {
            if (replace_complete == 0) {
                if (traits) {
                    // Case: Update only single trait items (unique keys possible only)
                    std::map<h2m_alloc_trait_key_t, h2m_alloc_trait_t::h2m_atv_union_t> vals;
                    if(cur_val->traits) {
                        // remember keys and values in map
                        for(int i = 0; i < cur_val->n_traits; i++) {
                            vals[cur_val->traits[i].key] = cur_val->traits[i].value;
                        }
                        // cleanup old memory
                        FCN_FREE(cur_val->traits);
                        cur_val->traits = nullptr;
                        cur_val->n_traits = 0;
                    }

                    // set/update trait values
                    for(int i = 0; i < n_traits; i++) {
                        vals[traits[i].key] = traits[i].value;
                    }
                    // only if there are any traits
                    if(vals.size() > 0) {
                        // allocate memory
                        size_t size_traits = vals.size() * sizeof(h2m_alloc_trait_t);
                        cur_val->n_traits = vals.size();
                        cur_val->traits = (h2m_alloc_trait_t*) FCN_MALLOC(size_traits);
                        // set traits
                        int ctr = 0;
                        for (auto const& x : vals) {
                            cur_val->traits[ctr].key    = x.first;
                            cur_val->traits[ctr].value  = x.second;
                            ctr++;
                        }
                    }
                }
            } else {
                // Case: Replace traits completly
                if(cur_val->traits) {
                    FCN_FREE(cur_val->traits);
                    cur_val->traits = nullptr;
                    cur_val->n_traits = 0;
                }

                if (traits) {
                    size_t size_traits = n_traits * sizeof(h2m_alloc_trait_t);
                    cur_val->n_traits = n_traits;
                    cur_val->traits = (h2m_alloc_trait_t*) FCN_MALLOC(size_traits);
                    memcpy(cur_val->traits, traits, size_traits);
                }
            }
            cur_val->last_updated = omp_get_wtime();
        }
    // }

    DBP(__VLVL_ENTRY_EXIT, "h2m_update_traits (ptr=" DPxMOD ") (exit)\n", DPxPTR(ptr));
    return H2M_SUCCESS;
}

int h2m_apply_migration() {
    DBP(__VLVL_ENTRY_EXIT, "h2m_apply_migration (enter)\n");
    verify_initialized();

    #if TOOL_SUPPORT
    if(tool_status.enabled && tool_status.callback_apply_migration) {
        tool_status.callback_apply_migration(h2m_tool_migration_start);
    }
    #endif // TOOL_SUPPORT

    int ret_val = H2M_SUCCESS;

    if (H2M_MIGRATION_ENABLED.load()) {
        // create new request
        h2m_migration_req_t inst = __create_default_request();

        // call inner function directly
        ret_val = __h2m_apply_migration_inner(inst);
        
        // cleanup again
        h2m_migration_req_free(inst);
    }

    #if TOOL_SUPPORT
    if(tool_status.enabled && tool_status.callback_apply_migration) {
        tool_status.callback_apply_migration(h2m_tool_migration_end);
    }
    #endif // TOOL_SUPPORT

    DBP(__VLVL_ENTRY_EXIT, "h2m_apply_migration (exit)\n");
    return ret_val;
}

int h2m_apply_migration_for_allocations(void** allocations, int n_allocs) {
    DBP(__VLVL_ENTRY_EXIT, "h2m_apply_migration_for_allocations (n_allocs=%d) (enter)\n", n_allocs);
    verify_initialized();

    #if TOOL_SUPPORT
    if(tool_status.enabled && tool_status.callback_apply_migration_alloc) {
        tool_status.callback_apply_migration_alloc(allocations, n_allocs, h2m_tool_migration_start);
    }
    #endif // TOOL_SUPPORT

    int ret_val = H2M_SUCCESS;

    if (H2M_MIGRATION_ENABLED.load()) {
        // create new request
        h2m_migration_req_t inst = __create_default_request();
        size_t tmp_size = sizeof(void*) * n_allocs;
        inst->allocations = (void**) FCN_MALLOC(tmp_size);
        memcpy(inst->allocations, allocations, tmp_size);
        inst->n_allocs = n_allocs;

        // call inner function directly
        ret_val = __h2m_apply_migration_inner(inst);
        
        // cleanup again
        h2m_migration_req_free(inst);
    }

    #if TOOL_SUPPORT
    if(tool_status.enabled && tool_status.callback_apply_migration_alloc) {
        tool_status.callback_apply_migration_alloc(allocations, n_allocs, h2m_tool_migration_end);
    }
    #endif // TOOL_SUPPORT

    DBP(__VLVL_ENTRY_EXIT, "h2m_apply_migration_for_allocations (n_allocs=%d) (exit)\n", n_allocs);
    return ret_val;
}

int h2m_apply_migration_async(h2m_migration_req_t *out_req) {
    DBP(__VLVL_ENTRY_EXIT, "h2m_apply_migration_async (enter)\n");
    verify_initialized();

    #if TOOL_SUPPORT
    if(tool_status.enabled && tool_status.callback_apply_migration_async) {
        tool_status.callback_apply_migration_async(out_req);
    }
    #endif // TOOL_SUPPORT

    int ret_val = H2M_SUCCESS;

    // create new request
    h2m_migration_req_t inst = __create_default_request();

    if (H2M_MIGRATION_ENABLED.load()) {
        if(H2M_BACKGROUND_THREAD_ENABLED.load()) {
            // queue request
            __migration_requests.push_back(inst);
        } else {
            // if background thread not available async behaves like blocking
            ret_val = __h2m_apply_migration_inner(inst);
            inst->is_completed = 1;
        }
    } else {
        // mark request as already done as migration is disabled
        inst->is_completed = 1;
    }

    *out_req = inst;
    DBP(__VLVL_ENTRY_EXIT, "h2m_apply_migration_async (exit)\n");
    return ret_val;
}

int h2m_apply_migration_for_allocations_async(void** allocations, int n_allocs, h2m_migration_req_t *out_req) {
    DBP(__VLVL_ENTRY_EXIT, "h2m_apply_migration_for_allocations_async (enter)\n");
    verify_initialized();

    #if TOOL_SUPPORT
    if(tool_status.enabled && tool_status.callback_apply_migration_alloc_async) {
        tool_status.callback_apply_migration_alloc_async(allocations, n_allocs, out_req);
    }
    #endif // TOOL_SUPPORT

    int ret_val = H2M_SUCCESS;

    // create new request
    h2m_migration_req_t inst = __create_default_request();

    if (H2M_MIGRATION_ENABLED.load()) {
        // populate list
        size_t tmp_size = sizeof(void*) * n_allocs;
        inst->allocations = (void**) FCN_MALLOC(tmp_size);
        memcpy(inst->allocations, allocations, tmp_size);
        inst->n_allocs = n_allocs;

        if(H2M_BACKGROUND_THREAD_ENABLED.load()) {
            // queue request
            __migration_requests.push_back(inst);
        } else {
            // if background thread not available async behaves like blocking
            ret_val = __h2m_apply_migration_inner(inst);
            inst->is_completed = 1;
        }
    } else {
        // mark request as already done as migration is disabled
        inst->is_completed = 1;
    }

    *out_req = inst;
    DBP(__VLVL_ENTRY_EXIT, "h2m_apply_migration_for_allocations_async (exit)\n");
    return ret_val;
}

int h2m_wait(h2m_migration_req_t req) {
    DBP(__VLVL_ENTRY_EXIT, "h2m_wait (enter)\n");
    verify_initialized();

    { // extra scope for time recorder
        h2m_stat_recorder_t wait_time_recorder(__stats_time_wait);

        #if TOOL_SUPPORT
        if(tool_status.enabled && tool_status.callback_wait) {
            tool_status.callback_wait(req, h2m_tool_wait_start);
        }
        #endif // TOOL_SUPPORT

        int us_sleep = H2M_THREAD_SLEEP_TIME_MICRO_SECS.load();
        while(!req->is_completed) {
            usleep(us_sleep);
        }

        #if TOOL_SUPPORT
        if(tool_status.enabled && tool_status.callback_wait) {
            tool_status.callback_wait(req, h2m_tool_wait_end);
        }
        #endif // TOOL_SUPPORT
    }

    DBP(__VLVL_ENTRY_EXIT, "h2m_wait (exit)\n");
    return H2M_SUCCESS;
}

int h2m_is_req_completed(h2m_migration_req_t req) {
    return req->is_completed;
}

int h2m_migration_req_free(h2m_migration_req_t req) {
    DBP(__VLVL_ENTRY_EXIT, "h2m_migration_req_free (enter)\n");
    verify_initialized();

    if(req) {
        if(req->allocations) {
            FCN_FREE(req->allocations);
            req->allocations = nullptr;
        }
        FCN_FREE(req);
        req = nullptr;
    }
    
    DBP(__VLVL_ENTRY_EXIT, "h2m_migration_req_free (exit)\n");
    return H2M_SUCCESS;
}

int h2m_phase_transition(const char* name) {
    DBP(__VLVL_ENTRY_EXIT, "h2m_phase_transition (name=%s) (enter)\n", name);
    verify_initialized();

    int ret = H2M_SUCCESS;

    __mtx_phase_transition.lock();
    int cur_phase_id = __phase_transition_id++;
    int next_phase_id = cur_phase_id+1;
    __phase_transitions[cur_phase_id].id = cur_phase_id;
    __phase_transitions[cur_phase_id].name = strdup(name);
    __phase_transitions[cur_phase_id].ts = new_date_ns();
    __mtx_phase_transition.unlock();

    DBP(__VLVL_NUMA_AND_MEM, "h2m_phase_transition: End of phase %d at %" PRIu64 "\n", cur_phase_id-1, __phase_transitions[cur_phase_id].ts);
    DBP(__VLVL_NUMA_AND_MEM, "h2m_phase_transition: Begin of phase %d (%s, ptr="  DPxMOD ") at %" PRIu64 "\n", cur_phase_id, __phase_transitions[cur_phase_id].name, DPxPTR(__phase_transitions[cur_phase_id].name), __phase_transitions[cur_phase_id].ts);

    if(H2M_MIGRATION_ENABLED.load()) {
        // wait for last migration req
        if(H2M_BACKGROUND_THREAD_ENABLED.load() && __req_last_prefetch != nullptr) {
            ret = h2m_wait(__req_last_prefetch);
            ret = h2m_migration_req_free(__req_last_prefetch);
            __req_last_prefetch = nullptr;
        }

        // for now only perform migration if JSON trait information available
        if(json_flag_available.load()) {
            // check if phase data available
            if(json_phases.count(next_phase_id)) {
                JsonPhase* ph = json_phases.find(next_phase_id)->second;
                int found;
                std::vector<void*> allocs_to_move;      // should be moved immediately
                std::vector<void*> allocs_to_prefetch;  // can be prefetched. need to wait on last prefetching event

                for(JsonTraitEntry& entry : ph->traits) {
                    // skip if there are no traits defined (should usually not happen)
                    if(entry.traits.size() == 0) {
                        continue;
                    }

                    // check whether information for allocation still exists
                    h2m_alloc_info_t* info = get_alloc_info_ref_for_json_id(entry.allocation_id, &found);
                    if(found) {
                        // update traits here and remember to trigger migration for that allocation
                        DBP(__VLVL_MIGRATION_DECISIONS, "Updating traits for json_allocation_id=%d (ptr=" DPxMOD ")\n", entry.allocation_id, DPxPTR(info->ptr));
                        h2m_update_traits(info->ptr, info->size, entry.traits.size(), &(entry.traits[0]), 1);

                        if(entry.prefetch) {
                            allocs_to_prefetch.push_back(info->ptr);
                        } else {
                            allocs_to_move.push_back(info->ptr);
                        }
                    }
                }

                if (H2M_BACKGROUND_THREAD_ENABLED.load()) {
                    // only perform async migration of background thread enabled
                    if(allocs_to_prefetch.size() > 0) {
                        DBP(__VLVL_MIGRATION_DECISIONS, "Triggering %zu async migrations\n", allocs_to_prefetch.size());
                        ret = h2m_apply_migration_for_allocations_async(&(allocs_to_prefetch[0]), allocs_to_prefetch.size(), &(__req_last_prefetch));
                    }
                    if(allocs_to_move.size() > 0) {
                        DBP(__VLVL_MIGRATION_DECISIONS, "Triggering %zu blocking migrations\n", allocs_to_move.size());
                        ret = h2m_apply_migration_for_allocations(&(allocs_to_move[0]), allocs_to_move.size());
                    }
                } else {
                    // combine lists
                    std::vector<void*> all_transfers = allocs_to_move;
                    all_transfers.insert(all_transfers.end(), allocs_to_prefetch.begin(), allocs_to_prefetch.end());
                    if(all_transfers.size() > 0) {
                        ret = h2m_apply_migration_for_allocations(&(all_transfers[0]), all_transfers.size());
                    }
                }
            }
        }
    }

    DBP(__VLVL_ENTRY_EXIT, "h2m_phase_transition (name=%s) (exit)\n", name);
    return ret;
}

h2m_declaration_t h2m_declare_alloc(void** ptr, size_t size, h2m_fcn_data_init_t fcn_data_init, void *fcn_args, int *err) {
    DBP(__VLVL_ENTRY_EXIT, "h2m_declare_alloc (enter)\n");
    verify_initialized();

    h2m_declaration_t ret_val = h2m_declare_alloc_w_traits(ptr, size, fcn_data_init, fcn_args, err, 0, nullptr);

    DBP(__VLVL_ENTRY_EXIT, "h2m_declare_alloc (exit)\n");
    return ret_val;
}

h2m_declaration_t h2m_declare_alloc_w_traits(void** ptr, size_t size, h2m_fcn_data_init_t fcn_data_init, void *fcn_args, int *err, int n_traits, const h2m_alloc_trait_t traits[]) {
    DBP(__VLVL_ENTRY_EXIT, "h2m_declare_alloc_w_traits (enter)\n");
    verify_initialized();

    h2m_declaration_t ret_val = nullptr;
    { // extra scope for time recorder
        h2m_stat_recorder_t declare_alloc_time_recorder(__stats_time_declare_allocations);

        #if TOOL_SUPPORT
        if(tool_status.enabled && tool_status.callback_declare_alloc) {
            tool_status.callback_declare_alloc(ptr, size, fcn_data_init, fcn_args, n_traits, traits);
        }
        #endif // TOOL_SUPPORT

        ret_val = (h2m_declaration_instance_t*) FCN_MALLOC(sizeof(h2m_declaration_instance_t));

        ret_val->ptr            = ptr;
        ret_val->size           = size;
        ret_val->fcn_data_init  = fcn_data_init;
        ret_val->args_data_init = fcn_args;
        ret_val->n_traits       = 0;
        ret_val->traits         = nullptr;

        if(traits) {
            size_t size_traits = n_traits * sizeof(h2m_alloc_trait_t);
            ret_val->n_traits = n_traits;
            ret_val->traits = (h2m_alloc_trait_t*) FCN_MALLOC(size_traits);
            memcpy(ret_val->traits, traits, size_traits);
        }
    }

    DBP(__VLVL_ENTRY_EXIT, "h2m_declare_alloc_w_traits (exit)\n");
    *err = H2M_SUCCESS;
    return ret_val;
}

int h2m_commit_allocs(const h2m_declaration_t handles[], size_t n_handles) {
    DBP(__VLVL_ENTRY_EXIT, "h2m_commit_allocs (enter)\n");
    verify_initialized();

    int err;
    { // extra scope for time recorder
        h2m_stat_recorder_t commit_alloc_time_recorder(__stats_time_commit_allocations);
        h2m_alloc_order_entry_t *order = nullptr;
        { // extra scope for time recorder
            h2m_stat_recorder_t commit_strat_time_recorder(__stats_time_commit_strategy);

            #if TOOL_SUPPORT
            if(tool_status.enabled && tool_status.callback_commit_allocs) {
                tool_status.callback_commit_allocs(handles, n_handles);
            }
            #endif // TOOL_SUPPORT

            #if TOOL_SUPPORT
            if(tool_status.enabled && tool_status.callback_commit_strategy) {
                order = tool_status.callback_commit_strategy(handles, n_handles, &err);
            } else
            #endif // TOOL_SUPPORT
            {
                order = h2m_default_commit_strategy(handles, n_handles, &err);
            }
        }

        if (err != H2M_SUCCESS) {
            if(order) FCN_FREE(order);

            DBP(__VLVL_ENTRY_EXIT, "h2m_commit_allocs (exit)\n");
            return err;
        }

        // ensure ptrs are nullptr
        for(int i = 0; i < n_handles; i++) {
            h2m_declaration_instance_t* inst = order[i].decl;
            *(inst->ptr) = nullptr;
        }
        // perform actual allocations
        for(int i = 0; i < n_handles; i++) {
            h2m_declaration_instance_t* inst = order[i].decl;
            *(inst->ptr) = __h2m_alloc_w_placement(inst->size, &err, inst->n_traits, inst->traits, order[i].pl);
            if (err != H2M_SUCCESS) {
                // stop if request cannot be fulfilled
                break;
            }
        }

        if (err != H2M_SUCCESS) {
            // roll back
            for(int i = 0; i < n_handles; i++) {
                h2m_declaration_instance_t* inst = order[i].decl;
                if(*(inst->ptr)) {
                    FCN_FREE(*(inst->ptr));
                    *(inst->ptr) = nullptr;
                }
            }
            if(order) FCN_FREE(order);

            DBP(__VLVL_ENTRY_EXIT, "h2m_commit_allocs (exit)\n");
            return err;
        }

        // call data init functions
        for(int i = 0; i < n_handles; i++) {
            h2m_declaration_instance_t* inst = order[i].decl;
            if(inst->fcn_data_init) {
                inst->fcn_data_init(*(inst->ptr), inst->args_data_init);
            }
        }

        if(order) FCN_FREE(order);
        err = H2M_SUCCESS;
    }

    DBP(__VLVL_ENTRY_EXIT, "h2m_commit_allocs (exit)\n");
    return err;
}

int h2m_declaration_free(h2m_declaration_t handle) {
    DBP(__VLVL_ENTRY_EXIT, "h2m_declaration_free (enter)\n");
    verify_initialized();

    if(handle) {
        if(handle->traits) {
            FCN_FREE(handle->traits);
            handle->n_traits = 0;
            handle->traits = nullptr;
        }
        FCN_FREE(handle);
    }
    
    DBP(__VLVL_ENTRY_EXIT, "h2m_declaration_free (exit)\n");
    return H2M_SUCCESS;
}
#pragma endregion High level interface

#pragma region Low level interface
//================================================================================
// Low-level memory allocation & migration
//================================================================================

h2m_placement_decision_t h2m_apply_prescriptive_traits(h2m_placement_decision_t orig, int n_traits, const h2m_alloc_trait_t traits[]) {
    h2m_placement_decision_t pl = orig;
    if(traits) {
        int is_written = 0;

        for(size_t i = 0; i < n_traits; i++) {
            if (traits[i].key == h2m_atk_req_mem_space) {
                pl.mem_space = traits[i].value.atv;
                pl.mem_prio = h2m_atk_req_mem_space;
            } else if (traits[i].key == h2m_atk_pref_mem_space) {
                pl.mem_space = traits[i].value.atv;
                pl.mem_prio = h2m_atk_pref_mem_space;
            } else if (traits[i].key == h2m_atk_mem_alignment) {
                pl.alignment = (size_t) traits[i].value.i;
            } else if (traits[i].key == h2m_atk_mem_partition) {
                pl.mem_partition = traits[i].value.atv;
            } else if (traits[i].key == h2m_atk_mem_blocksize_kb) {
                pl.mem_blocksize_kb = (size_t) traits[i].value.i;
            } else if (traits[i].key == h2m_atk_access_mode) {
                if(traits[i].value.atv & h2m_atv_access_mode_readwrite ||
                   traits[i].value.atv & h2m_atv_access_mode_writeonly) {
                    is_written = 1;
                }
            }
        }

        // DISCUSS: should data that is written always go to HBW memory?
        // if (is_written) {
        //     pl.mem_space = h2m_atv_mem_space_hbw;
        // }
    }
    return pl;
}

h2m_placement_decision_t h2m_call_allocation_strategy(size_t size, int n_traits, const h2m_alloc_trait_t traits[], int *err) {
    DBP(__VLVL_ENTRY_EXIT, "h2m_call_allocation_strategy (size=%zu, n_traits=%d) (enter)\n", size, n_traits);
    h2m_placement_decision_t pl;
    { // extra scope for time recorder
        h2m_stat_recorder_t alloc_strat_time_recorder(__stats_time_allocation_strategy);

        #if TOOL_SUPPORT
        if(tool_status.enabled && tool_status.callback_allocation_strategy) {
            pl = tool_status.callback_allocation_strategy(size, n_traits, traits, err);
        } else
        #endif // TOOL_SUPPORT
        {
            pl = h2m_default_allocation_strategy(size, n_traits, traits, err);
        }
    }

    DBP(__VLVL_ENTRY_EXIT, "h2m_call_allocation_strategy (size=%zu, n_traits=%d) (exit)\n", size, n_traits);
    return pl;
}

void* h2m_alloc_on_nodes(size_t size, h2m_placement_decision_t pl, unsigned long nodemask, int *err) {
    DBP(__VLVL_ENTRY_EXIT, "h2m_alloc_on_nodes (enter)\n");
    void* ret = h2m_alloc_on_nodes_w_traits(size, pl, nodemask, err, 0, nullptr);
    DBP(__VLVL_ENTRY_EXIT, "h2m_alloc_on_nodes (exit)\n");
    return ret;
}

void* h2m_alloc_on_nodes_w_traits(size_t size, h2m_placement_decision_t pl, unsigned long nodemask, int *err, int n_traits, const h2m_alloc_trait_t traits[]) {
    DBP(__VLVL_ENTRY_EXIT, "h2m_alloc_on_nodes_w_traits (enter)\n");
    verify_initialized();

    #if TOOL_SUPPORT
    if(tool_status.enabled && tool_status.callback_alloc_on_nodes) {
        tool_status.callback_alloc_on_nodes(size, pl, nodemask, n_traits, traits);
    }
    #endif // TOOL_SUPPORT

    #if defined(H2M_DEBUG)
    if (H2M_VERBOSITY_LVL.load() >= __VLVL_PRINT_TRAITS) {
        print_traits("h2m_alloc_on_nodes_w_traits", n_traits, traits);
    }
    #endif // H2M_DEBUG
    
    void* ptr = alloc_on_nodes(size, pl, nodemask, err);
    if (!ptr) {
        DBP(__VLVL_ENTRY_EXIT, "h2m_alloc_on_nodes_w_traits (exit)\n");
        return nullptr;
    }

    // increase usage
    *err = increment_used_capacity(pl.mem_space, size);
    if(*err != H2M_SUCCESS) {
        FCN_FREE(ptr);
        DBP(__VLVL_ENTRY_EXIT, "h2m_alloc_on_nodes_w_traits (exit)\n");
        return nullptr;
    }

    int gtid = h2m_get_gtid();
    
    // book keeping
    h2m_alloc_info_t *bk        = (h2m_alloc_info_t *) FCN_MALLOC(sizeof(h2m_alloc_info_t));
    bk->ptr                     = ptr;
    bk->size                    = size;
    bk->cur_mem_space           = pl.mem_space;
    bk->cur_mem_partition       = pl.mem_partition;
    bk->nodemask                = nodemask;
    bk->allocated_from_cpu_node = numa_node_of_cpu(sched_getcpu());
    bk->n_traits                = 0;
    bk->traits                  = nullptr;
    bk->last_updated            = omp_get_wtime();
    bk->json_alloc_id           = __thread_data[gtid].tmp_json_thread_id;

    // reset to -1
    __thread_data[gtid].tmp_json_thread_id = -1;

    if(traits) {
        size_t size_traits = n_traits * sizeof(h2m_alloc_trait_t);
        bk->n_traits = n_traits;
        bk->traits = (h2m_alloc_trait_t*) FCN_MALLOC(size_traits);
        memcpy(bk->traits, traits, size_traits);
    }
    
    __mtx_book_keeping.wrlock();

    __book_keeping_allocations[ptr] = bk;

    __mtx_book_keeping.unlock();

    *err = H2M_SUCCESS;
    DBP(__VLVL_ENTRY_EXIT, "h2m_alloc_on_nodes_w_traits (exit)\n");
    return ptr;
}

int __move_memory_mbind_core(const h2m_movement_info_t* m_info) {
    int err = move_memory_mbind(m_info->ptr, m_info->size, m_info->pl, m_info->nodemask);
    if (err == H2M_SUCCESS) {
        __mtx_book_keeping.rdlock();

        std::map<void*, h2m_alloc_info_t*>::iterator it = __book_keeping_allocations.find(m_info->ptr);
        if(it != __book_keeping_allocations.end()) {
            decrement_used_capacity(it->second->cur_mem_space, m_info->size);
            increment_used_capacity(m_info->pl->mem_space, m_info->size);
            it->second->nodemask            = m_info->nodemask;
            it->second->cur_mem_space       = m_info->pl->mem_space;
            it->second->cur_mem_partition   = m_info->pl->mem_partition;
            // TODO/FIXME: Do we need to update allocated_from?
        }

        __mtx_book_keeping.unlock();

    } else {
        RELP("ERROR: h2m_move_memory_mbind: Could not move data for ptr = " DPxMOD ". Returned %s\n", DPxPTR(m_info->ptr), h2m_result_types_t_str[err]);
    }
    return err;
}

int h2m_move_memory_mbind(const h2m_movement_info_t* m_info, int n_movements) {
    DBP(__VLVL_ENTRY_EXIT, "h2m_move_memory_mbind (n_movements=%d) (enter)\n", n_movements);
    verify_initialized();

    int ret = H2M_SUCCESS;
    { // extra scope for time recorder
        h2m_stat_recorder_t move_mbind_time_recorder(__stats_time_move_mbind);

        #if TOOL_SUPPORT
        if(tool_status.enabled && tool_status.callback_move_memory_mbind) {
            tool_status.callback_move_memory_mbind(m_info, n_movements, h2m_tool_movement_start);
        }
        #endif // TOOL_SUPPORT

        int tmp_thr = H2M_NUM_MIGRATION_THREADS.load();
        DBP(__VLVL_MIGRATION_DECISIONS, "h2m_move_memory_mbind: Executing with %d threads\n", tmp_thr);

        // First move allocations to LARGE_CAP to avoid issues with capacity limitations
        #pragma omp parallel for schedule(dynamic,1) num_threads(tmp_thr)
        for(int i = 0; i < n_movements; i++) {
            if (m_info[i].pl->mem_space == h2m_atv_mem_space_large_cap) {
                DBP(__VLVL_MIGRATION_DECISIONS, "move_memory_mbind (ptr=" DPxMOD ")\n", DPxPTR(m_info[i].ptr));
                int tmp = __move_memory_mbind_core(&(m_info[i]));
                if (tmp != H2M_SUCCESS) {
                    ret = tmp;
                }
            }
        }

        // Now move remaining allocations
        #pragma omp parallel for schedule(dynamic,1) num_threads(tmp_thr)
        for(int i = 0; i < n_movements; i++) {
            if (m_info[i].pl->mem_space != h2m_atv_mem_space_large_cap) {
                DBP(__VLVL_MIGRATION_DECISIONS, "move_memory_mbind (ptr=" DPxMOD ")\n", DPxPTR(m_info[i].ptr));
                int tmp = __move_memory_mbind_core(&(m_info[i]));
                if (tmp != H2M_SUCCESS) {
                    ret = tmp;
                }
            }
        }

        #if TOOL_SUPPORT
        if(tool_status.enabled && tool_status.callback_move_memory_mbind) {
            tool_status.callback_move_memory_mbind(m_info, n_movements, h2m_tool_movement_end);
        }
        #endif // TOOL_SUPPORT
    }

    DBP(__VLVL_ENTRY_EXIT, "h2m_move_memory_mbind (n_movements=%d) (exit)\n", n_movements);
    return ret;
}

int h2m_move_memory_movepages(const h2m_movement_info_t* m_info, int n_movements) {
    DBP(__VLVL_ENTRY_EXIT, "h2m_move_memory_movepages (enter)\n");
    verify_initialized();

    int ret = H2M_SUCCESS;

    { // extra scope for time recorder
        h2m_stat_recorder_t move_movepages_time_recorder(__stats_time_move_movepages);

        #if TOOL_SUPPORT
        if(tool_status.enabled && tool_status.callback_move_memory_movepages) {
            tool_status.callback_move_memory_movepages(m_info, n_movements, h2m_tool_movement_start);
        }
        #endif // TOOL_SUPPORT

        std::vector<void*>  l_pages;
        std::vector<int>    l_nodes;
        int stdps = getpagesize();

        for(int i = 0; i < n_movements; i ++) {
            if(m_info[i].pl->mem_partition == h2m_atv_mem_partition_interleaved ||
               m_info[i].pl->mem_partition == h2m_atv_mem_partition_first_touch ||
               m_info[i].pl->mem_partition == h2m_atv_mem_partition_blocked) {
                // TODO/FIXME: implement blocked
                // TODO/FIXME: can we actually do first-touch when moving data? keep NUMA balancer enabled and restrict to nodeset (=> seem only possible with mbind)
                // NUMA-aware view from corresponding cpu numa domain
                int cpu_numa_node = m_info[i].cpu_numa_node;
                if(cpu_numa_node == -1) {
                    cpu_numa_node = numa_node_of_cpu(sched_getcpu());
                }
                std::vector<int> &l_nodes_mem_space = __numa_data[cpu_numa_node].mem_space_mapping[m_info[i].pl->mem_space];
                int ctr_nodes = 0;
                int num_nodes = l_nodes_mem_space.size();

                for(size_t offset = 0; offset < m_info[i].size; offset += stdps) {
                    void *tmp_pointer = (void*)((char*)m_info[i].ptr + offset);
                    l_pages.push_back(tmp_pointer);
                    l_nodes.push_back(l_nodes_mem_space[ctr_nodes]);
                    ctr_nodes = (ctr_nodes+1) % num_nodes;
                }
            } else {
                int tmp_node = get_node_from_nodemask(m_info[i].nodemask);
                if(tmp_node == -1) {
                    RELP("ERROR: Getting Node for nodemask %s failed.\n", std::bitset<MAX_NUMA_NODES+2>(m_info[i].nodemask).to_string().c_str());
                    continue;
                }

                for(size_t offset = 0; offset < m_info[i].size; offset += stdps) {
                    void *tmp_pointer = (void*)((char*)m_info[i].ptr + offset);
                    l_pages.push_back(tmp_pointer);
                    l_nodes.push_back(tmp_node);
                }
            }
        }

        // actually move pages
        int err = move_memory_movepages(l_pages.size(), &(l_pages[0]), &(l_nodes[0]));
        if (err == -1) {
            DBP(__VLVL_ENTRY_EXIT, "h2m_move_memory_movepages (exit)\n");
            return H2M_FAILURE;
        } else if (err > 0) {
            ret = H2M_SOME_PAGES_NOT_MOVED;
        }

        // book keeping
        __mtx_book_keeping.rdlock();

        for (int i = 0; i < n_movements; i++) {
            std::map<void*, h2m_alloc_info_t*>::iterator it = __book_keeping_allocations.find(m_info[i].ptr);
            if(it != __book_keeping_allocations.end()) {
                decrement_used_capacity(it->second->cur_mem_space, m_info[i].size);
                increment_used_capacity(m_info[i].pl->mem_space, m_info[i].size);
                it->second->nodemask            = m_info[i].nodemask;
                it->second->cur_mem_space       = m_info[i].pl->mem_space;
                it->second->cur_mem_partition   = m_info[i].pl->mem_partition;
                // TODO/FIXME: Do we need to update allocated_from?
            }
        }

        __mtx_book_keeping.unlock();

        #if TOOL_SUPPORT
        if(tool_status.enabled && tool_status.callback_move_memory_movepages) {
            tool_status.callback_move_memory_movepages(m_info, n_movements, h2m_tool_movement_end);
        }
        #endif // TOOL_SUPPORT
    }

    DBP(__VLVL_ENTRY_EXIT, "h2m_move_memory_movepages (exit)\n");
    return ret;
}
#pragma endregion Low level interface

#pragma region Backgroud thread
//================================================================================
// Background thread
//================================================================================

void __action_progress_requests() {
    while (__migration_requests.size() > 0) {
        bool succ;
        h2m_migration_req_t req = __migration_requests.pop_front_b(&succ);
        if(succ) {
            DBP(__VLVL_MIGRATION_DECISIONS, "__action_progress_requests: picked up migration request and start processing\n");
            // apply the migration now
            int ret = __h2m_apply_migration_inner(req);
            // mark as completed
            req->is_completed = 1;
            DBP(__VLVL_MIGRATION_DECISIONS, "__action_progress_requests: processing migration request completed\n");
        }
    }
}

void* __bg_thread_action(void* arg) {
    // apply pinning of thread
    pin_bg_thread();

    // trigger signal to tell that thread is running now
    pthread_mutex_lock(&__mtx_bg_thread_start);
    __bg_thread_running = true;
    pthread_cond_signal(&__bg_thread_cond);
    pthread_mutex_unlock(&__mtx_bg_thread_start);

    DBP(__VLVL_ENTRY_EXIT, "__bg_thread_action (enter)\n");
    int us_sleep = H2M_THREAD_SLEEP_TIME_MICRO_SECS.load();

    while(true) {
        if(__flag_abort_bg_thread.load()) {
            DBP(__VLVL_ENTRY_EXIT, "__bg_thread_action (abort)\n");
            int ret_val = 0;
            pthread_exit(&ret_val);
        }
        __action_progress_requests();
        usleep(us_sleep);
    }

    DBP(__VLVL_ENTRY_EXIT, "__bg_thread_action (exit)\n");
}

int __start_bg_thread() {
    if(__bg_thread_created.load()) {
        return H2M_SUCCESS;
    }
    
    __mtx_bg_thread_create.lock();
    // need to check again
    if(__bg_thread_created.load()) {
        __mtx_bg_thread_create.unlock();
        return H2M_SUCCESS;
    }

    DBP(__VLVL_ENTRY_EXIT, "__start_bg_thread (enter)\n");
    
    // reset values
    __flag_abort_bg_thread = false;

    // explicitly make thread joinable to be portable
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    int err;
    err = pthread_create(&__bg_thread, &attr, __bg_thread_action, NULL);
    if(err != 0) {
        handle_error_en(err, "pthread_create - __bg_thread");
    }
    
    // wait until thread is running
    pthread_mutex_lock(&__mtx_bg_thread_start);
    while (!__bg_thread_running.load()) {
        pthread_cond_wait(&__bg_thread_cond, &__mtx_bg_thread_start);
    }
    pthread_mutex_unlock(&__mtx_bg_thread_start);

    // set flag to ensure that only a single thread is creating thread
    __bg_thread_created = true;
    __mtx_bg_thread_create.unlock();
    DBP(__VLVL_ENTRY_EXIT, "__start_bg_thread (exit)\n");
    return H2M_SUCCESS;
}

int __stop_bg_thread() {
    DBP(__VLVL_ENTRY_EXIT, "__stop_bg_thread (enter)\n");
    int err = 0;
    __flag_abort_bg_thread = true;
    // then wait for all threads to finish
    err = pthread_join(__bg_thread, NULL);
    if(err != 0) handle_error_en(err, "__stop_bg_thread - __bg_thread");

    // should be save to reset flags and counters here
    __bg_thread_running = false;
    __bg_thread_created = false;
    
    DBP(__VLVL_ENTRY_EXIT, "__stop_bg_thread (exit)\n");
    return H2M_SUCCESS;
}

#pragma endregion Backgroud thread

#ifdef __cplusplus
}
#endif
