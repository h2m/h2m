#ifndef __H2M_VERSION_H__
#define __H2M_VERSION_H__

#define H2M_VERSION_MAJOR 1
#define H2M_VERSION_MINOR 0
#define H2M_VERSION_BUILD 0

#define H2M_VERSION_STRING "1.0.0"

#endif // __H2M_VERSION_H__
