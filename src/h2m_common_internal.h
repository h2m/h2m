#ifndef __H2M_COMMON_INTERNAL_H__
#define __H2M_COMMON_INTERNAL_H__

// common includes or definintion of internal data structures

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "h2m.h"
#include "h2m_config.h"

#include <algorithm>
#include <assert.h>
#include <atomic>
#include <bitset>
#include <cassert>
#include <climits>
#include <cmath>
#include <cstdint>
#include <deque>
#include <dlfcn.h>
#include <errno.h>
#include <fstream>
#include <hwloc.h>
#include <inttypes.h>
#include <iterator>
#include <limits.h>
#include <link.h>
#include <list>
#include <map>
#include <mutex>
#include <numa.h>
#include <omp.h>
#include <pthread.h>
#include <sched.h>
#include <sstream>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <sys/syscall.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <unordered_map>
#include <vector>

#ifndef TOOL_SUPPORT
#define TOOL_SUPPORT 1
#endif

#ifndef MAX_NUMA_NODES
#define MAX_NUMA_NODES 8
#endif

#ifndef USE_RW_LOCK
#define USE_RW_LOCK 1
#endif

#include "h2m_tools.h"
#include "h2m_common.h"

#define WHITESPACES " \n\r\t\f\v"

#if defined(WIN32) || defined(_WIN32) 
#define PATH_SEPARATOR "\\" 
#else 
#define PATH_SEPARATOR "/" 
#endif 

#define FCN_MALLOC orig_malloc
#define FCN_ALIGNED_ALLOC orig_aligned_alloc
#define FCN_FREE orig_free

// ================================================================================
// Type Definitions
// ================================================================================

typedef void* (*fcn_malloc_t)(size_t size);
typedef void* (*fcn_calloc_t)(size_t num, size_t size);
typedef void* (*fcn_aligned_alloc_t)(size_t alignment, size_t size);
typedef void* (*fcn_memalign_t)(size_t alignment, size_t size);
typedef int   (*fcn_pmemalign_t)(void **memptr, size_t alignment, size_t size);
typedef void* (*fcn_realloc_t)(void *ptr, size_t size);
typedef void  (*fcn_free_t)(void *ptr);

typedef struct h2m_thread_data_t {
    int os_thread_id;
    int tmp_json_thread_id;
#if TOOL_SUPPORT
    h2m_tool_data_t thread_tool_data;
#endif
} h2m_thread_data_t;

typedef struct h2m_numa_node_data_t {
    std::map<h2m_alloc_trait_value_t, std::vector<int>> mem_space_mapping;
} h2m_numa_node_data_t;

template<class T>
class thread_safe_list_t {
    private:

    std::list<T> list;
    std::mutex m;
    std::atomic<size_t> list_size;

    public:

    thread_safe_list_t() { this->list_size = 0; }

    size_t size() {
        return this->list_size.load();
    }

    bool empty() {
        return this->list_size <= 0;
    }

    void push_back(T entry) {
        this->m.lock();
        this->list.push_back(entry);
        this->list_size++;
        this->m.unlock();
    }

    void remove(T entry) {
        this->m.lock();
        this->list.remove(entry);
        this->list_size--;
        this->m.unlock();
    }

    T pop_front() {
        if(this->empty())
            return NULL;

        T ret_val;

        this->m.lock();
        if(!this->empty()) {
            this->list_size--;
            ret_val = this->list.front();
            this->list.pop_front();
        }
        this->m.unlock();
        return ret_val;
    }

    T pop_front_b(bool *success) {
        *success = true;
        if(this->empty()) {
            *success = false;
            if (std::is_pointer<T>::value) {
                return nullptr;
            } else {
                return 0;
            }
        }

        T ret_val;

        this->m.lock();
        if(!this->empty()) {
            this->list_size--;
            ret_val = this->list.front();
            this->list.pop_front();
        } else {
            this->m.unlock();
            *success = false;
            if (std::is_pointer<T>::value) {
                return nullptr;
            } else {
                return 0;
            }
        }
        this->m.unlock();
        return ret_val;
    }

    bool find(T entry) {
        this->m.lock();
        bool found = (std::find(this->list.begin(), this->list.end(), entry) != this->list.end());
        this->m.unlock();
        return found;
    }
};

template<class T>
class thread_safe_deque_t {
    private:

    std::deque<T> list;
    std::mutex m;
    std::atomic<size_t> list_size;

    public:

    thread_safe_deque_t() { this->list_size = 0; }

    size_t size() {
        return this->list_size.load();
    }

    bool empty() {
        return this->list_size <= 0;
    }

    void push_back(T entry) {
        this->m.lock();
        this->list.push_back(entry);
        this->list_size++;
        this->m.unlock();
    }

    void push_front(T entry) {
        this->m.lock();
        this->list.push_front(entry);
        this->list_size++;
        this->m.unlock();
    }

    T pop_front() {
        if(this->empty())
            return NULL;

        T ret_val;

        this->m.lock();
        if(!this->empty()) {
            this->list_size--;
            ret_val = this->list.front();
            this->list.pop_front();
        } else {
            this->m.unlock();
            return NULL;
        }
        this->m.unlock();
        return ret_val;
    }

    T pop_front_b(bool *success) {
        *success = true;
        if(this->empty()) {
            *success = false;
            // if (std::is_fundamental<T>::value)
            //     return -1;
            // else
            //     return NULL;
            return 0;
        }

        T ret_val;

        this->m.lock();
        if(!this->empty()) {
            this->list_size--;
            ret_val = this->list.front();
            this->list.pop_front();
        } else {
            this->m.unlock();
            *success = false;
            // if (std::is_fundamental<T>::value)
            //     return -1;
            // else
            //     return NULL;
            return 0;
        }
        this->m.unlock();
        return ret_val;
    }
};

typedef struct h2m_phase_transition_t {
    int id;
    char* name;
    uint64_t ts;

    int n_migrations;
    double elapsed_sec_migrations;
    // h2m_stack_trace_t* stack_trace;
} h2m_phase_transition_t;

struct h2m_rwlock_t {
#if USE_RW_LOCK
    using mtx_type = pthread_rwlock_t;
    void rdlock() { pthread_rwlock_rdlock(&mtx_); }
    void wrlock() { pthread_rwlock_wrlock(&mtx_); }
    void unlock() { pthread_rwlock_unlock(&mtx_); }

    h2m_rwlock_t() : mtx_(PTHREAD_RWLOCK_INITIALIZER) { }
#else
    using mtx_type = std::mutex;
    void rdlock() { wrlock(); } // rdlock == wrlock
    void wrlock() { mtx_.lock(); }
    void unlock() { mtx_.unlock(); }

    h2m_rwlock_t() = default;
#endif // USE_RW_LOCK

    mtx_type mtx_;
};

// ================================================================================
// Variables
// ================================================================================

// original cpuset of the complete process
// (needs to be recorded in serial region at the beginning of the applicatrion)
extern cpu_set_t            __pid_mask;

// atomic counter for thread ids
extern std::atomic<int>     __thread_counter;
extern __thread int         __h2m_gtid;

// mutex section
extern std::mutex           __mtx_relp;

// data objects
extern h2m_thread_data_t*   __thread_data;

// profiling & tracing
extern std::atomic<int>     __tracing_enabled;

extern h2m_rwlock_t         __mtx_book_keeping;

// book keeping for regular allocations (w/o grouping or splitting for now)
extern std::map<void*, h2m_alloc_info_t*>   __book_keeping_allocations;

// mapping between memory spaces and numa nodes (e.g. DRAM, HBM, NVM)
extern std::map<h2m_alloc_trait_value_t, std::vector<int>>  __mapping_mem_space_nodes;
extern std::map<h2m_alloc_trait_value_t, unsigned long>     __mapping_mem_space_nodemask;
// memory capcaity cap and used memory for each nodemask
extern std::map<unsigned long, size_t>                     __mapping_nodemask_max_mem_cap;
extern std::map<unsigned long, size_t>                     __mapping_nodemask_mem_used;

extern h2m_numa_node_data_t *__numa_data;
extern h2m_memory_characteristics_t __mem_characteristics;

// Function references to real allocation functions (to avoid that these are intercepted)
extern fcn_malloc_t         orig_malloc;
extern fcn_aligned_alloc_t  orig_aligned_alloc;
extern fcn_memalign_t       orig_memalign;
extern fcn_pmemalign_t      orig_pmemalign;
extern fcn_calloc_t         orig_calloc;
extern fcn_realloc_t        orig_realloc;
extern fcn_free_t           orig_free;

// ============================================================
// Severity levels for outputs
// ============================================================

// Overview debug verbosity levels:
#define __VLVL_ENTRY_EXIT 30            // Function entry and exit points
#define __VLVL_ENTRY_EXIT_VERBOSE 35    // Function entry and exit points for heavily called functions
#define __VLVL_NUMA_AND_MEM 10          // NUMA map information and memory characteristics
#define __VLVL_TOOL 10                  // Tool init and finish
#define __VLVL_MIGRATION_DECISIONS 25   // Effective allocation spaces and migration decisions
#define __VLVL_MEM_SPACE_CAPACITY 28    // Updates about memory capacity limit
#define __VLVL_PRINT_TRAITS 40          // Traits for allocation

// ================================================================================
// Functions & Function Defines
// ================================================================================

void  atomic_add_size_t(std::atomic<size_t> &dbl, size_t d);
void  atomic_sub_size_t(std::atomic<size_t> &dbl, size_t d);

// Memory capacity management
size_t get_max_capacity(h2m_alloc_trait_value_t mem_space);
size_t get_used_capacity(h2m_alloc_trait_value_t mem_space);
int    increment_used_capacity(h2m_alloc_trait_value_t mem_space, size_t size);
int    decrement_used_capacity(h2m_alloc_trait_value_t mem_space, size_t size);

// String utils

void  string_split(const std::string& str, std::vector<std::string>& list, char delim);
std::string rtrim(const std::string &s);
std::string ltrim(const std::string &s);
std::string trim(const std::string &s);

static inline uint64_t new_date_ns() {
    struct timespec t;
    clock_gettime(CLOCK_MONOTONIC_RAW, &t);
    return t.tv_sec*1e9+t.tv_nsec;
}

// Configs and charateristics

void  build_mem_space_mapping();
void  determine_max_mem_for_mem_spaces();
void  init_mem_characteristics();
void  parse_mem_characteristics();
void  print_mem_characteristics();
void  cleanup_mem_characteristics();
h2m_alloc_info_t* get_alloc_info_ref(void *ptr, int *out_found);
h2m_alloc_info_t* get_alloc_info_ref_for_json_id(int json_id, int *out_found);

bool  check_overall_space_restriction(
        std::vector<int> &mem_space_nodes,
        h2m_alloc_trait_value_t mem_space,
        size_t size_req);
short pin_bg_thread();
int callback_iterate(struct dl_phdr_info *info, size_t size, void *data);

#ifndef RELP
#define RELP( ... ) h2m_dbg_print(H2M_RANK.load(), 0, __VA_ARGS__);
#endif

#ifndef DBP
#ifdef H2M_DEBUG
#define DBP(lvl, ... ) h2m_dbg_print(H2M_RANK.load(), lvl, __VA_ARGS__);
#else
#define DBP(lvl, ... ) { }
#endif
#endif

#define handle_error_en(en, msg) \
    do { errno = en; RELP("ERROR: %s : %s\n", msg, strerror(en)); exit(EXIT_FAILURE); } while (0)

#ifndef VT_BEGIN_CONSTRAINED
#define VT_BEGIN_CONSTRAINED(event_id) if (__tracing_enabled) VT_begin(event_id);
#endif

#ifndef VT_END_W_CONSTRAINED
#define VT_END_W_CONSTRAINED(event_id) if (__tracing_enabled) VT_end(event_id);
#endif

#define gettid() ((pid_t)syscall(SYS_gettid))

#endif // __H2M_COMMON_INTERNAL_H__
