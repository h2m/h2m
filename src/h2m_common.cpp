#include "h2m_common_internal.h"
#include "h2m_common.h"
#include "h2m_utils_numa.h"
#include "h2m_config.h"
#include <iomanip>

// ================================================================================
// Variables
// ================================================================================
// original cpuset of the complete process
// (needs to be recorded in serial region at the beginning of the applicatrion)
cpu_set_t               __pid_mask;

// atomic counter for thread ids
std::atomic<int>        __thread_counter;

__thread int            __h2m_gtid;

std::mutex              __mtx_relp;
h2m_thread_data_t*      __thread_data;

// profiling & tracing
std::atomic<int>        __tracing_enabled;

h2m_rwlock_t            __mtx_book_keeping;

// book keeping for regular allocations (w/o grouping or splitting for now)
std::map<void*, h2m_alloc_info_t*>  __book_keeping_allocations;

// mapping between memory spaces and numa nodes (e.g. DRAM, HBM, NVM)
std::map<h2m_alloc_trait_value_t, std::vector<int>> __mapping_mem_space_nodes;
std::map<h2m_alloc_trait_value_t, unsigned long>    __mapping_mem_space_nodemask;
// memory capcaity cap and used memory for each nodemask (in bytes)
std::map<unsigned long, size_t>                     __mapping_nodemask_max_mem_cap;
std::map<unsigned long, size_t>                     __mapping_nodemask_mem_used;
static std::mutex                                   __mtx_used_cap;
h2m_numa_node_data_t *__numa_data;
h2m_memory_characteristics_t __mem_characteristics;

// Function references to real allocation functions (to avoid that these are intercepted)
fcn_malloc_t         orig_malloc = nullptr;
fcn_aligned_alloc_t  orig_aligned_alloc = nullptr;
fcn_memalign_t       orig_memalign = nullptr;
fcn_pmemalign_t      orig_pmemalign = nullptr;
fcn_calloc_t         orig_calloc = nullptr;
fcn_realloc_t        orig_realloc = nullptr;
fcn_free_t           orig_free = nullptr;

// ================================================================================
// Functions
// ================================================================================

void atomic_add_size_t(std::atomic<size_t> &val, size_t d) {
    size_t old = val.load(std::memory_order_consume);
    size_t desired = old + d;
    while (!val.compare_exchange_weak(old, desired,
        std::memory_order_release, std::memory_order_consume)) {
        desired = old + d;
    }
}

void atomic_sub_size_t(std::atomic<size_t> &val, size_t d) {
    size_t old = val.load(std::memory_order_consume);
    size_t desired = old - d;
    while (!val.compare_exchange_weak(old, desired,
        std::memory_order_release, std::memory_order_consume)) {
        desired = old - d;
    }
}

int h2m_get_gtid() {
    if(__h2m_gtid != -1) {
        return __h2m_gtid;
    }

    __h2m_gtid = __thread_counter++;
    __thread_data[__h2m_gtid].os_thread_id = syscall(SYS_gettid);

    return __h2m_gtid;
}

void h2m_dbg_print_help(int print_prefix, const char * prefix, int rank, int lvl, va_list args) {
    timeval curTime;
    gettimeofday(&curTime, NULL);
    unsigned int milli = curTime.tv_usec / 1000;
    unsigned int micro_sec = curTime.tv_usec % 1000;
    char buffer [80];
    strftime(buffer, 80, "%Y-%m-%d %H:%M:%S", localtime(&curTime.tv_sec));
    char currentTime[88] = "";
    sprintf(currentTime, "%s.%03d.%03d", buffer, milli, micro_sec);

    __mtx_relp.lock();
    if(print_prefix) {
        fprintf(stderr, "%s %s R#%03d T#%03d (OS_TID:%ld) Lvl\t%d\t --> ", currentTime, prefix, rank, omp_get_thread_num(), syscall(SYS_gettid), lvl);
    }
    // get format (first element from var args)
    const char *fmt = va_arg(args, const char *);
    vfprintf(stderr, fmt, args);
    __mtx_relp.unlock();
}

void h2m_dbg_print(int rank, int lvl, ... ) {
    if(H2M_VERBOSITY_LVL.load() >= lvl) {
        va_list args;
        va_start(args, lvl);
        h2m_dbg_print_help(1, "H2M-Lib", rank, lvl, args);
        va_end (args);
    }
}

void string_split(const std::string& str, std::vector<std::string>& list, char delim) {
    std::stringstream ss(str);
    std::string token;
    while (std::getline(ss, token, delim)) {
        list.push_back(token);
    }
}

std::string ltrim(const std::string &s) {
    size_t start = s.find_first_not_of(WHITESPACES);
    return (start == std::string::npos) ? "" : s.substr(start);
}
 
std::string rtrim(const std::string &s) {
    size_t end = s.find_last_not_of(WHITESPACES);
    return (end == std::string::npos) ? "" : s.substr(0, end + 1);
}
 
std::string trim(const std::string &s) {
    return rtrim(ltrim(s));
}

short pin_bg_thread() {
    int err;
    int s, j;
    pthread_t thread;
    cpu_set_t new_cpu_set;

    // this only reflects binding of current thread. Problem when OpenMP already pinned threads due to OMP_PLACES and OMP_PROC_BIND. 
    // then comm thread only get cpuset to single core --> overdecomposition of core with computational and communication thread  
    // err = sched_getaffinity(getpid(), sizeof(cpu_set_t), &pid_mask);
    // if(err != 0) handle_error_en(err, "sched_getaffinity");

    hwloc_topology_t topology;
    err = hwloc_topology_init(&topology);
    err = hwloc_topology_load(topology);

    // also get the number of processing units (here)
    int depth = hwloc_get_type_depth(topology, HWLOC_OBJ_CORE);
    if(depth == HWLOC_TYPE_DEPTH_UNKNOWN) {
        handle_error_en(1001, "hwloc_get_type_depth");
    }
    const long n_physical_cores = hwloc_get_nbobjs_by_depth(topology, depth);
    const long n_logical_cores = sysconf( _SC_NPROCESSORS_ONLN );
    int ht_enabled = n_logical_cores > n_physical_cores;
    hwloc_topology_destroy(topology);

    if(H2M_PRINT_AFFINITY_MASKS.load()) {
        RELP("Original affinity mask:\n");
        print_affinity_mask(__pid_mask);
    }

    // create empty mask
    CPU_ZERO(&new_cpu_set);

    
    int mode = H2M_BACKGROUND_THREAD_PIN_MODE.load();
    if(mode == 0) {
        // Case: set mask to last core in original process cpu_set
        long last_core_in_set = 0;
        for (long i = n_logical_cores-1; i >= 0; i--) {
            //RELP("core/hw thread %ld not set in cpuset\n", i);
            if (CPU_ISSET(i, &__pid_mask)) {
                // RELP("Last core/hw thread in cpuset is %ld\n", i);
                last_core_in_set = i;
                break;
            }
        }
        
        if(!ht_enabled) {
            // Case: there are no hyper threads
            if(H2M_PRINT_AFFINITY_MASKS.load()) {
                RELP("BG_THREAD: Setting thread affinity to core %ld\n", last_core_in_set);
            }
            CPU_SET(last_core_in_set, &new_cpu_set);
        } else {
            // Case: there are at least 2 HT per core
            std::string cores(std::to_string(last_core_in_set));
            CPU_SET(last_core_in_set, &new_cpu_set);
            for(long i = last_core_in_set-n_physical_cores; i >= 0; i-=n_physical_cores) {
                cores = std::to_string(i)  + "," + cores;
                CPU_SET(i, &new_cpu_set);
            }
            if(H2M_PRINT_AFFINITY_MASKS.load()) {
                RELP("BG_THREAD: Setting thread affinity to cores %s\n", cores.c_str());
            }
        }        
    } else if(mode == 1) {
        // Case: set mask to complete machine (OS can move thread around)
        RELP("BG_THREAD: Setting thread affinity to complete machine\n");
        for(long i = 0; i < n_logical_cores; i ++) {
            CPU_SET(i, &new_cpu_set);
        }
    } else if(mode == 2) {
        RELP("BG_THREAD: Setting thread affinity to core %d\n", H2M_BACKGROUND_THREAD_PIN_CORE.load());
        // Case: fixed core specified through environment variable
        CPU_SET(H2M_BACKGROUND_THREAD_PIN_CORE.load(), &new_cpu_set);
    } else {
        RELP("WARNING: Invalid value for H2M_BACKGROUND_THREAD_PIN_MODE ==> %d. Setting affinity to core 0.\n", mode);
        CPU_SET(0, &new_cpu_set);
    }
    
    // set affinity now
    thread = pthread_self();
    err = pthread_setaffinity_np(thread, sizeof(cpu_set_t), &new_cpu_set);
    if (err != 0) {
        handle_error_en(err, "pthread_setaffinity_np");
    }

    if(H2M_PRINT_AFFINITY_MASKS.load()) {
        // verify that pinning worked
        cpu_set_t final_cpu_set;
        err = pthread_getaffinity_np(thread, sizeof(cpu_set_t), &final_cpu_set);
        if (err != 0) {
            handle_error_en(err, "pthread_getaffinity_np");
        }
        std::string final_cores("");
        for (int j = 0; j < n_logical_cores; j++) {
            if (CPU_ISSET(j, &final_cpu_set)) {
                final_cores += std::to_string(j) + ",";
            }
        }
        final_cores.pop_back(); // remove comma
        RELP("BG_THREAD: Verifying BG_THREAD affinity: pinned to cores %s\n", final_cores.c_str());
    }

    return H2M_SUCCESS;
}

size_t get_max_capacity(h2m_alloc_trait_value_t mem_space) {
    if (__mapping_mem_space_nodemask.count(mem_space) == 0) {
        RELP("WARNING: get_max_capacity: Invalid value for mem_space ==> %d\n", (int)mem_space);
        return INT_MAX;
    }
    unsigned long nodemask = __mapping_mem_space_nodemask[mem_space];
    return __mapping_nodemask_max_mem_cap[nodemask];
}

size_t get_used_capacity(h2m_alloc_trait_value_t mem_space) {
    if (__mapping_mem_space_nodemask.count(mem_space) == 0) {
        RELP("WARNING: get_used_capacity: Invalid value for mem_space ==> %d\n", (int)mem_space);
        return INT_MAX;
    }
    unsigned long nodemask = __mapping_mem_space_nodemask[mem_space];
    __mtx_used_cap.lock();
    size_t ret = __mapping_nodemask_mem_used[nodemask];
    __mtx_used_cap.unlock();
    return ret;
}

int increment_used_capacity(h2m_alloc_trait_value_t mem_space, size_t size) {
    if (__mapping_mem_space_nodemask.count(mem_space) == 0) {
        RELP("WARNING: increment_used_capacity: Invalid value for mem_space ==> %d\n", (int)mem_space);
        return H2M_INVALID_MEM_SPACE;
    }
    size_t cap_before;
    unsigned long nodemask = __mapping_mem_space_nodemask[mem_space];
    __mtx_used_cap.lock();
    cap_before = __mapping_nodemask_mem_used[nodemask];
    __mapping_nodemask_mem_used[nodemask] += size;
    __mtx_used_cap.unlock();
    DBP(__VLVL_MEM_SPACE_CAPACITY, "Capacity change (in bytes): mem-space=%d before=%zu increment=%zu now=%zu\n", mem_space, cap_before, size, cap_before+size);
    return H2M_SUCCESS;
}

int decrement_used_capacity(h2m_alloc_trait_value_t mem_space, size_t size) {
    if (__mapping_mem_space_nodemask.count(mem_space) == 0) {
        RELP("WARNING: decrement_used_capacity: Invalid value for mem_space ==> %d\n", (int)mem_space);
        return H2M_INVALID_MEM_SPACE;
    }
    size_t cap_before;
    unsigned long nodemask = __mapping_mem_space_nodemask[mem_space];
    __mtx_used_cap.lock();
    cap_before = __mapping_nodemask_mem_used[nodemask];
    __mapping_nodemask_mem_used[nodemask] -= size;    
    __mtx_used_cap.unlock();
    DBP(__VLVL_MEM_SPACE_CAPACITY, "Capacity change (in bytes): mem-space=%d before=%zu decrement=%zu now=%zu\n", mem_space, cap_before, size, cap_before-size);
    return H2M_SUCCESS;
}

h2m_capacity_usage_info_t h2m_get_capacity_usage_info() {
    h2m_capacity_usage_info_t ret;
    ret.mem_bytes_used_hbw          = get_used_capacity(h2m_atv_mem_space_hbw);
    ret.mem_bytes_used_low_lat      = get_used_capacity(h2m_atv_mem_space_low_lat);
    ret.mem_bytes_used_large_cap    = get_used_capacity(h2m_atv_mem_space_large_cap);

    ret.nodemask_hbw                = __mapping_mem_space_nodemask[h2m_atv_mem_space_hbw];
    ret.nodemask_low_lat            = __mapping_mem_space_nodemask[h2m_atv_mem_space_low_lat];
    ret.nodemask_large_cap          = __mapping_mem_space_nodemask[h2m_atv_mem_space_large_cap];
    
    ret.max_mem_cap_bytes_hbw       = get_max_capacity(h2m_atv_mem_space_hbw);
    ret.max_mem_cap_bytes_low_lat   = get_max_capacity(h2m_atv_mem_space_low_lat);
    ret.max_mem_cap_bytes_large_cap = get_max_capacity(h2m_atv_mem_space_large_cap);
    ret.max_mem_cap_MB_hbw          = H2M_MAX_MEM_CAP_OVERALL_MB_HBW.load();
    ret.max_mem_cap_MB_low_lat      = H2M_MAX_MEM_CAP_OVERALL_MB_LOW_LAT.load();
    ret.max_mem_cap_MB_large_cap    = H2M_MAX_MEM_CAP_OVERALL_MB_LARGE_CAP.load();
    return ret;
}

h2m_memory_characteristics_t h2m_get_mem_characteristics() {
    return __mem_characteristics;
}

bool check_overall_space_restriction(std::vector<int> &mem_space_nodes, h2m_alloc_trait_value_t mem_space, size_t size_req) {
    DBP(__VLVL_ENTRY_EXIT_VERBOSE, "check_overall_space_restriction (enter)\n");
    // TODO/FIXME: How to handle race condition on used and max capacity?
    size_t tmp_max_cap  = get_max_capacity(mem_space);
    size_t tmp_used_cap = get_used_capacity(mem_space);

    // check mem space limits set by user
    if ((tmp_used_cap + size_req) > tmp_max_cap) {
        DBP(__VLVL_ENTRY_EXIT_VERBOSE, "check_overall_space_restriction (exit)\n");
        return false;
    }
    DBP(__VLVL_ENTRY_EXIT_VERBOSE, "check_overall_space_restriction (exit)\n");
    return true;
}

void build_mem_space_mapping() {
    // clean first
    __mapping_mem_space_nodes.clear();
    __mapping_mem_space_nodemask.clear();

    // retreive the real type of nodes
    std::vector<int> nodes_real_hbm;
    std::vector<int> nodes_real_dram;
    std::vector<int> nodes_real_nvm;

    hwloc_topology_t topology;
    hwloc_obj_t obj;

    /* Perform the topology detection. */
    hwloc_topology_init(&topology);
    hwloc_topology_load(topology);
    
    int n_nodes = hwloc_get_nbobjs_by_type(topology, HWLOC_OBJ_NUMANODE);
    DBP(__VLVL_NUMA_AND_MEM, "Number of NUMA nodes: %d\n", n_nodes);

    for(int i = 0; i < n_nodes; i++) {
        int offset = 0;
        char tmp_type[128];
        char tmp_attr[128];

        obj     = hwloc_get_obj_by_type(topology, HWLOC_OBJ_NUMANODE, i);
        offset  = hwloc_obj_type_snprintf(tmp_type, sizeof(tmp_type), obj, 1);
        if (obj->subtype) {
            offset += sprintf(tmp_type + offset, " (%s)", obj->subtype);
        }
        hwloc_obj_attr_snprintf(tmp_attr, sizeof(tmp_attr), obj, ",", 1);

        DBP(__VLVL_NUMA_AND_MEM, "%s Idx=%d, os_index=%d - %s\n", tmp_type, i, obj->os_index, tmp_attr);

        if(strstr(tmp_type, "MCDRAM") != NULL) {
            nodes_real_hbm.push_back(obj->os_index);
        } else if(strstr(tmp_attr, "DAXDevice=") != NULL) {
            nodes_real_nvm.push_back(obj->os_index);
        } else {
            nodes_real_dram.push_back(obj->os_index);
        }
    }

    // destroy topology again
    hwloc_topology_destroy(topology);

    std::vector<std::string> vec;
    std::vector<int> vec_int;

    // now fill global arrays - HBM
    if (H2M_NODES_HBW.load()) {
        DBP(__VLVL_NUMA_AND_MEM, "Mapping h2m_atv_mem_space_hbw -> %s\n", H2M_NODES_HBW.load());
        vec.clear(); vec_int.clear();
        string_split(std::string(H2M_NODES_HBW.load()), vec, ',');
        for(const std::string& el : vec) {
            vec_int.push_back(std::atoi(el.c_str()));
        }
        __mapping_mem_space_nodes[h2m_atv_mem_space_hbw] = std::vector<int>(vec_int);
    } else if (nodes_real_hbm.size() > 0) {
        DBP(__VLVL_NUMA_AND_MEM, "Mapping h2m_atv_mem_space_hbw -> HBM\n");
        __mapping_mem_space_nodes[h2m_atv_mem_space_hbw] = std::vector<int>(nodes_real_hbm);
    } else {
        DBP(__VLVL_NUMA_AND_MEM, "Mapping h2m_atv_mem_space_hbw -> DRAM\n");
        __mapping_mem_space_nodes[h2m_atv_mem_space_hbw] = std::vector<int>(nodes_real_dram);
    }

    unsigned long tmp_node_mask = 0;
    for(int n : __mapping_mem_space_nodes[h2m_atv_mem_space_hbw]) {
        tmp_node_mask |= 1 << n;
    }
    __mapping_mem_space_nodemask[h2m_atv_mem_space_hbw] = tmp_node_mask;
    DBP(__VLVL_NUMA_AND_MEM, "Nodemask for h2m_atv_mem_space_hbw is %s\n", std::bitset<MAX_NUMA_NODES+2>(tmp_node_mask).to_string().c_str());

    // now fill global arrays - Low Latency
    if (H2M_NODES_LOW_LAT.load()) {
        DBP(__VLVL_NUMA_AND_MEM, "Mapping h2m_atv_mem_space_low_lat -> %s\n", H2M_NODES_LOW_LAT.load());
        vec.clear(); vec_int.clear();
        string_split(std::string(H2M_NODES_LOW_LAT.load()), vec, ',');
        for(const std::string& el : vec) {
            vec_int.push_back(std::atoi(el.c_str()));
        }
        __mapping_mem_space_nodes[h2m_atv_mem_space_low_lat] = std::vector<int>(vec_int);
    } else {
        DBP(__VLVL_NUMA_AND_MEM, "Mapping h2m_atv_mem_space_low_lat -> DRAM\n");
        __mapping_mem_space_nodes[h2m_atv_mem_space_low_lat] = std::vector<int>(nodes_real_dram);
    }

    tmp_node_mask = 0;
    for(int n : __mapping_mem_space_nodes[h2m_atv_mem_space_low_lat]) {
        tmp_node_mask |= 1 << n;
    }
    __mapping_mem_space_nodemask[h2m_atv_mem_space_low_lat] = tmp_node_mask;
    DBP(__VLVL_NUMA_AND_MEM, "Nodemask for h2m_atv_mem_space_low_lat is %s\n", std::bitset<MAX_NUMA_NODES+2>(tmp_node_mask).to_string().c_str());

    // now fill global arrays - Large Capacity
    if (H2M_NODES_LARGE_CAP.load()) {
        DBP(__VLVL_NUMA_AND_MEM, "Mapping h2m_atv_mem_space_large_cap -> %s\n", H2M_NODES_LARGE_CAP.load());
        vec.clear(); vec_int.clear();
        string_split(std::string(H2M_NODES_LARGE_CAP.load()), vec, ',');
        for(const std::string& el : vec) {
            vec_int.push_back(std::atoi(el.c_str()));
        }
        __mapping_mem_space_nodes[h2m_atv_mem_space_large_cap] = std::vector<int>(vec_int);
    } else if (nodes_real_nvm.size() > 0) {
        DBP(__VLVL_NUMA_AND_MEM, "Mapping h2m_atv_mem_space_large_cap -> NVM\n");
        __mapping_mem_space_nodes[h2m_atv_mem_space_large_cap] = std::vector<int>(nodes_real_nvm);
    } else {
        DBP(__VLVL_NUMA_AND_MEM, "Mapping h2m_atv_mem_space_large_cap -> DRAM\n");
        __mapping_mem_space_nodes[h2m_atv_mem_space_large_cap] = std::vector<int>(nodes_real_dram);        
    }

    tmp_node_mask = 0;
    for(int n : __mapping_mem_space_nodes[h2m_atv_mem_space_large_cap]) {
        tmp_node_mask |= 1 << n;
    }
    __mapping_mem_space_nodemask[h2m_atv_mem_space_large_cap] = tmp_node_mask;
    DBP(__VLVL_NUMA_AND_MEM, "Nodemask for h2m_atv_mem_space_large_cap is %s\n", std::bitset<MAX_NUMA_NODES+2>(tmp_node_mask).to_string().c_str());

    // Now create numa aware views on nodes
    n_nodes = numa_num_configured_nodes();
    for(int i = 0; i < n_nodes; i++) {
        // clear map first
        __numa_data[i].mem_space_mapping.clear();

        std::vector<std::pair<int, int> > tmp_nodes;
        for(int j = 0; j < n_nodes; j++) {
            tmp_nodes.push_back(std::make_pair(j, numa_distance(i, j)));
        }
        std::sort(tmp_nodes.begin(), tmp_nodes.end(), [](const std::pair<int,int> &left, const std::pair<int,int> &right) {
            return left.second < right.second;
        });

        #if defined(H2M_DEBUG)
        if (H2M_VERBOSITY_LVL.load() >= __VLVL_NUMA_AND_MEM) {
            std::stringstream str_nodes;
            str_nodes << "Source node " << i << " - Sorted by distances: ";
            for(int j = 0; j < tmp_nodes.size(); j++) {
                str_nodes << "(N: " << tmp_nodes[j].first << ", D: " << tmp_nodes[j].second << ") ";
            }
            DBP(__VLVL_NUMA_AND_MEM, "%s\n", str_nodes.str().c_str());
        }
        #endif // H2M_DEBUG

        h2m_alloc_trait_value_t mem_spaces[3] = {h2m_atv_mem_space_hbw, h2m_atv_mem_space_low_lat, h2m_atv_mem_space_large_cap};
        for(int j = 0; j < n_nodes; j++) {
            int tmp_node = tmp_nodes[j].first;
            for (const h2m_alloc_trait_value_t mem : mem_spaces) {
                std::vector<int> &v = __mapping_mem_space_nodes[mem];
                if (std::find(v.begin(), v.end(), tmp_node) != v.end()) {
                    __numa_data[i].mem_space_mapping[mem].push_back(tmp_node);
                }
            }
        }

        #if defined(H2M_DEBUG)
        if (H2M_VERBOSITY_LVL.load() >= __VLVL_NUMA_AND_MEM) {
            for (const h2m_alloc_trait_value_t mem : mem_spaces) {
                std::stringstream str_numa;
                char mem_space_name[32];
                h2m_alloc_trait_value_t_str(mem, mem_space_name);
                str_numa << "Resulting view from numa node " << i << " and memspace " << mem_space_name << ": ";
                for(int j = 0; j < __numa_data[i].mem_space_mapping[mem].size(); j++) {
                    str_numa << __numa_data[i].mem_space_mapping[mem][j] << " ";
                }
                DBP(__VLVL_NUMA_AND_MEM, "%s\n", str_numa.str().c_str());
            }
        }
        #endif // H2M_DEBUG
    }
}

void determine_max_mem_for_mem_spaces() {
    // get the pyhsical NUMA capacities once (in bytes)
    std::map<int, size_t> avail_space = get_numa_capacities();
    
    h2m_alloc_trait_value_t mem_spaces[3] = {
        h2m_atv_mem_space_hbw,
        h2m_atv_mem_space_large_cap,
        h2m_atv_mem_space_low_lat
    };

    for(int i = 0; i < 3; i++) {
        h2m_alloc_trait_value_t cur_ms = mem_spaces[i];
        // get numa nodes involved in mem space
        int cpu_numa_node = numa_node_of_cpu(sched_getcpu());
        std::vector<int> &node_list = __numa_data[cpu_numa_node].mem_space_mapping[cur_ms];
        unsigned long nodemask = __mapping_mem_space_nodemask[cur_ms];

        size_t tmp_sum_bytes = 0;
        for(int n = 0; n < node_list.size(); n++) {
            tmp_sum_bytes += avail_space[node_list[n]];
        }
        // convert to MB
        int tmp_sum_mb = (int)(tmp_sum_bytes / (1000*1000));

        if(cur_ms == h2m_atv_mem_space_hbw) {            
            if(tmp_sum_mb < H2M_MAX_MEM_CAP_OVERALL_MB_HBW.load()) {
                H2M_MAX_MEM_CAP_OVERALL_MB_HBW = tmp_sum_mb;
            } else {
                tmp_sum_mb = H2M_MAX_MEM_CAP_OVERALL_MB_HBW.load();
            }
        } else if(cur_ms == h2m_atv_mem_space_large_cap) {
            if(tmp_sum_mb < H2M_MAX_MEM_CAP_OVERALL_MB_LARGE_CAP.load()) {
                H2M_MAX_MEM_CAP_OVERALL_MB_LARGE_CAP = tmp_sum_mb;
            } else {
                tmp_sum_mb = H2M_MAX_MEM_CAP_OVERALL_MB_LARGE_CAP.load();
            }
        } else if(cur_ms == h2m_atv_mem_space_low_lat) {
            if(tmp_sum_mb < H2M_MAX_MEM_CAP_OVERALL_MB_LOW_LAT.load()) {
                H2M_MAX_MEM_CAP_OVERALL_MB_LOW_LAT = tmp_sum_mb;
            } else {
                tmp_sum_mb = H2M_MAX_MEM_CAP_OVERALL_MB_LOW_LAT.load();
            }
        }
        
        size_t cur_size_bytes = ((size_t)tmp_sum_mb) * 1000 * 1000;
        // check if there is already a value for nodemask
        if (__mapping_nodemask_max_mem_cap.count(nodemask)) {
            cur_size_bytes = std::min(cur_size_bytes, __mapping_nodemask_max_mem_cap[nodemask]);
        }
        // set min value for nodemask
        __mapping_nodemask_max_mem_cap[nodemask]    = cur_size_bytes;
        // initialize used capacity with 0
        __mapping_nodemask_mem_used[nodemask]       = 0;
    }
}

void  init_mem_characteristics() {
    __mem_characteristics.num_cpu_nodes = -1;
    __mem_characteristics.num_mem_nodes = -1;
    __mem_characteristics.num_threads = -1;
    __mem_characteristics.matrix_lat_read_only = nullptr;
    __mem_characteristics.matrix_lat_read_write = nullptr;
    __mem_characteristics.matrix_bw_read_only = nullptr;
    __mem_characteristics.matrix_bw_read_write = nullptr;
}

int __determine_num_cpu_nodes(std::vector<std::string> &content) {
    std::string tmp;
    int num_cpu_nodes = 0;
    for(int i = 0; i < content.size(); i++) {
        tmp = content[i];
        if (tmp.find("=====") == 0) {
            num_cpu_nodes++;
        }
    }
    return num_cpu_nodes;
}

int __determine_num_memory_nodes(std::vector<std::string> &content) {
    std::string tmp, tmp2;
    std::vector<std::string> tmp_split;
    int num_memory_nodes = 0;
    for(int i = 0; i < content.size(); i++) {
        tmp = content[i];
        if (tmp.find("Thread") == 0) {
            string_split(tmp, tmp_split, ';');
            num_memory_nodes = tmp_split.size()-1;
            break;
        }
    }
    return num_memory_nodes;
}

int __determine_num_threads(std::vector<std::string> &content) {
    std::vector<std::string> tmp_split;
    // check last number of last entry
    string_split(content[content.size()-1], tmp_split, ';');
    int num_threads = std::atoi(tmp_split[0].c_str());
    return num_threads;
}

bool parse_mem_lookup(std::string path, bool bandwidth, bool read_only) {
    std::ifstream file(path);
    if (!file.is_open()) {
        return false;
    } else {
        std::vector<std::string> content, tmp_split;
        std::string line, tmp;

        // parse file
        while (std::getline(file, line)) {
            if (!trim(line).empty()) {
                content.push_back(line);
            }
        }
        file.close();

        int num_cpu_nodes = __mem_characteristics.num_cpu_nodes = __determine_num_cpu_nodes(content);
        int num_mem_nodes = __mem_characteristics.num_mem_nodes = __determine_num_memory_nodes(content);
        int num_threads   = __mem_characteristics.num_threads   = __determine_num_threads(content);

        // allocate memory
        double** matrix = (double**) FCN_MALLOC(sizeof(double*) * num_cpu_nodes);
        for(int i = 0; i < num_cpu_nodes; i++) {
            matrix[i] = (double*) FCN_MALLOC(sizeof(double) * num_threads * num_mem_nodes);
            memset(matrix[i], 0, sizeof(double) * num_threads * num_mem_nodes);
        }

        // set reference for matrix
        if(bandwidth && read_only) {
            __mem_characteristics.matrix_bw_read_only = matrix;
        } else if(bandwidth && !read_only) {
            __mem_characteristics.matrix_bw_read_write = matrix;
        } else if(!bandwidth && read_only) {
            __mem_characteristics.matrix_lat_read_only = matrix;
        } else if(!bandwidth && !read_only) {
            __mem_characteristics.matrix_lat_read_write = matrix;
        }

        // parse matrix
        int cur_cpu_node = -1;
        for(int i = 0; i < content.size(); i++) {
            tmp = content[i];
            if (tmp.find("=====") == 0) {
                cur_cpu_node++;
                continue;
            }
            if (tmp.find("Thread") == 0) {
                continue;
            }

            // parse line and get thread
            tmp_split.clear(); string_split(tmp, tmp_split, ';');
            int thr = std::atoi(tmp_split[0].c_str()) - 1;

            // assumption: node idx going from 0 to n-1
            for(int j = 1; j < tmp_split.size(); j++) {
                matrix[cur_cpu_node][thr * num_mem_nodes + (j-1)] = std::atof(tmp_split[j].c_str());
            }
        }
    }
    return true;
}

void parse_mem_characteristics() {
    if (H2M_LOOKUP_DIR.load()) {
        bool success;

        std::string path_lat_read_only(H2M_LOOKUP_DIR.load());
        path_lat_read_only.append("/lookup_lat_read-only.csv");
        success = parse_mem_lookup(path_lat_read_only, false, true);
        if (!success) {
            RELP("ERROR: Could not parse lookup_lat_read-only.csv\n");
        }

        std::string path_lat_read_write(H2M_LOOKUP_DIR.load());
        path_lat_read_write.append("/lookup_lat_read-write.csv");
        success = parse_mem_lookup(path_lat_read_write, false, false);
        if (!success) {
            RELP("ERROR: Could not parse lookup_lat_read-write.csv\n");
        }

        std::string path_bw_read_only(H2M_LOOKUP_DIR.load());
        path_bw_read_only.append("/lookup_bw_read-only.csv");
        success = parse_mem_lookup(path_bw_read_only, true, true);
        if (!success) {
            RELP("ERROR: Could not parse lookup_bw_read-only.csv\n");
        }

        std::string path_bw_read_write(H2M_LOOKUP_DIR.load());
        path_bw_read_write.append("/lookup_bw_read-write.csv");
        success = parse_mem_lookup(path_bw_read_write, true, false);
        if (!success) {
            RELP("ERROR: Could not parse lookup_bw_read-write.csv\n");
        }
    }
}

void print_mem_lookup(const double** matrix, std::string type, std::string mode) {
    if (matrix) {
        std::stringstream ss;
        ss << "\n" << "=== " << type << " Matrix (" << mode << "):\n";

        for(int c = 0; c < __mem_characteristics.num_cpu_nodes; c++) {
            // print header
            ss << "CPU-Node-" << c << "\n\t";
            for(int i = 0; i < __mem_characteristics.num_mem_nodes; i++) {
                ss << "Mem-Node-" << i << "\t";
            }
            ss << "\n";
            // matrix
            for(int i = 0; i < __mem_characteristics.num_threads; i++) {
                ss << "N_Thr=" << std::setfill('0') << std::setw(2) << i+1 << "\t";
                for(int j = 0; j < __mem_characteristics.num_mem_nodes; j++) {
                    ss << matrix[c][i * __mem_characteristics.num_mem_nodes + j] << "\t";
                }
                ss << "\n";
            }
            ss << "\n";
        }
        RELP("%s\n", ss.str().c_str());
    }
}

void print_mem_characteristics() {
    print_mem_lookup((const double**) __mem_characteristics.matrix_bw_read_only,   "Bandwidth", "read-only");
    print_mem_lookup((const double**) __mem_characteristics.matrix_bw_read_write,  "Bandwidth", "read-write");
    print_mem_lookup((const double**) __mem_characteristics.matrix_lat_read_only,  "Latency", "read-only");
    print_mem_lookup((const double**) __mem_characteristics.matrix_lat_read_write, "Latency", "read-write");
}

void cleanup_mem_matrix(double*** matrix) {
    if(*matrix) {
        for(int i = 0; i < __mem_characteristics.num_cpu_nodes; i++) {
            FCN_FREE((*matrix)[i]);
        }
        FCN_FREE(*matrix);
        *matrix = nullptr;
    }
}

void cleanup_mem_characteristics() {
    cleanup_mem_matrix(&__mem_characteristics.matrix_bw_read_only);
    cleanup_mem_matrix(&__mem_characteristics.matrix_bw_read_write);
    cleanup_mem_matrix(&__mem_characteristics.matrix_lat_read_only);
    cleanup_mem_matrix(&__mem_characteristics.matrix_lat_read_write);
}

h2m_alloc_info_t* get_alloc_info_ref(void *ptr, int *out_found) {
    DBP(__VLVL_ENTRY_EXIT, "get_alloc_info_ref (enter)\n");
    *out_found = 0;
    h2m_alloc_info_t* ret = nullptr;

    __mtx_book_keeping.rdlock();

    std::map<void*, h2m_alloc_info_t*>::iterator it = __book_keeping_allocations.find(ptr);
    if(it != __book_keeping_allocations.end()) {
        ret = it->second;
        *out_found = 1;
    }

    __mtx_book_keeping.unlock();

    DBP(__VLVL_ENTRY_EXIT, "get_alloc_info_ref (exit)\n");
    return ret;
}

h2m_alloc_info_t* get_alloc_info_ref_for_json_id(int json_id, int *out_found) {
    DBP(__VLVL_ENTRY_EXIT, "get_alloc_info_ref_for_json_id (json_id=%d) (enter)\n", json_id);
    *out_found = 0;
    h2m_alloc_info_t* ret = nullptr;

    __mtx_book_keeping.rdlock();

    std::map<void*, h2m_alloc_info_t*>::iterator it;
    for (it = __book_keeping_allocations.begin(); it != __book_keeping_allocations.end(); it++) {
        if(it->second->json_alloc_id == json_id) {
            ret = it->second;
            *out_found = 1;
            break;
        }
    }

    __mtx_book_keeping.unlock();

    DBP(__VLVL_ENTRY_EXIT, "get_alloc_info_ref_for_json_id (json_id=%d) (exit)\n", json_id);
    return ret;
}

int callback_iterate(struct dl_phdr_info *info, size_t size, void *data) {
    if (strstr(info->dlpi_name, "libc.so") != NULL) {
        // fprintf(stderr, "Name: \"%s\" (%d segments)\n", info->dlpi_name, info->dlpi_phnum);
        void* fcn = nullptr;
        void* obj = dlopen (info->dlpi_name, RTLD_LAZY);

        orig_malloc         = (fcn_malloc_t)            dlsym(obj , "malloc");
        orig_memalign       = (fcn_memalign_t)          dlsym(obj, "memalign");
        orig_pmemalign      = (fcn_pmemalign_t)         dlsym(obj, "posix_memalign");
        orig_aligned_alloc  = (fcn_aligned_alloc_t)     dlsym(obj, "aligned_alloc");
        orig_calloc         = (fcn_calloc_t)            dlsym(obj, "calloc");
        orig_realloc        = (fcn_realloc_t)           dlsym(obj, "realloc");
        orig_free           = (fcn_free_t)              dlsym(obj, "free");

        // === DEBUG
        // fprintf (stderr, "orig_malloc        found at addr %p\n", orig_malloc);
        // fprintf (stderr, "orig_memalign      found at addr %p\n", orig_memalign);
        // fprintf (stderr, "orig_pmemalign     found at addr %p\n", orig_pmemalign);
        // fprintf (stderr, "orig_aligned_alloc found at addr %p\n", orig_aligned_alloc);
        // fprintf (stderr, "orig_calloc        found at addr %p\n", orig_calloc);
        // fprintf (stderr, "orig_realloc       found at addr %p\n", orig_realloc);
        // fprintf (stderr, "orig_free          found at addr %p\n", orig_free);
        // === DEBUG

        // fcn = dlsym (obj, "aligned_alloc");
        // if (fcn) {
        //     fprintf (stderr, "Object %s has 'aligned_alloc' at addr %p\n", info->dlpi_name, fcn);
        //     orig_aligned_alloc  = (fcn_aligned_alloc_t) fcn;
        // }

        dlclose(obj);
    }
    return 0;
}

void __attribute__((constructor(101))) h2m_mem_profiler_constructor(void) {
    load_config_values();
    // RELP("__attribute__((constructor(101))) (enter)\n");
    dl_iterate_phdr (callback_iterate, NULL);
    // RELP("__attribute__((constructor(101))) (exit)\n");
}