#ifndef __H2M_STATISTICS_H__
#define __H2M_STATISTICS_H__

#include <atomic>
#include <mutex>
#include <string>
#include <omp.h>

#ifndef H2M_RECORD_STATS
#define H2M_RECORD_STATS 1
#endif

class MinMaxAvgStats {
    private:
    std::string stat_name;
    std::string unit_str;
    std::mutex mtx_priv;

    public:
    std::atomic<bool> has_values;
    std::atomic<double> val_sum;
    std::atomic<int> count;
    std::atomic<double> val_min;
    std::atomic<double> val_max;
    std::atomic<double> val_avg;

    MinMaxAvgStats(std::string name, std::string unit);
    void add_stat_value(double val);
    void reset();
    void print_stats(FILE *cur_file);
};


class h2m_stat_recorder_t {
    double t_elapsed;
    MinMaxAvgStats& stats_;

protected:
    double get_elapsed_time() { return omp_get_wtime() - t_elapsed; }
    void tick(bool force) { if (force) t_elapsed = omp_get_wtime(); }
    void tock(bool force) { if (force) t_elapsed = get_elapsed_time(); }
    void record(bool force) { if (force) stats_.add_stat_value(t_elapsed); }

public:
    h2m_stat_recorder_t(MinMaxAvgStats& stats) : stats_(stats)
    {
        tick();
    }
    ~h2m_stat_recorder_t()
    {
        // Automatically store the time elapsed
        // at the end of the variable  context/lifespan
        tock();
        record();
    }

#if H2M_RECORD_STATS
    void tick() { tick(true); }
    void tock() { tock(true); }
    void record() { record(true); }
#else
    void tick() { }
    void tock() { }
    void record() { }
#endif // H2M_RECORD_STATS
};

class h2m_explicit_stat_recorder_t : public h2m_stat_recorder_t {
public:
    h2m_explicit_stat_recorder_t(MinMaxAvgStats& stats) : h2m_stat_recorder_t(stats)
    {
        tick(true);
    }
    ~h2m_explicit_stat_recorder_t() = default;

    using h2m_stat_recorder_t::get_elapsed_time;
};

// Examples:

extern MinMaxAvgStats       __stats_time_init_finalize;
extern MinMaxAvgStats       __stats_time_allocate;
extern MinMaxAvgStats       __stats_time_declare_allocations;
extern MinMaxAvgStats       __stats_time_commit_allocations;
extern MinMaxAvgStats       __stats_time_get_callstack_info;
extern MinMaxAvgStats       __stats_time_get_traits_from_file;
extern MinMaxAvgStats       __stats_time_apply_migration_data_collection;
extern MinMaxAvgStats       __stats_time_apply_migration_alloc_data_collection;
extern MinMaxAvgStats       __stats_time_allocation_strategy;
extern MinMaxAvgStats       __stats_time_commit_strategy;
extern MinMaxAvgStats       __stats_time_migration_strategy;
extern MinMaxAvgStats       __stats_time_migration_inner;
extern MinMaxAvgStats       __stats_time_move_mbind;
extern MinMaxAvgStats       __stats_time_move_movepages;
extern MinMaxAvgStats       __stats_time_select_nodemask;
extern MinMaxAvgStats       __stats_time_wait;

#ifdef __cplusplus
extern "C" {
#endif

void h2m_stats_print();
void h2m_stats_print_w_mean(FILE *cur_file, const char* name, double sum, int count, bool cummulative);

#ifdef __cplusplus
}
#endif

void h2m_stats_print_w_mean(FILE *cur_file, std::string name, double sum, int count, bool cummulative = false);

#endif // __H2M_STATISTICS_H__
