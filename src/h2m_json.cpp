#include "h2m_json.h"
#include "h2m_common_internal.h"
#include "h2m_common.h"
#include "h2m_utils_numa.h"

#define JSON_RELP( ... ) \
    do { if (H2M_PRINT_JSON_ALLOCATION_WARNINGS.load()) { RELP( __VA_ARGS__ ); } } while(0)

#include <set>

// ========================================
// === Variables
// ========================================
std::atomic<bool>                       json_flag_available(false);
static std::mutex                       json_initializing;
std::map<int, JsonPhase*>               json_phases;
std::map<int, JsonAllocationGroup*>     json_allocation_groups;
std::map<int, JsonAllocation*>          json_allocations;

static int max_phase_id             = -100;

// ========================================
// === Constructors & Functions
// ========================================
JsonConfigMemorySpace::JsonConfigMemorySpace(const json& data)
: 
    idx_(json_get(data, "idx", -1)),
    name_(json_get(data, "name", " ")),
    numa_domain_(json_get(data, "NUMA domains",std::vector<int>())),
    is_mem_space_low_lat_(json_get(data, "is_mem_space_low_lat", false)),
    is_mem_space_hbw_(json_get(data, "is_mem_space_hbw", false)),
    is_mem_space_large_cap_(json_get(data, "is_mem_space_large_cap", false)),
    capacity_MB_(json_get(data, "capacity_MB", 0.0)),
    scaling_threads_(json_get(data, "scaling_threads", std::vector<int>())),
    scaling_bandwidth_read_only_MBs_(json_get(data, "scaling_bandwidth_read_only_MBs", std::vector<double>())),
    scaling_bandwidth_read_write_MBs_(json_get(data, "scaling_bandwidth_read_write_MBs", std::vector<double>())),
    scaling_latency_read_only_ns_(json_get(data, "scaling_latency_read_only_ns", std::vector<double>())),
    scaling_latency_read_only_bandwidth_disturbance_MBs_(json_get(data, "scaling_latency_read_only_bandwidth_disturbance_MBs", std::vector<double>())),
    scaling_latency_read_write_ns_(json_get(data, "scaling_latency_read_write_ns", std::vector<double>())),
    scaling_latency_read_write_bandwidth_disturbance_MBs_(json_get(data, "scaling_latency_read_write_bandwidth_disturbance_MBs", std::vector<double>()))
{ }

void JsonConfigMemorySpace::Print() const {
    std::cout << " Memory space information: " << std::endl;
    std::cout << " Memory device is: " << name_ << std::endl;
}

JsonConfigCopyBandwidth::JsonConfigCopyBandwidth(const json& data)
:
    idx_from_(json_get(data, "idx_from", -1)),
    idx_to_(json_get(data, "idx_to", -1)),
    size_bytes_(json_get(data, "size_bytes", std::vector<int>())),
    bandwidth_MBs_(json_get(data, "bandwidth_MBs", std::vector<double>()))
{ }

JsonAllocationGroup::JsonAllocationGroup(const json& data) {
    ctr                 = 0;
    id                  = json_get(data, "id", -1);
    callsite_group_id   = json_get(data, "callsite_group_id", -1);
    callstack_group_id  = json_get(data, "callstack_group_id", -1);
    callstack_rip       = json_get(data, "callstack_rip", "");
    callstack_offsets   = json_get(data, "callstack_offsets", "");
    callstack_offsets_clean = get_clean_callsite_offsets(callstack_offsets);
    callsite_rip        = json_get(data, "callsite_rip", "");
    callsite_str        = json_get(data, "callsite_str", "");
    sum_size_mb         = json_get(data, "size_mb", 0.0);
    // min_alloc_date      = json_get(data, "min_alloc_date", 0.0);
    // max_dealloc_date    = json_get(data, "max_dealloc_date", 0.0);
}

JsonAllocation::JsonAllocation(const json& data) {
    id                  = json_get(data, "id", -1);
    group_id            = json_get(data, "aggregation_id", -1);
    size_mb             = json_get(data, "size_mb", 0.0);
    date_alloc_ms       = json_get(data, "date_alloc_ms", 0.0);
    date_dealloc_ms     = json_get(data, "date_dealloc_ms", 0.0);
}

JsonTraitEntry::JsonTraitEntry(const json& data) {
    allocation_id   = json_get(data, "allocation_id", -1);
    prefetch        = json_get(data, "prefetch", 0) == 1;

    traits.clear();
    if(data.contains("traits")) {
        for (auto& element : data["traits"]) {
            h2m_alloc_trait_t t;
            std::string key     = json_get(element, "key", "");
            std::string value   = json_get(element, "value", "");

            // TODO: implement proper mapping for keys and values as C++ does not have an eval function
            if (key == "h2m_atk_req_mem_space") {
                t.key = h2m_atk_pref_mem_space; // for now keep it like this to avoid issues
            } else if (key == "h2m_atk_pref_mem_space") {
                t.key = h2m_atk_pref_mem_space;
            }
            
            if (value == "h2m_atv_mem_space_low_lat") {
                t.value = { .atv = h2m_atv_mem_space_low_lat};
                desired_mem_space = h2m_atv_mem_space_low_lat;
            } else if (value == "h2m_atv_mem_space_hbw") {
                t.value = { .atv = h2m_atv_mem_space_hbw};
                desired_mem_space = h2m_atv_mem_space_hbw;
            } else if (value == "h2m_atv_mem_space_large_cap") {
                t.value = { .atv = h2m_atv_mem_space_large_cap};
                desired_mem_space = h2m_atv_mem_space_large_cap;
            }
            traits.push_back(t);
        }
    }
}

JsonPhase::JsonPhase(const json& data) {
    id              = json_get(data, "id", -1);
    name            = json_get(data, "name", "Complete Program");
    ts_ms_start     = json_get(data, "ts_ms_start", 0.0);
    ts_ms_end       = json_get(data, "ts_ms_end", 0.0);
    duration_ms     = json_get(data, "duration_ms", 0.0);
    id_trans_start  = json_get(data, "id_trans_start", -1);
    id_trans_end    = json_get(data, "id_trans_end", -1);
    traits.clear();
}

int parse_json_config(const char* file_path,
    std::vector<JsonConfigMemorySpace*>& memory_space,
    std::vector<JsonConfigCopyBandwidth*>& copy_bandwidth) {

    std::ifstream config_file(file_path);
    if(!config_file.good()) {
        std::cout << "ERROR, cannot open config.json file " << std::endl;
        std::cout << "Check path, currently here: " << std::endl;
        std::cout << file_path << std::endl;
        return 0;
    }
   
    // read json file
    const json data = json::parse(config_file);

    // parse aggregations/groups
    if(data.contains("memory_spaces")) {
        for (auto& element : data["memory_spaces"]) {
            JsonConfigMemorySpace* ele = new JsonConfigMemorySpace(element);
            memory_space.push_back(ele);
        }
    }

    // parse aggregations/groups
    if(data.contains("copy_bandwidth")) {
        for (auto& element : data["copy_bandwidth"]) {
        JsonConfigCopyBandwidth* ele = new JsonConfigCopyBandwidth(element);
            copy_bandwidth.push_back(ele);
        }
    }
 return 0;
}

void parse_json_traits(const char* file_path) {
    if (json_flag_available.load()) {
        // already loaded
        return;
    }

    json_initializing.lock();
    if (json_flag_available.load()) {
        // need to recheck
        json_initializing.unlock();
        return;
    }

    std::ifstream trait_file(file_path);
    if(!trait_file.good()) {
        JSON_RELP("WARNING: JSON trait file %s not existing or accessible!\n", file_path);
        return;
    }

    // cleanup first
    json_allocation_groups.clear();
    json_allocations.clear();    
    json_phases.clear();

    // read json file
    json data = json::parse(trait_file);

    // parse aggregations/groups
    if(data.contains("aggregations")) {
        for (auto& element : data["aggregations"]) {
            JsonAllocationGroup* grp = new JsonAllocationGroup(element);
            json_allocation_groups[grp->id] = grp;
        }
    }

    // parse aggregations/groups
    if(data.contains("allocations")) {
        for (auto& element : data["allocations"]) {
            JsonAllocation* alloc = new JsonAllocation(element);
            json_allocations[alloc->id] = alloc;

            // assign it to group
            JsonAllocationGroup* grp = json_allocation_groups[alloc->group_id];
            grp->allocations.push_back(alloc);
        }
    }

    // sort allocation lists by allocation order
    for (auto const& x : json_allocation_groups) {
        std::sort(x.second->allocations.begin(), x.second->allocations.end(),
            [](JsonAllocation* const& a, JsonAllocation* const& b) {
                return a->date_alloc_ms < b->date_alloc_ms;
            }
        );
    }

    // parse phases
    if(data.contains("phases")) {
        for (auto& element : data["phases"]) {
            JsonPhase* ph = new JsonPhase(element);
            json_phases[ph->id] = ph;

            // identify max phase id
            if (ph->id > max_phase_id) {
                max_phase_id = ph->id;
            }
        }
    }

    // parse traits
    if(data.contains("phase_traits")) {
        for (auto& ele_ph : data["phase_traits"]) {
            // first get phase
            int ph_id = json_get(ele_ph, "phase_id", -1);
            if(json_phases.count(ph_id)) {
                JsonPhase* ph = json_phases.find(ph_id)->second;

                if(ele_ph.contains("allocation_traits")) {
                    for (auto& element : ele_ph["allocation_traits"]) {
                        JsonTraitEntry te(element);
                        ph->traits.push_back(te);
                    }
                }
            }
        }
    }

    // === DEBUG
    // printf("\nDEBUG -> Allocation Groups and Allocations:\n");
    // for (auto const& x : json_allocation_groups) {
    //     JsonAllocationGroup* g = x.second;
    //     printf("-- id=%d, callsite_group_id=%d, callstack_group_id=%d, callsite=%s, callstack=%s, n_allocations=%zu\n",
    //         g->id, g->callsite_group_id, g->callstack_group_id, g->callsite_rip.c_str(), g->callstack_rip.c_str(), g->allocations.size());
        
    //     for (auto const& a : g->allocations) {
    //         printf("\t-- id=%d, size_mb=%f, date_alloc=%f, date_dealloc=%f\n",
    //             a->id, a->size_mb, a->date_alloc_ms, a->date_dealloc_ms);
    //     }
    // }
    // printf("\nDEBUG -> Phases:\n");
    // for (auto const& x : json_phases) {
    //     JsonPhase* p = x.second;
    //     printf("-- id=%d, name=%s, start=%f, end=%f, duration_ms=%f\n",
    //         p->id, p->name.c_str(), p->ts_ms_start, p->ts_ms_end, p->duration_ms);

    //     for(auto const& y : p->traits) {
    //         char buf_val[255];
    //         h2m_alloc_trait_value_t_str(y.traits[0].value.atv, buf_val);
    //         printf("\t-- alloc_id=%d, space=%s\n", y.allocation_id, buf_val);
    //     }
    // }
    // === DEBUG

    // set flag telling that json data is available
    json_flag_available = true;
    json_initializing.unlock();
}

void cleanup_json_traits() {
    for (auto const& x : json_allocations) {
        JsonAllocation* y = x.second;
        delete y;
    }
    for (auto const& x : json_allocation_groups) {
        JsonAllocationGroup* y = x.second;
        delete y;
    }
    for (auto const& x : json_phases) {
        JsonPhase* y = x.second;
        delete y;
    }
    json_allocations.clear();
    json_allocation_groups.clear();
    json_phases.clear();
}

void cleanup_json_config(std::vector<JsonConfigMemorySpace*>& memory_space,
    std::vector<JsonConfigCopyBandwidth*>& copy_bandwidth) {
    // delete vector of memory space
    for (auto& x : memory_space)
    {
        delete x;
    }
    // delete class content
    for (auto& x : copy_bandwidth)
    {
        delete x;
    }
}

JsonTraitEntry* __get_trait_entry_for_phase_and_allocation_id(JsonPhase* ph, int alloc_id, bool *found) {
    *found = false;
    // next try to find the trait entry
    std::vector<JsonTraitEntry>::iterator it_trait = find_if(ph->traits.begin(), ph->traits.end(), [&alloc_id](const JsonTraitEntry& obj) {return obj.allocation_id == alloc_id;});
    if (it_trait == ph->traits.end()) {
        return nullptr;
    }
    *found = true;
    return &(*it_trait);
}

h2m_alloc_trait_t* get_json_traits_for_allocation(int phase_id, int callsite_id, std::string callstack_offsets_clean, bool use_id, int* json_alloc_id, int* n_traits, bool* prefetch) {
    // default returns
    *json_alloc_id = -1;
    *n_traits = 0;
    *prefetch = false;

    if (!json_flag_available.load()) {
        JSON_RELP("WARNING: JSON trait file has not been loaded. Falling back to no or default traits!\n");
        return nullptr;
    }

    // determine phase to use
    int cur_ph_id = phase_id;
    if(!json_phases.count(cur_ph_id)) {
        if(max_phase_id == -1 && json_phases.count(-1)) {
            // special case: JSON contains traits for the complete program
            cur_ph_id = max_phase_id;
        } else {
            JSON_RELP("WARNING: Phase %d not found in JSON trait file. Falling back to no or default traits!\n", cur_ph_id);
            return nullptr;
        }
    }

    JsonAllocationGroup* grp = nullptr;
    if(use_id) {
        // next find allocation id based on group id
        if(!json_allocation_groups.count(callsite_id)) {
            JSON_RELP("WARNING: Allocation callsite with ID %d not found in JSON trait file. Falling back to no or default traits!\n", callsite_id);
            return nullptr;
        }
        grp = json_allocation_groups.find(callsite_id)->second;
    } else {
        for (auto const& ent : json_allocation_groups) {
            if(ent.second->callstack_offsets_clean == callstack_offsets_clean) {
                grp = ent.second;
            }
        }
        if(!grp) {
            JSON_RELP("WARNING: Allocation group for callstack %s not found in JSON trait file. Falling back to no or default traits!\n", callstack_offsets_clean.c_str());
            return nullptr;
        }
    }

    // determine allocation id based on counter
    grp->mtx.lock();
    int alloc_id = grp->allocations[grp->ctr]->id;
    // increase counter for next allocation if possible
    if(grp->ctr < grp->allocations.size()-1) {
        grp->ctr++;
    }
    grp->mtx.unlock();

    // return allocation id independent of whether traits are specified or not for it
    *json_alloc_id = alloc_id;

    JsonTraitEntry* te;
    bool found;
    JsonPhase* ph = json_phases.find(cur_ph_id)->second;
    te = __get_trait_entry_for_phase_and_allocation_id(ph, alloc_id, &found);

    if(!found) {
        if(H2M_RELAXED_JSON_SEARCH_ENABLED.load()) {
            JSON_RELP("WARNING: Allocation with id %d not found in phase %d within JSON trait file. Checking upcoming phases!\n", alloc_id, cur_ph_id);
            // try to find the first entry of the allocation id in the upcoming phases
            for(int nid = cur_ph_id+1; nid <= max_phase_id; nid++) {
                if(json_phases.count(nid)) {
                    JsonPhase* ph = json_phases.find(nid)->second;
                    te = __get_trait_entry_for_phase_and_allocation_id(ph, alloc_id, &found);
                    if(found) {
                        RELP("INFO: Found allocation traits for id %d in phase %d within JSON trait file.\n", alloc_id, nid);
                        // return traits
                        *n_traits = te->traits.size();
                        *prefetch = te->prefetch;
                        h2m_alloc_trait_t* ret = &(te->traits[0]);
                        return ret;
                    }
                }
            }
            JSON_RELP("WARNING: Allocation with id %d not found in any phase within JSON trait file. Falling back to no or default traits!\n", alloc_id);
            return nullptr;
        } else {
            JSON_RELP("WARNING: Allocation with id %d not found in phase %d within JSON trait file. Falling back to no or default traits!\n", alloc_id, cur_ph_id);
            return nullptr;
        }
    }
    
    // return traits
    *n_traits = te->traits.size();
    *prefetch = te->prefetch;
    h2m_alloc_trait_t* ret = &(te->traits[0]);
    return ret;

}
