#ifndef __H2M_TOOLS_INTERNAL_H__
#define __H2M_TOOLS_INTERNAL_H__

#include "h2m_common.h"
#include "h2m_tools.h"

typedef struct h2m_tool_callbacks_active_t {
    unsigned int enabled : 1;
    
    // list of callback pointers
    h2m_tool_callback_thread_init_t                 callback_thread_init            = nullptr;
    h2m_tool_callback_post_init_serial_t            callback_post_init_serial       = nullptr;
    h2m_tool_callback_thread_finalize_t             callback_thread_finalize        = nullptr;
    h2m_tool_callback_verify_traits_t               callback_verify_traits          = nullptr;
    h2m_tool_callback_declare_alloc_t               callback_declare_alloc          = nullptr;
    h2m_tool_callback_commit_allocs_t               callback_commit_allocs          = nullptr;
    h2m_tool_callback_alloc_t                       callback_alloc                  = nullptr;
    h2m_tool_callback_alloc_on_nodes_t              callback_alloc_on_nodes         = nullptr;
    h2m_tool_callback_free_t                        callback_free                   = nullptr;
    h2m_tool_callback_update_traits_t               callback_update_traits          = nullptr;
    h2m_tool_callback_apply_migration_t             callback_apply_migration        = nullptr;
    h2m_tool_callback_apply_migration_async_t       callback_apply_migration_async  = nullptr;
    h2m_tool_callback_apply_migration_alloc_t       callback_apply_migration_alloc  = nullptr;
    h2m_tool_callback_apply_migration_alloc_async_t callback_apply_migration_alloc_async = nullptr;
    h2m_tool_callback_wait_t                        callback_wait                   = nullptr;
    h2m_tool_callback_allocation_strategy_t         callback_allocation_strategy    = nullptr;
    h2m_tool_callback_commit_strategy_t             callback_commit_strategy        = nullptr;
    h2m_tool_callback_migration_strategy_t          callback_migration_strategy     = nullptr;
    h2m_tool_callback_move_memory_mbind_t           callback_move_memory_mbind      = nullptr;
    h2m_tool_callback_move_memory_movepages_t       callback_move_memory_movepages  = nullptr;

} h2m_tool_callbacks_active_t;

extern h2m_tool_callbacks_active_t tool_status;

#ifdef __cplusplus
extern "C" {
#endif

void    h2m_tool_init(void);
void    h2m_tool_fini(void);

h2m_tool_data_t* h2m_tool_get_thread_data(void);

#ifdef __cplusplus
};
#endif

#endif // __H2M_TOOLS_INTERNAL_H__
