module h2m_lib
    use iso_c_binding
    implicit none

    interface
        function h2m_init() bind(c)
            implicit none
            integer :: h2m_init
        end function h2m_init

        function h2m_thread_init() bind(c)
            implicit none
            integer :: h2m_thread_init
        end function h2m_thread_init

        function h2m_thread_finalize() bind(c)
            implicit none
            integer :: h2m_thread_finalize
        end function h2m_thread_finalize

        function h2m_finalize() bind(c)
            implicit none
            integer :: h2m_finalize
        end function h2m_finalize

        ! int h2m_phase_transition(const char* name);
        function h2m_phase_transition(name) bind(c,name='h2m_phase_transition')
            use iso_c_binding
            implicit none
            character (kind=c_char, len=1) :: name
            integer :: h2m_phase_transition
        end function h2m_phase_transition
    end interface
end module
