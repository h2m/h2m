#include "h2m_common_internal.h"
#include "h2m_common.h"
#include "h2m_config.h"
#include "h2m_tools.h"
#include "h2m_tools_internal.h"
#include "h2m_version.h"

//================================================================================
// Forward declarations
//================================================================================

h2m_tool_interface_fn_t h2m_tool_fn_lookup(const char *s);
h2m_tool_set_result_t h2m_tool_set_callback(h2m_tool_callback_types_t which, h2m_tool_callback_t callback);
int h2m_tool_get_callback(h2m_tool_callback_types_t which, h2m_tool_callback_t *callback);
typedef h2m_tool_start_result_t *(*h2m_tool_start_t)(const char *);

//================================================================================
// Variables
//================================================================================

// flag that tells whether tool part has already been initialized
std::mutex          __mtx_tool_initialized;
std::atomic<bool>   __tool_initialized(false);

h2m_tool_callbacks_active_t tool_status;
h2m_tool_start_result_t *h2m_tool_start_result = NULL;

//================================================================================
// Functions
//================================================================================

h2m_tool_start_result_t* h2m_tool_start(const char* version) {
    h2m_tool_start_result_t *ret = NULL;
    // Search next symbol in the current address space. This can happen if the
    // runtime library is linked before the tool. Since glibc 2.2 strong symbols
    // don't override weak symbols that have been found before unless the user
    // sets the environment variable LD_DYNAMIC_WEAK.
    h2m_tool_start_t next_tool = (h2m_tool_start_t)dlsym(RTLD_NEXT, "h2m_tool_start");
    if (next_tool) {
        ret = next_tool(version);
    }
    return ret;
}

static h2m_tool_start_result_t * h2m_tool_try_start(const char* version) {
    h2m_tool_start_result_t *ret    = NULL;
    h2m_tool_start_t start_tool     = NULL;
    const char sep                  = ':';

    ret = h2m_tool_start(version);
    if (ret) {
        return ret;
    }

    // Try tool-libraries-var ICV
    const char *tool_libs = getenv("H2M_TOOL_LIBRARIES");
    if (tool_libs) {
        RELP("H2M_TOOL_LIBRARIES = %s\n", tool_libs);
        std::string str(tool_libs);
        std::vector<std::string> libs;
        string_split(str, libs, sep);

        for (auto &cur_lib : libs) {
            void *h = dlopen(cur_lib.c_str(), RTLD_LAZY);
            if (h) {
                start_tool = (h2m_tool_start_t)dlsym(h, "h2m_tool_start");
                if (start_tool && (ret = (*start_tool)(version))) {
                    break;
                } else {
                    dlclose(h);
                }
            }
        }
    }
    return ret;
}

void h2m_tool_init() {
    if(__tool_initialized) {
        return;
    }
    
    __mtx_tool_initialized.lock();
    // need to check again
    if(__tool_initialized) {
        __mtx_tool_initialized.unlock();
        return;
    }

    // default: try to initilize tool, but possibility to disable it
    tool_status.enabled = 1;

    const char *h2m_tool_env_var = getenv("H2M_TOOL_ENABLE");
    if (!h2m_tool_env_var || !strcmp(h2m_tool_env_var, ""))
        tool_status.enabled = 1; // default: tool support enabled
    else if (!strcmp(h2m_tool_env_var, "disabled"))
        tool_status.enabled = 0;
    else if (!strcmp(h2m_tool_env_var, "0"))
        tool_status.enabled = 0;

    DBP(__VLVL_TOOL, "h2m_tool_init: H2M_TOOL_ENABLE = %s\n", h2m_tool_env_var);
    
    if(tool_status.enabled)
    {
        // try to load tool
        h2m_tool_start_result = h2m_tool_try_start(H2M_VERSION_STRING);
        if (h2m_tool_start_result) {
            tool_status.enabled = !!h2m_tool_start_result->initialize(h2m_tool_fn_lookup, &(h2m_tool_start_result->tool_data));
            if (!tool_status.enabled) {
                return;
            }
        } else {
            tool_status.enabled = 0;
        }
    }
    
    DBP(__VLVL_TOOL, "h2m_tool_init: h2m_tool_status.enabled = %d\n", tool_status.enabled);

    __tool_initialized = 1;
    __mtx_tool_initialized.unlock();
}

void h2m_tool_fini() {
    if (tool_status.enabled && h2m_tool_start_result) {
        DBP(__VLVL_TOOL, "h2m_tool_fini: enter\n");
        h2m_tool_start_result->finalize(&(h2m_tool_start_result->tool_data));
    }
}

//================================================================================
// Callbacks
//================================================================================

h2m_tool_interface_fn_t h2m_tool_fn_lookup(const char *s) {
    if(!strcmp(s, "h2m_tool_set_callback"))
        return (h2m_tool_interface_fn_t)h2m_tool_set_callback;
    else if(!strcmp(s, "h2m_tool_get_callback"))
        return (h2m_tool_interface_fn_t)h2m_tool_get_callback;
    else if(!strcmp(s, "h2m_tool_get_thread_data"))
        return (h2m_tool_interface_fn_t)h2m_tool_get_thread_data;
    else if(!strcmp(s, "h2m_tool_get_capacity_usage_info"))
        return (h2m_tool_interface_fn_t)h2m_get_capacity_usage_info;
    else if(!strcmp(s, "h2m_tool_get_mem_characteristics"))
        return (h2m_tool_interface_fn_t)h2m_get_mem_characteristics;
    else
        fprintf(stderr, "ERROR: function lookup for name %s not possible.\n", s);
    return (h2m_tool_interface_fn_t)0;
}

h2m_tool_set_result_t h2m_tool_set_callback(h2m_tool_callback_types_t which, h2m_tool_callback_t callback) {
    int tmp_val = (int) which;
    // printf("Setting callback %d\n", tmp_val);
    switch(tmp_val) {
        case h2m_callback_thread_init:
            tool_status.callback_thread_init = (h2m_tool_callback_thread_init_t)callback;
            break;
        case h2m_callback_post_init_serial:
            tool_status.callback_post_init_serial = (h2m_tool_callback_post_init_serial_t)callback;
            break;
        case h2m_callback_thread_finalize:
            tool_status.callback_thread_finalize = (h2m_tool_callback_thread_finalize_t)callback;
            break;
        case h2m_callback_verify_traits:
            tool_status.callback_verify_traits = (h2m_tool_callback_verify_traits_t)callback;
            break;
        case h2m_callback_declare_alloc:
            tool_status.callback_declare_alloc = (h2m_tool_callback_declare_alloc_t)callback;
            break;
        case h2m_callback_commit_allocs:
            tool_status.callback_commit_allocs = (h2m_tool_callback_commit_allocs_t)callback;
            break;
        case h2m_callback_alloc:
            tool_status.callback_alloc = (h2m_tool_callback_alloc_t)callback;
            break;
        case h2m_callback_alloc_on_nodes:
            tool_status.callback_alloc_on_nodes = (h2m_tool_callback_alloc_on_nodes_t)callback;
            break;
        case h2m_callback_free:
            tool_status.callback_free = (h2m_tool_callback_free_t)callback;
            break;
        case h2m_callback_update_traits:
            tool_status.callback_update_traits = (h2m_tool_callback_update_traits_t)callback;
            break;
        case h2m_callback_apply_migration:
            tool_status.callback_apply_migration = (h2m_tool_callback_apply_migration_t)callback;
            break;
        case h2m_callback_apply_migration_async:
            tool_status.callback_apply_migration_async = (h2m_tool_callback_apply_migration_async_t)callback;
            break;
        case h2m_callback_apply_migration_alloc:
            tool_status.callback_apply_migration_alloc = (h2m_tool_callback_apply_migration_alloc_t)callback;
            break;
        case h2m_callback_apply_migration_alloc_async:
            tool_status.callback_apply_migration_alloc_async = (h2m_tool_callback_apply_migration_alloc_async_t)callback;
            break;
        case h2m_callback_wait:
            tool_status.callback_wait = (h2m_tool_callback_wait_t)callback;
            break;
        case h2m_callback_allocation_strategy:
            tool_status.callback_allocation_strategy = (h2m_tool_callback_allocation_strategy_t)callback;
            break;
        case h2m_callback_commit_strategy:
            tool_status.callback_commit_strategy = (h2m_tool_callback_commit_strategy_t)callback;
            break;
        case h2m_callback_migration_strategy:
            tool_status.callback_migration_strategy = (h2m_tool_callback_migration_strategy_t)callback;
            break;
        case h2m_callback_move_memory_mbind:
            tool_status.callback_move_memory_mbind = (h2m_tool_callback_move_memory_mbind_t)callback;
            break;
        case h2m_callback_move_memory_movepages:
            tool_status.callback_move_memory_movepages = (h2m_tool_callback_move_memory_movepages_t)callback;
            break;
        default:
            fprintf(stderr, "ERROR: Unable to set callback for specifier %d\n", which);
            return h2m_tool_set_error;
    }
    return h2m_tool_set_always;
}

int h2m_tool_get_callback(h2m_tool_callback_types_t which, h2m_tool_callback_t *callback) {
    // can be implemented, but currently I dont see any reason/use case for that except unit testing
    return H2M_NOT_IMPLEMENTED;
}

h2m_tool_data_t* h2m_tool_get_thread_data(void) {
    #if TOOL_SUPPORT
    int cur_gtid = h2m_get_gtid();
    return &(__thread_data[cur_gtid].thread_tool_data);
    #else
    return nullptr;
    #endif // TOOL_SUPPORT
}
