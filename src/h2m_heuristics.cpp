#include "h2m_heuristics.h"
#include "h2m.h"
#include "h2m_common_internal.h"
#include "h2m_common.h"
#include "h2m_config.h"

h2m_placement_decision_t h2m_default_allocation_strategy(size_t size, int n_traits, const h2m_alloc_trait_t traits[], int *out_err) {
    // get default values and respect prescriptive traits
    h2m_placement_decision_t pl = h2m_get_default_placement();
    pl = h2m_apply_prescriptive_traits(pl, n_traits, traits);

    *out_err = H2M_SUCCESS;
    return pl;
}

h2m_alloc_order_entry_t* h2m_default_commit_strategy(const h2m_declaration_t handles[], size_t n_handles, int *out_err) {
    // default: use FIFO strategy for ordering allocations
    h2m_alloc_order_entry_t* ret = (h2m_alloc_order_entry_t*) FCN_MALLOC(sizeof(h2m_alloc_order_entry_t)*n_handles);
    for(int i = 0; i < n_handles; i++) {
        h2m_declaration_t h = handles[i];
        ret[i].decl = h;
        ret[i].pl   = h2m_call_allocation_strategy(h->size, h->n_traits, h->traits, out_err);
        if(*out_err != H2M_SUCCESS) {
            return ret;
        }
    }
    *out_err = H2M_SUCCESS;
    return ret;
}

h2m_migration_decision_t* h2m_default_migration_strategy(int n_ele, const h2m_alloc_info_t *elements, int* out_n_decisions) {
    
    if(n_ele <= 0) {
        *out_n_decisions = 0;
        return nullptr;
    }

    // allocate memory
    h2m_migration_decision_t *decisions = (h2m_migration_decision_t *) FCN_MALLOC(sizeof(h2m_migration_decision_t) * n_ele);

    // get current resource usage and limits (your algorithm might need to consider that)
    // h2m_capacity_usage_info_t cap_usage = h2m_get_capacity_usage_info();

    // counter used to identify how many migration decision are returned
    int ctr = 0;

    for(int i = 0; i < n_ele; i++) {
        // determine placement decision based on allocation strategy
        int err;
        h2m_placement_decision_t pl = h2m_call_allocation_strategy(elements[i].size, elements[i].n_traits, elements[i].traits, &err);
        // skip if some error occurred or data is missing
        if(err != H2M_SUCCESS) {
            continue;
        }

        // === Don't do that explicitly anymore but handle that via allocation strategy
        // === Allocation strategy also considers prescriptive traits
        // int mem_traits_found = 0;        
        // h2m_placement_decision_t pl = h2m_get_default_placement();
        // for (int k = 0; k < elements[i].n_traits; k++) {
        //     if (elements[i].traits[k].key == h2m_atk_req_mem_space || elements[i].traits[k].key == h2m_atk_pref_mem_space) {
        //         pl.mem_space        = elements[i].traits[k].value.atv;
        //         pl.mem_prio         = elements[i].traits[k].key;
        //         mem_traits_found    = 1;
        //         continue;
        //     }
        //     if (elements[i].traits[k].key == h2m_atk_mem_partition) {
        //         pl.mem_partition    = elements[i].traits[k].value.atv;
        //         if (!mem_traits_found) {
        //             pl.mem_space    = elements[i].cur_mem_space;
        //         }
        //         mem_traits_found    = 1;
        //         continue;
        //     }
        // }

        // check whether items alread reside in the desired memory space (more specifically mask)
        unsigned long mask_current  = h2m_get_nodemask_for_mem_space(elements[i].cur_mem_space);
        unsigned long mask_target   = h2m_get_nodemask_for_mem_space(pl.mem_space);

        // TODO: handle difference in memory partitioning later
        if (mask_current != mask_target) {

            #if defined(H2M_DEBUG)
            if (H2M_VERBOSITY_LVL.load() >= __VLVL_MIGRATION_DECISIONS) {
                char str_space[64];
                char str_part[64];
                h2m_alloc_trait_value_t_str(pl.mem_space, str_space);
                h2m_alloc_trait_value_t_str(pl.mem_partition, str_part);
                DBP(__VLVL_MIGRATION_DECISIONS, "Decision [ctr=%d] to move ptr=" DPxMOD " to space=%s with partition=%s\n", ctr, DPxPTR(elements[i].ptr), str_space, str_part);
            }
            #endif // H2M_DEBUG

            decisions[ctr].alloc_info   = elements[i];
            decisions[ctr].placement    = pl;
            ctr++;
        }
    }
    
    *out_n_decisions = ctr;
    if(ctr) {
        return decisions;
    } else {
        FCN_FREE(decisions);
        return nullptr;
    }
}
