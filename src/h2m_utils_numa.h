#ifndef __H2M_UTILS_NUMA__
#define __H2M_UTILS_NUMA__

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <cerrno>
#include <cstdlib>
#include <cstdio>
#include <malloc.h>
#include <map>
#include <numa.h>
#include <numaif.h>
#include <sched.h>
#include <stdint.h>
#include <stdio.h>
#include <string>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <vector>

#include "h2m.h"

#ifndef H2M_ENABLE_ALLOC_ERR_OUTPUT
#define H2M_ENABLE_ALLOC_ERR_OUTPUT 1
#endif

#define gettid() ((pid_t)syscall(SYS_gettid))

void* alloc(size_t size, size_t alignment, int *err);
void* alloc_on_nodes(size_t size, h2m_placement_decision_t pl, unsigned long nodemask, int *err);
int move_memory_mbind(void* ptr, size_t size, h2m_placement_decision_t *pl, unsigned long nodemask);
int move_memory_movepages(int num_pages, void** pages, int *nodes);
void check_page_nodes(void* ptr, size_t size, unsigned long nodemask, int* correct_pages, int* wrong_pages, int output);
void print_numa_capacities();
std::map<int, size_t> get_numa_capacities();
int verify_page_locations(void** pointers, size_t num_ptrs, size_t size_per_pointer, unsigned long nodemask, const char* str, int output);
std::map<int, size_t> get_self_numa_memory_usage();
void* get_page_start_for_address(void* ptr);

void  print_affinity_mask(cpu_set_t mask);
void  get_and_print_affinity_mask();
cpu_set_t get_cpuset_for_bitmask(struct bitmask * mask);
int get_node_from_nodemask(unsigned long nodemask);

void** get_caller_rip(int depth, int* size_callstack, void** caller_rip);
std::string get_callstack_rip_str(int additional_idx_start);
std::string get_callstack_offsets(int additional_idx_start);
std::string get_clean_callsite_offsets(std::string callstack_offsets);

#endif // __H2M_UTILS_NUMA__