// data migration strategy header
#ifndef __H2M_HEURISTICS_H__
#define __H2M_HEURISTICS_H__

#include "h2m.h"
#include "h2m_common.h"
#include "h2m_statistics.h"
#include "h2m_tools.h"
#include "h2m_tools_internal.h"
#include "h2m_utils_numa.h"
#include "h2m_version.h"

h2m_placement_decision_t  h2m_default_allocation_strategy(size_t size, int n_traits, const h2m_alloc_trait_t traits[], int *out_err);
h2m_alloc_order_entry_t*        h2m_default_commit_strategy(const h2m_declaration_t handles[], size_t n_handles, int *out_err);
h2m_migration_decision_t* h2m_default_migration_strategy(int n_ele, const h2m_alloc_info_t *elements, int* out_n_decisions);

#endif // __H2M_HEURISTICS_H__
