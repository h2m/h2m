project(h2m_src)

# ========================================
# === Build options
# ========================================
option(H2M_ENABLE_DEBUG_OUTPUT "Enable detailed debug outputs" off)
option(H2M_RECORD_STATS "Record internal statistics" on)
option(H2M_TRACE_ITAC "Enable tracing with Intel Trace Analyzer " off)
option(H2M_TOOL_SUPPORT "Enable tool support" on)
set(H2M_MAX_NUMA_NODES 8 CACHE STRING "Maximum number of supported NUMA nodes")
option(H2M_ENABLE_ALLOC_ERR_OUTPUT "Enable allocation error output" on)

# ========================================
# === Source and header files
# ========================================
file(GLOB_RECURSE H2M_LIBRARY_SOURCES "h2m.cpp" "h2m_common.cpp" "h2m_config.cpp" "h2m_heuristics.cpp" "h2m_statistics.cpp" "h2m_tools.cpp" "h2m_utils_numa.cpp" "h2m_json.cpp")

# ========================================
# === Add include and link directories
# ========================================
set(H2M_LIBRARY_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR})
include_directories(${H2M_LIBRARY_INCLUDE_DIRS} ${HWLOC_INCLUDE_DIRS})
link_directories(${HWLOC_LIBRARY_DIRS})

if (H2M_TRACE_ITAC)
  include_directories($ENV{VT_ROOT}/include)
  link_directories($ENV{VT_ROOT}/lib)
endif()

include_directories(${ADDITIONAL_INCLUDES})

# ========================================
# === Generate version header file
# ========================================
configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/h2m_version.h.in
    ${CMAKE_CURRENT_SOURCE_DIR}/h2m_version.h
)

# ========================================
# === Set compile and link flags
# ========================================
set (ADDITIONAL_COMPILE_FLAGS ${ADDITIONAL_COMPILE_FLAGS} "")
if (H2M_ENABLE_DEBUG_OUTPUT)
    set (ADDITIONAL_COMPILE_FLAGS "${ADDITIONAL_COMPILE_FLAGS} -DH2M_DEBUG")
endif()

if (H2M_TRACE_ITAC)
    set (ADDITIONAL_COMPILE_FLAGS "${ADDITIONAL_COMPILE_FLAGS} -DTRACE -trace ")
endif()

if (H2M_RECORD_STATS)
    set (ADDITIONAL_COMPILE_FLAGS "${ADDITIONAL_COMPILE_FLAGS} -DH2M_RECORD_STATS=1")
else()
    set (ADDITIONAL_COMPILE_FLAGS "${ADDITIONAL_COMPILE_FLAGS} -DH2M_RECORD_STATS=0")
endif()

if (H2M_TOOL_SUPPORT)
    set (ADDITIONAL_COMPILE_FLAGS "${ADDITIONAL_COMPILE_FLAGS} -DTOOL_SUPPORT=1")
else()
    set (ADDITIONAL_COMPILE_FLAGS "${ADDITIONAL_COMPILE_FLAGS} -DTOOL_SUPPORT=0")
endif()

if (H2M_ENABLE_ALLOC_ERR_OUTPUT)
    set (ADDITIONAL_COMPILE_FLAGS "${ADDITIONAL_COMPILE_FLAGS} -DH2M_ENABLE_ALLOC_ERR_OUTPUT=1")
else()
    set (ADDITIONAL_COMPILE_FLAGS "${ADDITIONAL_COMPILE_FLAGS} -DH2M_ENABLE_ALLOC_ERR_OUTPUT=0")
endif()

set (ADDITIONAL_COMPILE_FLAGS "${ADDITIONAL_COMPILE_FLAGS} -DMAX_NUMA_NODES=${H2M_MAX_NUMA_NODES}")

set (H2M_LIB_LIBRARIES 
    ${H2M_LIB_LIBRARIES} 
    pthread 
    hwloc
    numa
    dl
)

# ========================================
# === Build targets
# ========================================
set (H2M_LIBRARY_NAME "h2m")

# TODO: Future Work
# # F90 module file
# add_library(
#     "h2m_lib"
#     MODULE
#     h2m_lib.f90
# )

# variables for pkg-config files
set (H2M_LIBRARY_NAME ${H2M_LIBRARY_NAME} PARENT_SCOPE)
set (H2M_PHASE_NAME ${H2M_PHASE_NAME} PARENT_SCOPE)
string(REPLACE ";" " " PKG_CONFIG_H2M_PHASE_REQUIRES_PRIVATE "${H2M_PHASE_LIBRARIES}")
set (PKG_CONFIG_H2M_PHASE_REQUIRES_PRIVATE "${PKG_CONFIG_H2M_PHASE_REQUIRES_PRIVATE}" PARENT_SCOPE)
string(REPLACE ";" " " PKG_CONFIG_H2M_LIB_REQUIRES_PRIVATE "${H2M_LIB_LIBRARIES}")
set (PKG_CONFIG_H2M_LIB_REQUIRES_PRIVATE "${PKG_CONFIG_H2M_LIB_REQUIRES_PRIVATE}" PARENT_SCOPE)

# define H2M shared library
add_library(
    ${H2M_LIBRARY_NAME}
    SHARED
    ${H2M_LIBRARY_SOURCES}
    # ${H2M_LIBRARY_SOURCES_F90}
)

set_target_properties(
    ${H2M_LIBRARY_NAME}
    PROPERTIES 
    POSITION_INDEPENDENT_CODE TRUE
)

# Linker settings
if (H2M_TRACE_ITAC)
    set (H2M_LIB_LIBRARIES 
      ${H2M_LIB_LIBRARIES} 
      VT
      vtunwind
      dwarf
      elf
    )
    set_property(TARGET ${H2M_LIBRARY_NAME} APPEND_STRING PROPERTY LINK_FLAGS " -DTRACE -trace ")
endif()

target_link_libraries(
    ${H2M_LIBRARY_NAME}
    ${H2M_LIB_LIBRARIES}
)

# Compile flags used for build
set_target_properties(
    ${H2M_LIBRARY_NAME}
    PROPERTIES 
    COMPILE_FLAGS "${ADDITIONAL_COMPILE_FLAGS}"
)

# ========================================
# === Installation
# ========================================
install(TARGETS ${H2M_LIBRARY_NAME} DESTINATION lib) # shared library
install(FILES "h2m_pre_init.h" "h2m.h" "h2m_tools.h" "h2m_utils_numa.h" "h2m_common.h" DESTINATION include) # headers
# install(FILES ${CMAKE_BINARY_DIR}/src/H2M_lib.mod DESTINATION include) # fortran mod file
