# Heuristics for Heterogeneous Memory (H2M) - Optimizing Data Placement using Memory Profiling

The following step-by-step guide describes the workflow to optimize the data placement based on previously collected memory profiling data for an application. In the subsequent text, we describe what needs to be performed in order to install required software and execute the workflow consisting of the following steps.
- Step 1: Profiling memory allocation and accesses of existing applications
- Step 2: Analyzing the profiling data and optimizing the data placement with the H2M trait recommender
- Step 3: Applying the data placement decision using allocation abstraction or interception

## 1. Profiling memory allocation and accesses of existing applications

For memory profiling of existing application we use the tool [NumaMMA](https://github.com/numamma/numamma), which is applied with an `LD_PRELOAD` mechanism. Internally, it is using a sampling based approach based on *Precise Event Based Sampling (PEBS)* or *Instruction Based Sampling (IBS)* for Intel or AMD architectures, respectively.

### 1.1 Requirements and installation
- Use GCC 10 or higher
- Clone the NumaMMA repository and install it including all dependencies
```bash
# define location where it should be installed
export NUMAMMA_ROOT_DIR=<desired-dir>

# download NumaMMA
git clone https://github.com/numamma/numamma.git
cd  numamma

# install NumaMMA + dependencies
chmod u+x install_everything.sh
./install_everything.sh ${NUMAMMA_ROOT_DIR}
```
- *Optional:* Recording phase transitions also requires the H2M library (specifically the `h2m_phase.so`)
```bash
# download H2M and follow the installation steps in the README.md
git clone https://gitlab.inria.fr/h2m/h2m.git
```

### 1.2 Profiling
- Make sure that path variables are extended to make NumaMMA and dependencies available at execution time
```bash
# define temporary path extensions
BASE_PATH=${NUMAMMA_ROOT_DIR}/install
TMP_INCLUDE_PATHS=${BASE_PATH}/libbacktrace/include:${BASE_PATH}/libelf/include:${BASE_PATH}/libpfm/include:${BASE_PATH}/numactl-2.0.12/include:${BASE_PATH}/numap/include:${BASE_PATH}/numamma/include
TMP_LIB_PATHS=${BASE_PATH}/libbacktrace/lib/:${BASE_PATH}/libelf/lib:${BASE_PATH}/libpfm/lib:${BASE_PATH}/numactl-2.0.12/lib:${BASE_PATH}/numap/lib:${BASE_PATH}/numamma/lib
TMP_BIN_PATH=${BASE_PATH}/libelf/bin:${BASE_PATH}/numactl-2.0.12/bin:${BASE_PATH}/numamma/bin

# prepend path environment variables
export LD_LIBRARY_PATH="${TMP_LIB_PATHS}:${LD_LIBRARY_PATH}"
export LIBRARY_PATH="${TMP_LIB_PATHS}:${LIBRARY_PATH}"
export INCLUDE="${TMP_INCLUDE_PATHS}:${INCLUDE}"
export CPATH="${TMP_INCLUDE_PATHS}:${CPATH}"
export C_INCLUDE_PATH="${TMP_INCLUDE_PATHS}:${C_INCLUDE_PATH}"
export CPLUS_INCLUDE_PATH="${TMP_INCLUDE_PATHS}:${CPLUS_INCLUDE_PATH}"
export PATH="${TMP_BIN_PATH}:${PATH}"
```
- *Optional:* If phase transitions are used in the application, make sure that H2M is available in the env paths variables as well
```bash
BASE_PATH=/home/sdp/jannis/installed/h2m

# define temporary path extensions
TMP_INCLUDE_PATHS=${BASE_PATH}/include
TMP_LIB_PATHS=${BASE_PATH}/lib

# prepend path environment variables
export LD_LIBRARY_PATH="${TMP_LIB_PATHS}:${LD_LIBRARY_PATH}"
export LIBRARY_PATH="${TMP_LIB_PATHS}:${LIBRARY_PATH}"
export INCLUDE="${TMP_INCLUDE_PATHS}:${INCLUDE}"
export CPATH="${TMP_INCLUDE_PATHS}:${CPATH}"
export C_INCLUDE_PATH="${TMP_INCLUDE_PATHS}:${C_INCLUDE_PATH}"
export CPLUS_INCLUDE_PATH="${TMP_INCLUDE_PATHS}:${CPLUS_INCLUDE_PATH}"
```
- Run your application while profiling it with NumaMMA
```bash
# define your alarm and sampling rate
ALARM_RATE=100
SAMPLING_RATE=6000

# define the result directory
RES_DIR=$(pwd)/results
mkdir -p ${RES_DIR}
export H2M_DUMP_DIR=${RES_DIR}

# define your executable and parameters
EXEC_PATH=<...>
EXEC_PARAMS=<...>

# run profiling
numamma --alarm=${ALARM_RATE} --sampling-rate=${SAMPLING_RATE} --buffer-size=512 -d -D --outputdir=/${RES_DIR} -- ${EXEC_PATH} ${EXEC_PARAMS}
```
- The result directory should now contain the NumaMMA dumps, specifically the following items that are required for the remaining workflow
  - `all_memory_accesses.dat`
  - `all_memory_objects.dat`
  - `h2m_dump_phase_transitions.csv` (only if phase transitions have been recorded)

## 2. Analyzing dumps and optimizing the data placement

### 2.1 Requirements and installation

### 2.2 Data placement optimization (post-mortem)

## 3. Applying data placement decisions
- Talk about abstraction and interception

### 3.1 Requirements and installation

### 3.2 Allocation abstraction

### 3.3 Allocation interception

