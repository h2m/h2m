#include "compute_kernels.h"

int compute_dot_prod_strided(double * SPEC_RESTRICT a, double * SPEC_RESTRICT b, double * SPEC_RESTRICT c, size_t N, int repetitions) {
    volatile int n_rep = 0;
    for(int r = 0; r < repetitions; r++) {
        #pragma omp parallel for schedule(static)
        for(size_t i = 0; i < N; i++) {
            for(size_t j = 0; j < N; j++) {
                // NOTE: traverse c and a linearly, and b in strided fashion
                c[i * N + j] += a[i * N + j] * b[j * N + i];
            }
        }
        n_rep += r; // prevent optimizing out iteration loop
    }
    return n_rep;
}

int compute_dot_prod_linear(double * SPEC_RESTRICT a, double * SPEC_RESTRICT b, double * SPEC_RESTRICT c, size_t N, int repetitions) {
    volatile int n_rep = 0;
    for(int r = 0; r < repetitions; r++) {
        #pragma omp parallel for schedule(static)
        for(size_t i = 0; i < N; i++) {
            for(size_t j = 0; j < N; j++) {
                // NOTE traverse all items in a linearly fashion
                c[i * N + j] += a[i * N + j] * b[i * N + j];
            }
        }
        n_rep += r; // prevent optimizing out iteration loop
    }
    return n_rep;
}

int compute_DGEMM(double * SPEC_RESTRICT a, double * SPEC_RESTRICT b, double * SPEC_RESTRICT c, size_t N, int repetitions) {
    volatile int n_rep = 0;
    for(int r = 0; r < repetitions; r++) {
        #pragma omp parallel for schedule(static)
        for(size_t i = 0; i < N; i++) {
            for(size_t j = 0; j < N; j++) {
                double tmp = 0.0;
                for(size_t k = 0; k < N; k++) {
                    tmp += a[i * N + k] * b[k * N + j];
                }
                c[i * N + j] = tmp;
            }
        }
        n_rep += r; // prevent optimizing out iteration loop
    }
    return n_rep;
}

int compute_DGEMM_linear(double * SPEC_RESTRICT a, double * SPEC_RESTRICT b, double * SPEC_RESTRICT c, size_t N, int repetitions) {
    volatile int n_rep = 0;
    for(int r = 0; r < repetitions; r++) {
        #pragma omp parallel for schedule(static)
        for(size_t i = 0; i < N; i++) {
            for(size_t j = 0; j < N; j++) {
                double tmp = 0.0;
                for(size_t k = 0; k < N; k++) {
                    tmp += a[i * N + k] * b[j * N + k];
                }
                c[i * N + j] = tmp;
            }
        }
        n_rep += r; // prevent optimizing out iteration loop
    }
    return n_rep;
}

int compute_daxpy(double * SPEC_RESTRICT a, double * SPEC_RESTRICT b, size_t N, double da, int repetitions) {
    volatile int n_rep = 0;
    for(int r = 0; r < repetitions; r++) {
        #pragma omp parallel for schedule(static)
        for(size_t i = 0; i < N; i++) {
            a[i] = a[i] + da * b[i];
        }
        n_rep += r; // prevent optimizing out iteration loop
    }
    return n_rep;
}

int compute_stencil(double * SPEC_RESTRICT a, double * SPEC_RESTRICT b, size_t N, int is_star, int radius, int repetitions) {
    volatile int n_rep = 0;
    for(int r = 0; r < repetitions; r++) {

        if(is_star) {
            int cnt = 4*radius+1;
            #pragma omp parallel for schedule(static)
            for(size_t i = radius; i < N-radius; i++) {
                for(size_t j = radius; j < N-radius; j++) {
                    double tmp = 0.0;
                    for (size_t ii = -radius; ii <= radius; ii++)   tmp += b[ii * N + j];
                    for (size_t jj = -radius; jj < 0; jj++)         tmp += b[i * N + j+jj];
                    for (size_t jj = 1; jj <= radius; jj++)         tmp += b[i * N + j+jj];
                    a[i * N + j] = tmp / cnt;
                }
            }
        } else {
            int cnt = (2*radius+1)*(2*radius+1);
            #pragma omp parallel for schedule(static)
            for(size_t i = radius; i < N-radius; i++) {
                for(size_t j = radius; j < N-radius; j++) {
                    double tmp = b[i * N + j];
                    for (size_t ii = -radius; ii <= radius; ii++) {
                        for (size_t jj = -radius; jj <= radius; jj++) {
                            tmp += b[i+ii * N + j+jj];
                        }
                    }
                    a[i * N + j] = tmp / cnt;
                }
            }
        }
        n_rep += r; // prevent optimizing out iteration loop
    }
    return n_rep;
}