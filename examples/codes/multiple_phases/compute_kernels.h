#ifndef __COMPUTE_KERNELS_H__
#define __COMPUTE_KERNELS_H__

#include <cstddef>
#include <cstdlib>

#define SPEC_RESTRICT __restrict__
//#define SPEC_RESTRICT restrict

int compute_dot_prod_strided(double * SPEC_RESTRICT a, double * SPEC_RESTRICT b, double * SPEC_RESTRICT c, size_t N, int repetitions);
int compute_dot_prod_linear (double * SPEC_RESTRICT a, double * SPEC_RESTRICT b, double * SPEC_RESTRICT c, size_t N, int repetitions);
int compute_DGEMM           (double * SPEC_RESTRICT a, double * SPEC_RESTRICT b, double * SPEC_RESTRICT c, size_t N, int repetitions);
int compute_DGEMM_linear    (double * SPEC_RESTRICT a, double * SPEC_RESTRICT b, double * SPEC_RESTRICT c, size_t N, int repetitions);
int compute_daxpy           (double * SPEC_RESTRICT a, double * SPEC_RESTRICT b, size_t N, double da, int repetitions);
int compute_stencil         (double * SPEC_RESTRICT a, double * SPEC_RESTRICT b, size_t N, int is_star, int radius, int repetitions);

#define NUM_KERNEL_TYPES 6

typedef enum {
    DOT_STRIDED = 0,
    DOT_LINEAR = 1,
    DGEMM = 2,
    DGEMM_LINEAR = 3,
    DAXPY = 4,
    STENCIL = 5
} KernelType;

static int NUM_KERNEL_BUFFERS[NUM_KERNEL_TYPES] = {3,3,3,3,2,2};

#endif // __COMPUTE_KERNELS_H__

