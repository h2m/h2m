#include "def.h"

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <string.h>

int parse_arguments(int argc, char **argv, Config *cfg) {
    if(argc > 1) {
        if (strcmp(argv[1], "--help") == 0 || strcmp(argv[1], "-h") == 0) {
            std::cout << "Usage: ./multiple_phases.exe" << std::endl;
            std::cout << "  Further, you can set the following environment variables: " << std::endl;
            std::cout << "    MP_NUM_ALLOCS:             Overall number of allocations" << std::endl;
            std::cout << "    MP_MATRIX_SIZE:            Matrix size to use for each data item" << std::endl;
            std::cout << "    MP_NUM_PHASES:             Number of phases to execute" << std::endl;
            std::cout << "    MP_NUM_KERNELS_PER_PHASE:  Number of kernels per phase" << std::endl;
            std::cout << "    MP_NUM_ITER_PER_PHASE:     Number of iterations per phase" << std::endl;
            std::cout << "    MP_ALLOC_GROUP_ID:         Allocation group id for h2m_alloc call" << std::endl << std::endl;

            std::cout << "  Default values are:" << std::endl;
            std::cout << "    MP_NUM_ALLOCS = " << cfg->num_allocations << std::endl;
            std::cout << "    MP_MATRIX_SIZE = " << cfg->matrix_size << std::endl;
            std::cout << "    MP_NUM_PHASES = " << cfg->num_phases << std::endl;
            std::cout << "    MP_NUM_KERNELS_PER_PHASE = " << cfg->num_kernels_per_phase << std::endl;
            std::cout << "    MP_NUM_ITER_PER_PHASE = " << cfg->num_iterations_per_phase << std::endl;
            std::cout << "    MP_ALLOC_GROUP_ID = " << cfg->alloc_group_id << std::endl;
            return 1;
        }
    }

    // parse environment variables
    char* tmp = nullptr;

    tmp = std::getenv("MP_NUM_ALLOCS");
    if(tmp) {
        cfg->num_allocations = std::atoi(tmp);
    }

    tmp = std::getenv("MP_MATRIX_SIZE");
    if(tmp) {
        cfg->matrix_size = std::atoi(tmp);
    }

    tmp = std::getenv("MP_NUM_PHASES");
    if(tmp) {
        cfg->num_phases = std::atoi(tmp);
    }

    tmp = std::getenv("MP_NUM_KERNELS_PER_PHASE");
    if(tmp) {
        cfg->num_kernels_per_phase = std::atoi(tmp);
    }

    tmp = std::getenv("MP_NUM_ITER_PER_PHASE");
    if(tmp) {
        cfg->num_iterations_per_phase = std::atoi(tmp);
    }
    
    tmp = std::getenv("MP_ALLOC_GROUP_ID");
    if(tmp) {
        cfg->alloc_group_id = std::atoi(tmp);
    }

    return 0;
}

void set_default_config(Config *cfg) {
    cfg->num_allocations = 30;
    cfg->matrix_size = 1600;
    cfg->num_phases = 20;
    cfg->num_kernels_per_phase = 5;
    cfg->num_iterations_per_phase = 2;
    cfg->alloc_group_id = -1;
}

void print_config(Config *cfg) {
    printf("=== Configuration ===\n");
    printf("-- num_phases: %d\n", cfg->num_phases);
    printf("-- num_kernels_per_phase: %d\n", cfg->num_kernels_per_phase);
    printf("-- num_iterations_per_phase: %d\n", cfg->num_iterations_per_phase);
    printf("-- num_allocations: %d\n", cfg->num_allocations);
    printf("-- matrix_size: %zux%zu\n", cfg->matrix_size, cfg->matrix_size);
    printf("-- alloc_group_id: %d\n", cfg->alloc_group_id);
    printf("-- USE_H2M_ALLOC_ABSTRACTION: %d\n", USE_H2M_ALLOC_ABSTRACTION);
    printf("-- RECORD_TRANSITIONS: %d\n", RECORD_TRANSITIONS);
    printf("\n");
}

void select_distinct_data_items(int num_items, int max_val, int* idx_vals) {
    for(int i = 0; i < num_items; i++) {
        bool duplicate = true;
        int cur_idx;
        
        // avoid selecting data item twice
        while (duplicate) {
            cur_idx = rand() % max_val;
            duplicate = false;
            for (int j = 0; j < i; j++) {
                if(idx_vals[j] == cur_idx) {
                    duplicate = true;
                    break;
                }
            }
        }
        // add to list
        idx_vals[i] = cur_idx;
    }
}

double f_rand(double fMin, double fMax) {
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

int i_rand(int iMin, int iMax) {
    return iMin + (rand() % (iMax - iMin + 1));
}