#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <inttypes.h>
#include <iostream>
#include <math.h>
#include <omp.h>
#include <string>
#include <string.h>

#include "def.h"
#include "compute_kernels.h"

#if USE_H2M_ALLOC_ABSTRACTION || RECORD_TRANSITIONS
#include "h2m.h"
#endif

void initialize_matrix(double * arr, size_t N, double val) {
    #pragma omp parallel for schedule(static)
    for(size_t i = 0; i < N * N; i++) {
        arr[i] = val;
    }
}

int main(int argc, char **argv) {
    // dont buffer stdout
    setbuf(stdout, NULL);
    int err;
    double t_alloc, t_execution, t_phase, t_kernel;
    double **data_items;
    int *item_usage;
    size_t alloc_size;
    Config cfg;

#if USE_H2M_ALLOC_ABSTRACTION || RECORD_TRANSITIONS
    h2m_init();
    #pragma omp parallel
    {
        h2m_thread_init();
    }
#endif // USE_H2M_ALLOC_ABSTRACTION || RECORD_TRANSITIONS

    // ensure reproducability for runs with random numbers
    srand(42);

    set_default_config(&cfg);
    int ret_code = parse_arguments(argc, argv, &cfg);
    if (ret_code != 0) {
        return ret_code;
    }

    printf("Running sample application with multiple application phases that use different data items\n");
    print_config(&cfg);

    // ============================================================
    // === Data Allocation
    // ============================================================
#if USE_H2M_ALLOC_ABSTRACTION || RECORD_TRANSITIONS
    h2m_phase_transition("Data Allocation");
#endif // USE_H2M_ALLOC_ABSTRACTION || RECORD_TRANSITIONS
    t_alloc = omp_get_wtime();
    alloc_size = cfg.matrix_size * cfg.matrix_size * sizeof(double);
    data_items = new double*[cfg.num_allocations];
    item_usage = new int[cfg.num_allocations];

    for(int i = 0; i < cfg.num_allocations; i++) {
        // first allocate
        #if USE_H2M_ALLOC_ABSTRACTION
        data_items[i] = (double*) h2m_alloc_w_traits_file(alloc_size, cfg.alloc_group_id, &err);
        #else
        data_items[i] = (double*) malloc(alloc_size);
        #endif // USE_H2M_ALLOC_ABSTRACTION
        // now initalize in parallel
        initialize_matrix(data_items[i], cfg.matrix_size, f_rand(-42, 42));
        // reset counter
        item_usage[i] = 0;
    }
    t_alloc = omp_get_wtime() - t_alloc;

    // ============================================================
    // === Phase execution
    // ============================================================
    int dummy_returns[cfg.num_kernels_per_phase];
    int idx_di[cfg.num_kernels_per_phase][3]; // buffer reused for data item idx
    int idx_kernels[cfg.num_kernels_per_phase];
    int kernel_reps[cfg.num_kernels_per_phase];
    int kernel_is_star[cfg.num_kernels_per_phase];
    int kernel_is_radius[cfg.num_kernels_per_phase];

    t_execution = omp_get_wtime();

    for(int p = 0; p < cfg.num_phases; p++) {
#if USE_H2M_ALLOC_ABSTRACTION || RECORD_TRANSITIONS
        std::string tmp = "PhaseStart " + std::to_string(p);
        h2m_phase_transition(tmp.c_str());
#endif // USE_H2M_ALLOC_ABSTRACTION || RECORD_TRANSITIONS
        printf("Start execution of phase %d\n", p);
        
        t_phase = omp_get_wtime();
        for(int iter = 0; iter < cfg.num_iterations_per_phase; iter++) {
            for(int k = 0; k < cfg.num_kernels_per_phase; k++) {
                if(iter == 0) {
                    // determine which kernel should be executed
                    idx_kernels[k] = rand() % NUM_KERNEL_TYPES;
                    int n_buffers = NUM_KERNEL_BUFFERS[idx_kernels[k]];

                    // determine data items to be used for kernel
                    select_distinct_data_items(3, cfg.num_allocations, idx_di[k]);

                    // mark buffers as used
                    for(int i = 0; i < n_buffers; i++) {
                        item_usage[idx_di[k][i]]++;
                    }
                }

                KernelType kt = (KernelType) idx_kernels[k];
                double* SPEC_RESTRICT mA = data_items[idx_di[k][0]];
                double* SPEC_RESTRICT mB = data_items[idx_di[k][1]];
                double* SPEC_RESTRICT mC = data_items[idx_di[k][2]];

                switch(kt) {
                    case DGEMM:
                    {
                        if(iter == 0) kernel_reps[k] = i_rand(1, 2); // DGEMM is way more expensive
                        t_kernel = omp_get_wtime();
                        dummy_returns[k] = compute_DGEMM(mA, mB, mC, cfg.matrix_size, kernel_reps[k]);
                        t_kernel = omp_get_wtime() - t_kernel;
                        printf("-- K-%02d DGEMM        data-items=%02d,%02d,%02d  reps=%02d --> %f sec\n", k, idx_di[k][0], idx_di[k][1], idx_di[k][2], kernel_reps[k], t_kernel);
                        break;
                    }
                    case DGEMM_LINEAR:
                    {
                        if(iter == 0) kernel_reps[k] = i_rand(1, 2); // DGEMM is way more expensive
                        t_kernel = omp_get_wtime();
                        dummy_returns[k] = compute_DGEMM_linear(mA, mB, mC, cfg.matrix_size, kernel_reps[k]);
                        t_kernel = omp_get_wtime() - t_kernel;
                        printf("-- K-%02d DGEMM_LINEAR data-items=%02d,%02d,%02d  reps=%02d --> %f sec\n", k, idx_di[k][0], idx_di[k][1], idx_di[k][2], kernel_reps[k], t_kernel);
                        break;
                    }
                    case DOT_STRIDED:
                    {
                        if(iter == 0) kernel_reps[k] = i_rand(15, 45);
                        t_kernel = omp_get_wtime();
                        dummy_returns[k] = compute_dot_prod_strided(mA, mB, mC, cfg.matrix_size, kernel_reps[k]);
                        t_kernel = omp_get_wtime() - t_kernel;
                        printf("-- K-%02d DOT_STRIDED  data-items=%02d,%02d,%02d  reps=%02d --> %f sec\n", k, idx_di[k][0], idx_di[k][1], idx_di[k][2], kernel_reps[k], t_kernel);
                        break;
                    }
                    case DOT_LINEAR:
                    {
                        if(iter == 0) kernel_reps[k] = i_rand(15, 45);
                        t_kernel = omp_get_wtime();
                        dummy_returns[k] = compute_dot_prod_linear(mA, mB, mC, cfg.matrix_size, kernel_reps[k]);
                        t_kernel = omp_get_wtime() - t_kernel;
                        printf("-- K-%02d DOT_LINEAR   data-items=%02d,%02d,%02d  reps=%02d --> %f sec\n", k, idx_di[k][0], idx_di[k][1], idx_di[k][2], kernel_reps[k], t_kernel);
                        break;
                    }
                    case DAXPY:
                    {
                        if(iter == 0) kernel_reps[k] = i_rand(15, 45);
                        double da = f_rand(-42, 42);
                        t_kernel = omp_get_wtime();
                        dummy_returns[k] = compute_daxpy(mA, mB, cfg.matrix_size * cfg.matrix_size, da, kernel_reps[k]);
                        t_kernel = omp_get_wtime() - t_kernel;
                        printf("-- K-%02d DAXPY        data-items=%02d,%02d     reps=%02d da=%f --> %f sec\n", k, idx_di[k][0], idx_di[k][1], kernel_reps[k], da, t_kernel);
                        break;
                    }
                    case STENCIL:
                    {
                        if(iter == 0) kernel_reps[k] = i_rand(15, 45);
                        if(iter == 0) kernel_is_star[k] = i_rand(0, 1);
                        if(iter == 0) kernel_is_radius[k] = i_rand(1, 3);
                        t_kernel = omp_get_wtime();
                        dummy_returns[k] = compute_stencil(mA, mB, cfg.matrix_size, kernel_is_star[k], kernel_is_radius[k], kernel_reps[k]);
                        t_kernel = omp_get_wtime() - t_kernel;
                        printf("-- K-%02d STENCIL      data-items=%02d,%02d     reps=%02d is_star=%d radius=%d --> %f sec\n", k, idx_di[k][0], idx_di[k][1], kernel_reps[k], kernel_is_star[k], kernel_is_radius[k], t_kernel);
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
            }
        }
        t_phase = omp_get_wtime() - t_phase;

        // print dummy values once to avoid optimizing out variables by compiler
        printf("-- DummyValues [%d]: ", p);
        for (int k = 0; k < cfg.num_kernels_per_phase; k++) {
            printf("%d,", dummy_returns[k]);
        }
        printf("\nElapsed time for phase %d is %.5f sec\n\n", p, t_phase);
    }

    t_execution = omp_get_wtime() - t_execution;
    printf("Data allocation took %.5f sec\n", t_alloc);
    printf("Computations took %.5f sec\n\n", t_execution);

    // print item usage once
    printf("Data item usage: [");
    for(int i = 0; i < cfg.num_allocations; i++) {
        if(i == cfg.num_allocations-1) {
            printf("%d", item_usage[i]);
        } else {
            printf("%d,", item_usage[i]);
        }        
    }
    printf("]\n");
    
    // ============================================================
    // === Cleanup
    // ============================================================
    for(int i = 0; i < cfg.num_allocations; i++) {
        // first allocate
        #if USE_H2M_ALLOC_ABSTRACTION
        h2m_free(data_items[i]);
        #else
        free(data_items[i]);
        #endif // USE_H2M_ALLOC_ABSTRACTION
    }
    delete[] data_items;
    delete[] item_usage;

#if USE_H2M_ALLOC_ABSTRACTION || RECORD_TRANSITIONS
    #pragma omp parallel
    {
        h2m_thread_finalize();
    }
    h2m_finalize();
#endif // USE_H2M_ALLOC_ABSTRACTION || RECORD_TRANSITIONS

    return 0;
}