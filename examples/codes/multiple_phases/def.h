#ifndef __DEF_H__
#define __DEF_H__

#ifndef USE_H2M_ALLOC_ABSTRACTION
#define USE_H2M_ALLOC_ABSTRACTION 0
#endif

#ifndef RECORD_TRANSITIONS
#define RECORD_TRANSITIONS 0
#endif

#include <cstddef>
#include <omp.h>

typedef struct {
    int     num_phases;
    int     num_kernels_per_phase;
    int     num_iterations_per_phase;
    int     num_allocations;
    size_t  matrix_size;
    int     max_repetitions;
    int     alloc_group_id;
} Config;

int parse_arguments(int argc, char **argv, Config *cfg);
void set_default_config(Config *cfg);
void print_config(Config *cfg);
void select_distinct_data_items(int num_items, int max_val, int* idx_vals);
double f_rand(double fMin, double fMax);
int i_rand(int iMin, int iMax);

#endif // __DEF_H__

