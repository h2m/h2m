#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <inttypes.h>
#include <iostream>
#include <math.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/syscall.h>

#include "h2m.h"
#include "h2m_pre_init.h"
#include "h2m_utils_numa.h"

#ifndef PARALLEL_INIT
#define PARALLEL_INIT 1
#endif

#ifndef ITERATIVE_VERSION
#define ITERATIVE_VERSION 1
#endif

#ifndef NUM_ITERATIONS
#define NUM_ITERATIONS 5
#endif

#ifndef NUM_REPETITIONS
#define NUM_REPETITIONS 2
#endif

#ifndef N_PHASES
#define N_PHASES 4
#endif

#ifndef USE_DATA_INIT_FCN
#define USE_DATA_INIT_FCN 1
#endif

#define SPEC_RESTRICT __restrict__
//#define SPEC_RESTRICT restrict

#ifndef DPxPTR
#define DPxPTR(ptr) ((int)(2*sizeof(uintptr_t))), ((uintptr_t) (ptr))
#endif

int num_tasks       = 200;
int matrix_size     = 600;
int prefetch        = 0;
int prefetch_offset = 1;

typedef struct data_init_args_t {
    size_t matrix_size;
    double value;
} data_init_args_t;

void initialize_matrix(void *ptr, void* args) {
    double *arr = (double *)ptr;
    data_init_args_t* init_args = (data_init_args_t*)args; 
    for(int i = 0; i < init_args->matrix_size * init_args->matrix_size; i++) {
        arr[i] = init_args->value;
    }
}

int compute_dot_prod(double * SPEC_RESTRICT a, double * SPEC_RESTRICT b, double * SPEC_RESTRICT c, int matrix_size) {
    // make the tasks more computational expensive by repeating this operation several times to better see effects 
    volatile int n_rep = 0;
    for(int r = 0; r < NUM_REPETITIONS; r++) {
        memset(c, 0, matrix_size * matrix_size * sizeof(double));
        for(int i = 0; i < matrix_size; i++) {
            // for(int j = 0; j < matrix_size; j++) {
            //     c[i * matrix_size + j] += a[i * matrix_size + j] * b[j * matrix_size + i];
            // }
            for(int j = 0; j < matrix_size; j++) {
                double tmp = 0;
                for(int k = 0; k < matrix_size; k++) {
                    tmp += a[i * matrix_size + k] * b[k * matrix_size + j];
                }
                c[i * matrix_size + j] = tmp;
            }
        }
        n_rep += r; // prevent optimizing out iteration loop
    }
    return n_rep;
}

bool check_matrix(double * SPEC_RESTRICT c, int array_idx, int matrix_size, double desired_val) {
    if (NUM_REPETITIONS > 0) {
        for(int i = 0; i < matrix_size * matrix_size; i++) {
            double val = c[i];
            if(fabs(val - desired_val) > 1e-3) {
                printf("(OS_TID:%ld): Error in array %03d entry (%d) expected:%f but value is %f\n", syscall(SYS_gettid), array_idx, i, desired_val, val);
                return false;
            }
        }
    }
    return true;
}

int parse_command_line_args(int argc, char **argv) {
    if(argc > 1) {
        if (strcmp(argv[1], "--help") == 0 || strcmp(argv[1], "-h") == 0) {
            std::cout << "Usage: ./dot_prod <n_tasks> <matrix_size>" << std::endl;
            std::cout << "  Arguments: " << std::endl;
            std::cout << "    n_tasks:     Overall number of dot prod tasks" << std::endl;
            std::cout << "    matrix_size: Matrix size to use for each dot prod" << std::endl;
            std::cout << "    prefetch:    Flag whether to prefetch (run migration in background) or not. Default=0" << std::endl;
            return 1;
        }
        num_tasks = atoi(argv[1]);
    }

    if(argc > 2) {
        matrix_size = atoi(argv[2]);
    }

    if(argc > 3) {
        prefetch = atoi(argv[3]);
    }

    return 0;
}

int main(int argc, char **argv)
{
    // parse command line arguments
    int ret_code = parse_command_line_args(argc, argv);
    if (ret_code != 0) {
        return ret_code;
    }

    // library initialization
    h2m_pre_init();
    #pragma omp parallel
    {
        h2m_thread_init();
    }
    h2m_post_init_serial();

    data_init_args_t* init_args_0  = (data_init_args_t*) malloc(sizeof(data_init_args_t));
    data_init_args_t* init_args_42 = (data_init_args_t*) malloc(sizeof(data_init_args_t));
    init_args_0->matrix_size = matrix_size;
    init_args_0->value = 0;
    init_args_42->matrix_size = matrix_size;
    init_args_42->value = 42;

    // Traits for HBM
    h2m_alloc_trait_t traits_hbm_readwrite_linear[6] = {
        h2m_atk_req_mem_space, h2m_atv_mem_space_hbw, 
        h2m_atk_mem_alignment, 4096,
        h2m_atk_access_mode, h2m_atv_access_mode_readwrite,
        h2m_atk_access_origin, h2m_atv_access_origin_single_threaded,
        h2m_atk_access_pattern, h2m_atv_access_pattern_linear,
        h2m_atk_structure_type, h2m_atv_structure_type_matrix
        };
    h2m_alloc_trait_t traits_hbm_readonly_linear[6] = {
        h2m_atk_req_mem_space, h2m_atv_mem_space_hbw, 
        h2m_atk_mem_alignment, 4096,
        h2m_atk_access_mode, h2m_atv_access_mode_readonly,
        h2m_atk_access_origin, h2m_atv_access_origin_single_threaded,
        h2m_atk_access_pattern, h2m_atv_access_pattern_linear,
        h2m_atk_structure_type, h2m_atv_structure_type_matrix
        };
    h2m_alloc_trait_t traits_hbm_readonly_strided[7] = {
        h2m_atk_req_mem_space, h2m_atv_mem_space_hbw, 
        h2m_atk_mem_alignment, 4096,
        h2m_atk_access_mode, h2m_atv_access_mode_readonly,
        h2m_atk_access_origin, h2m_atv_access_origin_single_threaded,
        h2m_atk_access_pattern, h2m_atv_access_pattern_strided,
        h2m_atk_access_stride, matrix_size,
        h2m_atk_structure_type, h2m_atv_structure_type_matrix
        };

    // Traits for Large Cap
    h2m_alloc_trait_t traits_lcap_readwrite_linear[6] = {
        h2m_atk_req_mem_space, h2m_atv_mem_space_large_cap, 
        h2m_atk_mem_alignment, 4096,
        h2m_atk_access_mode, h2m_atv_access_mode_readwrite,
        h2m_atk_access_origin, h2m_atv_access_origin_single_threaded,
        h2m_atk_access_pattern, h2m_atv_access_pattern_linear,
        h2m_atk_structure_type, h2m_atv_structure_type_matrix
        };
    h2m_alloc_trait_t traits_lcap_readonly_linear[6] = {
        h2m_atk_req_mem_space, h2m_atv_mem_space_large_cap, 
        h2m_atk_mem_alignment, 4096,
        h2m_atk_access_mode, h2m_atv_access_mode_readonly,
        h2m_atk_access_origin, h2m_atv_access_origin_single_threaded,
        h2m_atk_access_pattern, h2m_atv_access_pattern_linear,
        h2m_atk_structure_type, h2m_atv_structure_type_matrix
        };
    h2m_alloc_trait_t traits_lcap_readonly_strided[7] = {
        h2m_atk_req_mem_space, h2m_atv_mem_space_large_cap, 
        h2m_atk_mem_alignment, 4096,
        h2m_atk_access_mode, h2m_atv_access_mode_readonly,
        h2m_atk_access_origin, h2m_atv_access_origin_single_threaded,
        h2m_atk_access_pattern, h2m_atv_access_pattern_strided,
        h2m_atk_access_stride, matrix_size,
        h2m_atk_structure_type, h2m_atv_structure_type_matrix
        };
    
    // verify that traits are sufficient for the chosen strategy
    int traits_ok = 0;
    traits_ok += h2m_verify_traits(6, traits_hbm_readwrite_linear);
    traits_ok += h2m_verify_traits(6, traits_hbm_readonly_linear);
    traits_ok += h2m_verify_traits(7, traits_hbm_readonly_strided);
    traits_ok += h2m_verify_traits(6, traits_lcap_readwrite_linear);
    traits_ok += h2m_verify_traits(6, traits_lcap_readonly_linear);
    traits_ok += h2m_verify_traits(7, traits_lcap_readonly_strided);
    
    if (traits_ok != H2M_SUCCESS) {
        printf("Error: Trait sets for H2M strategy / heuristic do not fulfill the necessary requiremnets.\n");
        return 1;
    }
    
    printf("Matrix Dot Prod sample application using H2M version %s. Creating %d tasks per phase with matrix size %dx%d. Prefetching = %d (offset %d)\n", h2m_get_version(), num_tasks, matrix_size, matrix_size, prefetch, prefetch_offset);

    int err;
    double t_elapsed_alloc = omp_get_wtime();
    size_t tmp_alloc_size = (size_t)matrix_size * matrix_size * sizeof(double);

    double ***matrices_a = new double**[N_PHASES];
    double ***matrices_b = new double**[N_PHASES];
    double ***matrices_c = new double**[N_PHASES];
    for(int p = 0; p < N_PHASES; p++) {
        matrices_a[p] = new double*[num_tasks]; 
        matrices_b[p] = new double*[num_tasks];
        matrices_c[p] = new double*[num_tasks];
    }

    std::vector<h2m_declaration_t> list_declarations;
    h2m_fcn_data_init_t fcn = nullptr;
#if USE_DATA_INIT_FCN
    fcn = &initialize_matrix;
#endif // USE_DATA_INIT_FCN
    for(int i = 0; i < num_tasks; i++) {
        for(int p = 0; p < N_PHASES; p++) {
            if (p == 0) {
                // first phase should be in HBW memory
                list_declarations.push_back(h2m_declare_alloc_w_traits((void **)&(matrices_a[p][i]), tmp_alloc_size, fcn, (void *)init_args_42, &err, 6, traits_hbm_readonly_linear));
                list_declarations.push_back(h2m_declare_alloc_w_traits((void **)&(matrices_b[p][i]), tmp_alloc_size, fcn, (void *)init_args_42, &err, 7, traits_hbm_readonly_strided));
                list_declarations.push_back(h2m_declare_alloc_w_traits((void **)&(matrices_c[p][i]), tmp_alloc_size, fcn, (void *)init_args_0,  &err, 6, traits_hbm_readwrite_linear));
            } else {
                // rest is initially on Large Cap memory
                list_declarations.push_back(h2m_declare_alloc_w_traits((void **)&(matrices_a[p][i]), tmp_alloc_size, fcn, (void *)init_args_42, &err, 6, traits_lcap_readonly_linear));
                list_declarations.push_back(h2m_declare_alloc_w_traits((void **)&(matrices_b[p][i]), tmp_alloc_size, fcn, (void *)init_args_42, &err, 7, traits_lcap_readonly_strided));
                list_declarations.push_back(h2m_declare_alloc_w_traits((void **)&(matrices_c[p][i]), tmp_alloc_size, fcn, (void *)init_args_0,  &err, 6, traits_lcap_readwrite_linear));
            }
        }
    }
    
    // commit allocations now (and potentially also initialize data)
    err = h2m_commit_allocs(&(list_declarations[0]), list_declarations.size());
    // free declarations again
    for(int i = 0; i < list_declarations.size(); i++) {
        h2m_declaration_free(list_declarations[i]);
    }
    list_declarations.clear();

#if !USE_DATA_INIT_FCN
#if PARALLEL_INIT
    printf("Executing parallel init\n");
    #pragma omp parallel for
#endif // PARALLEL_INIT
    for(int i = 0; i < num_tasks; i++) {        
        for(int p = 0; p < N_PHASES; p++) {
            initialize_matrix(matrices_a[p][i], (void *)init_args_42);
            initialize_matrix(matrices_b[p][i], (void *)init_args_42);
            initialize_matrix(matrices_c[p][i], (void *)init_args_0);
        }
    }
#endif // USE_DATA_INIT_FCN
    t_elapsed_alloc = omp_get_wtime() - t_elapsed_alloc;

    double t_elapsed = omp_get_wtime();
    #pragma omp parallel
    {
#if ITERATIVE_VERSION
        for(int iter = 0; iter < NUM_ITERATIONS; iter++) {
            #pragma omp master
            printf("Executing iteration %d ...\n", iter);
#endif // ITERATIVE_VERSION
            
            for(int p = 0; p < N_PHASES; p++) {
                // ========== Migrate ==========
                #pragma omp single
                {
                    // determine which arrays to move to hbm and large cap memory
                    int idx_hbm        = prefetch ? (p + prefetch_offset) % N_PHASES : p;
                    int idx_large_cap  = p == 0 ? N_PHASES-1 : p-1;
                    for(int i = 0; i < num_tasks; i++) {
                        h2m_update_traits(matrices_a[idx_large_cap][i], tmp_alloc_size, 6, traits_lcap_readonly_linear,  1);
                        h2m_update_traits(matrices_b[idx_large_cap][i], tmp_alloc_size, 7, traits_lcap_readonly_strided, 1);
                        h2m_update_traits(matrices_c[idx_large_cap][i], tmp_alloc_size, 6, traits_lcap_readwrite_linear, 1);
                    }
                    for(int i = 0; i < num_tasks; i++) {
                        h2m_update_traits(matrices_a[idx_hbm][i], tmp_alloc_size, 6, traits_hbm_readonly_linear,   1);
                        h2m_update_traits(matrices_b[idx_hbm][i], tmp_alloc_size, 7, traits_hbm_readonly_strided,  1);
                        h2m_update_traits(matrices_c[idx_hbm][i], tmp_alloc_size, 6, traits_hbm_readwrite_linear,  1);
                    }
                    h2m_apply_migration();
                }

                // ========== Phase Execution ==========
                #pragma omp master
                printf("-- Phase %d\n", p);

                double** ptr_a = matrices_a[p];
                double** ptr_b = matrices_b[p];
                double** ptr_c = matrices_c[p];

                #pragma omp for
                for(int i = 0; i < num_tasks; i++) {
                    compute_dot_prod(ptr_a[i], ptr_b[i], ptr_c[i], matrix_size);
                }
            }

#if ITERATIVE_VERSION
        }
#endif // ITERATIVE_VERSION
    }
    t_elapsed = omp_get_wtime() - t_elapsed;
    printf("Data allocation took %.5f sec\n", t_elapsed_alloc);
    printf("Computations took %.5f sec\n", t_elapsed);
    
    bool pass = true;
    if(num_tasks > 0) {
        for(int p = 0; p < N_PHASES; p++) {
            for(int t = 0; t < num_tasks; t++) {
                pass &= check_matrix(matrices_c[p][t], t, matrix_size, 42.0*42.0*matrix_size);
            }
        }
        if(pass) printf("Validation: TEST SUCCESS\n");
        else     printf("Validation: TEST FAILED\n");
    }

    // cleanup
    for(int p = 0; p < N_PHASES; p++) {
        for(int i = 0; i < num_tasks; i++) {
            h2m_free(matrices_a[p][i]);
            h2m_free(matrices_b[p][i]);
            h2m_free(matrices_c[p][i]);
        }
        delete[] matrices_a[p];
        delete[] matrices_b[p];
        delete[] matrices_c[p];
    }

    delete[] matrices_a;
    delete[] matrices_b;
    delete[] matrices_c;

    free(init_args_0);
    free(init_args_42);

    #pragma omp parallel
    {
        h2m_thread_finalize();
    }
    h2m_finalize();

    return 0;
}