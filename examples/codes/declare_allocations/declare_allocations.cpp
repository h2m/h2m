#include "h2m.h"
#include "h2m_pre_init.h"
#include "h2m_utils_numa.h"

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <inttypes.h>
#include <iostream>
#include <math.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/syscall.h>
#include <map>

#ifndef N_BUFFS
#define N_BUFFS 20
#endif

int main(int argc, char **argv)
{
    // library initialization
    #pragma omp parallel
    {
        h2m_thread_init();
    }

    int err;
    double t_elapsed_alloc = omp_get_wtime();
    double **buffers = new double*[N_BUFFS];
    size_t base_size = 1*1000*1000*sizeof(double);
    srand(42);

    // declare allocations now
    std::vector<h2m_declaration_t> list_declarations;
    for(int i = 0; i < N_BUFFS; i++) {
        // create and verity traits for allocation
        h2m_alloc_trait_t tmp_traits[2] = {
            h2m_atk_access_prio, {.dbl = ((double)rand())/RAND_MAX},
            h2m_atk_pref_mem_space, h2m_atv_mem_space_hbw
        };
        err = h2m_verify_traits(2, tmp_traits);
        if(err != H2M_SUCCESS) {
            fprintf(stderr, "Error: Trait sets for H2M strategy / heuristic do not fulfill the necessary requiremnets.\n");
        }

        // declare single allocation
        list_declarations.push_back(h2m_declare_alloc_w_traits((void **)&(buffers[i]), base_size*(i+1), nullptr, nullptr, &err, 2, tmp_traits));
    }
    
    // commit allocations now (and potentially also initialize data)
    err = h2m_commit_allocs(&(list_declarations[0]), list_declarations.size());
    
    // free declarations again
    for(int i = 0; i < list_declarations.size(); i++) {
        h2m_declaration_free(list_declarations[i]);
    }
    list_declarations.clear();

    // init data
    #pragma omp parallel for schedule(static)
    for(int i = 0; i < N_BUFFS; i++) {
        memset(buffers[i], 0, base_size*(i+1));
    }

    // check where data has been allocated in the end
    int found;
    printf("Resulting placement:\n");
    for(int i = 0; i < N_BUFFS; i++) {
        h2m_alloc_info_t info = h2m_get_alloc_info(buffers[i], &found);
        if(!found) {
            fprintf(stderr, "Error: Alloc info not found for %p\n", buffers[i]);
        }
        char str_space[64];
        char str_partition[64];
        h2m_alloc_trait_value_t_str(info.cur_mem_space, str_space);
        h2m_alloc_trait_value_t_str(info.cur_mem_partition, str_partition);
        printf("%p, size=%11zu, prio=%1.6f, mem_space=%30s,\tmem_partition=%s,\tnodemask=%lu\n", buffers[i], info.size, info.traits[0].value.dbl, str_space, str_partition, info.nodemask);
    }
    
    // cleanup
    for(int i = 0; i < N_BUFFS; i++) {
        h2m_free(buffers[i]);
    }
    delete[] buffers;

    #pragma omp parallel
    {
        h2m_thread_finalize();
    }
    h2m_finalize();

    return 0;
}