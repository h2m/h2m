program main
    ! load the H2M module that links to the C interface
    use h2m_lib

    ! declare the err variable
    integer :: ierr

    ! initialize H2M library
    ierr = h2m_init()

    ! add a phase transition
    ierr = h2m_phase_transition("My awesome phase transition")

    ! finalize H2M library
    ierr = h2m_finalize()
end program main