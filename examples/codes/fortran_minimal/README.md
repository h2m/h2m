# Minimal Example for H2M integration into Fortran that allows triggering Phase Transitions
- Requirements
  - `LD_LIBRARY_PATH` and `LIBRARY_PATH` need to be extended to find `libh2m.so`
  - Set the custom variable `H2M_INCLUDE_DIR` that points to your H2M installation where header files and Fortran module are located. Reason: Some Fortran compilers such as `gfortran` require to explicitly set include path during compilation with `-I`. Others might search for module files in directories specified with e.g., `CPATH` (Example: `ifort`)
  - Then execute the following:
    ```bash
    export H2M_INCLUDE_DIR=path-to-h2m-include
    make
    ./fortran_h2m.exe
    ```