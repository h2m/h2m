# Testing Allocation with Traits defined in JSON file
- Make sure you loaded all required modules and H2M is available
- Run the following
```bash
# build executable first
make
# specify JSON file when running
H2M_JSON_TRAIT_FILE="$(pwd)/sample_traits.json" ./allocation_traits_file.exe
```