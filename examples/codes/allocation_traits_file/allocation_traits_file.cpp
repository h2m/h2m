#include <stdio.h>
#include <stdlib.h>

#include "h2m.h"
#include "h2m_utils_numa.h"

int main(int argc, char* argv[]) {
    int err;
    size_t arr_size = sizeof(double) * 10000000;
    double **buffers = new double*[3];
    int* object_ids = new int[3] {4, 5, 6};

    // Init
    err = h2m_init();
    printf("Testing allocation traits from JSON file for H2M version %s\n", h2m_get_version());

    for(int i = 0; i < 3; i++) {
        int tmp_id = object_ids[i];
        double* tmp_ptr = (double*) h2m_alloc_w_traits_file(arr_size, tmp_id, &err);
        if (err != H2M_SUCCESS) printf("Error (alloc): %d\n", err);
        memset(tmp_ptr, 0, arr_size);
        buffers[i] = tmp_ptr;
    }
    
    // check where data has been allocated in the end
    int found;
    printf("Resulting placement:\n");
    for(int i = 0; i < 3; i++) {
        h2m_alloc_info_t info = h2m_get_alloc_info(buffers[i], &found);
        if(!found) {
            fprintf(stderr, "Error: Alloc info not found for %p\n", buffers[i]);
        }
        char str_space[64];
        char str_partition[64];
        h2m_alloc_trait_value_t_str(info.cur_mem_space, str_space);
        h2m_alloc_trait_value_t_str(info.cur_mem_partition, str_partition);
        printf("object_id=%d, addr=%p, size=%11zu, mem_space=%30s,\tmem_partition=%s,\tnodemask=%lu\n", object_ids[i], buffers[i], info.size, str_space, str_partition, info.nodemask);

        unsigned long nodemask = h2m_get_nodemask_for_mem_space(info.cur_mem_space);
        verify_page_locations((void**)&(buffers[i]), 1, arr_size, nodemask, "Test", 1);
    }
    
    // cleanup
    for(int i = 0; i < 3; i++) {
        err = h2m_free(buffers[i]);
        if (err != H2M_SUCCESS) printf("Error (free): %d\n", err);
    }
    
    // Finialize
    err = h2m_finalize();

    return 0;
}