#include <stdio.h>
#include <stdlib.h>

#include "h2m.h"
#include "h2m_utils_numa.h"

int main(int argc, char* argv[]) {
    int err;
    double* ptr;
    size_t arr_size = sizeof(double) * 10000000;

    h2m_alloc_trait_value_t mem_space = h2m_atv_mem_space_large_cap;
    unsigned long nodemask;
    int num_nodes; 

    // Init
    err = h2m_init();
    printf("Testing allocation traits for H2M version %s\n", h2m_get_version());
    
    printf("========================================\n");
    printf("=== Test 1: No traits passed:\n");
    printf("========================================\n");
    ptr = (double*) h2m_alloc(arr_size, &err);
    if (err != H2M_SUCCESS) printf("Error (alloc): %d\n", err);
    memset(ptr, 0, arr_size);
    err = h2m_free(ptr);
    if (err != H2M_SUCCESS) printf("Error (free): %d\n", err);

    printf("========================================\n");
    printf("=== Test 2: Empty traits passed:\n");
    printf("========================================\n");
    ptr = (double*) h2m_alloc_w_traits(arr_size, &err, 0, {});
    if (err != H2M_SUCCESS) printf("Error (alloc): %d\n", err);
    memset(ptr, 0, arr_size);
    err = h2m_free(ptr);
    if (err != H2M_SUCCESS) printf("Error (free): %d\n", err);

    printf("========================================\n");
    printf("=== Test 3: mem space requested:\n");
    printf("========================================\n");
    h2m_alloc_trait_t traits_a[1] = {h2m_atk_req_mem_space, mem_space};
    printf("size_traits=%ld, size_data_type=%ld\n", sizeof(traits_a), sizeof(h2m_alloc_trait_t));
    ptr = (double*) h2m_alloc_w_traits(arr_size, &err, 1, traits_a);
    if (err != H2M_SUCCESS) printf("Error (alloc): %d\n", err);
    memset(ptr, 0, arr_size);
    nodemask = h2m_get_nodemask_for_mem_space(mem_space);
    verify_page_locations((void**)&ptr, 1, arr_size, nodemask, "Test 3", 1);
    err = h2m_free(ptr);
    if (err != H2M_SUCCESS) printf("Error (free): %d\n", err);

    printf("========================================\n");
    printf("=== Test 4: Complex trait set passed:\n");
    printf("========================================\n");
    h2m_alloc_trait_t traits_b[10] = {
        h2m_atk_req_mem_space, mem_space, 
        h2m_atk_mem_alignment, 4096,
        h2m_atk_sensitivity, h2m_atv_bw_sensitive | h2m_atv_lat_sensitive,
        h2m_atk_access_mode, h2m_atv_access_mode_readonly,
        h2m_atk_access_origin, h2m_atv_access_origin_multi_threaded,
        h2m_atk_access_freq, h2m_atv_access_freq_medium,
        h2m_atk_access_prio, 42,
        h2m_atk_access_pattern, h2m_atv_access_pattern_strided,
        h2m_atk_access_stride, 2,
        h2m_atk_structure_type, h2m_atv_structure_type_array
        };
    printf("size_traits=%ld, size_data_type=%ld\n", sizeof(traits_b), sizeof(h2m_alloc_trait_t));
    ptr = (double*) h2m_alloc_w_traits(arr_size, &err, 10, traits_b);
    if (err != H2M_SUCCESS) printf("Error (alloc): %d\n", err);
    memset(ptr, 0, arr_size);
    nodemask = h2m_get_nodemask_for_mem_space(mem_space);
    verify_page_locations((void**)&ptr, 1, arr_size, nodemask, "Test 4", 1);
    err = h2m_free(ptr);
    if (err != H2M_SUCCESS) printf("Error (free): %d\n", err);
    
    // Finialize
    err = h2m_finalize();

    return 0;
}