#include <h2m.h>
#include <h2m_tools.h>

#include <unistd.h>
#include <sys/syscall.h>
#include <assert.h>

static h2m_tool_set_callback_t              func_set_callback;
static h2m_tool_get_callback_t              func_get_callback;
static h2m_tool_get_thread_data_t           func_get_thread_data;
static h2m_tool_get_capacity_usage_info_t   func_get_capacity_usage_info;
static h2m_tool_get_mem_characteristics_t   func_get_mem_characteristics;

void print_mem_lookup(h2m_memory_characteristics_t ch, const double** matrix, const char* type, const char* mode) {
    if (matrix) {
        printf("\n=== %s Matrix (%s):\n", type, mode);
        for(int c = 0; c < ch.num_cpu_nodes; c++) {
            // print header
            printf("CPU-Node-%d\n\t", c);
            for(int i = 0; i < ch.num_mem_nodes; i++) {
                printf("Mem-Node-%d\t", i);
            }
            printf("\n");
            // matrix
            for(int i = 0; i < ch.num_threads; i++) {
                printf("N_Thr=%02d\t", i+1);
                for(int j = 0; j < ch.num_mem_nodes; j++) {
                    printf("%.3f\t", matrix[c][i * ch.num_mem_nodes + j]);
                }
                printf("\n");
            }
            printf("\n");
        }
    }
}

void print_mem_characteristics(h2m_memory_characteristics_t ch) {
    print_mem_lookup(ch, (const double**) ch.matrix_bw_read_only,   "Bandwidth", "read-only");
    print_mem_lookup(ch, (const double**) ch.matrix_bw_read_write,  "Bandwidth", "read-write");
    print_mem_lookup(ch, (const double**) ch.matrix_lat_read_only,  "Latency", "read-only");
    print_mem_lookup(ch, (const double**) ch.matrix_lat_read_write, "Latency", "read-write");
}

static void
on_callback_thread_init(h2m_tool_data_t *thread_data) 
{
    thread_data->value = syscall(SYS_gettid);
    printf("on_callback_thread_init ==> thread_id=%d\n", thread_data->value);
}

static void
on_callback_post_init_serial(h2m_tool_data_t *thread_data) 
{
    printf("on_callback_post_init_serial ==> thread_id=%d\n", thread_data->value);
}

static void
on_callback_thread_finalize(h2m_tool_data_t *thread_data) 
{
    printf("on_callback_thread_finalize ==> thread_id=%d\n", thread_data->value);
}

static int
on_callback_verify_traits(
    int n_traits, 
    const h2m_alloc_trait_t traits[]) 
{
    h2m_tool_data_t *thread_data  = func_get_thread_data();
    printf("on_callback_verify_traits ==> thread_data=" DPxMOD ";n_traits=%d;traits=" DPxMOD "\n", DPxPTR(thread_data), n_traits, DPxPTR(traits));
    return H2M_SUCCESS;
}

static void
on_callback_declare_alloc(
    void** ptr, 
    size_t size, 
    h2m_fcn_data_init_t fcn_data_init, 
    void *fcn_args, 
    int n_traits, 
    const h2m_alloc_trait_t traits[]) 
{
    h2m_tool_data_t *thread_data  = func_get_thread_data();
    printf("on_callback_declare_alloc ==> thread_data=" DPxMOD ";ptr=" DPxMOD ";size=%zu;fcn_data_init=" DPxMOD ";fcn_args=" DPxMOD ";n_traits=%d;traits=" DPxMOD "\n", DPxPTR(thread_data), DPxPTR(*ptr), size, DPxPTR(fcn_data_init), DPxPTR(fcn_args), n_traits, DPxPTR(traits));
}

static void
on_callback_commit_allocs(
    const h2m_declaration_t handles[],
    size_t n_handles)
{
    h2m_tool_data_t *thread_data  = func_get_thread_data();
    printf("on_callback_commit_allocs ==> thread_data=" DPxMOD ";handles=" DPxMOD ";n_handles=%zu\n", DPxPTR(thread_data), DPxPTR(handles), n_handles);
}

static void
on_callback_alloc(
    size_t size, 
    int n_traits, 
    const h2m_alloc_trait_t traits[]) 
{
    int i;
    h2m_tool_data_t *thread_data  = func_get_thread_data();
    printf("on_callback_alloc ==> thread_data=" DPxMOD ";size=%zu;n_traits=%d;traits=" DPxMOD "\n", DPxPTR(thread_data), size, n_traits, DPxPTR(traits));

    // ============================================================
    // ===== Example how to access per trait information
    // ============================================================
    if(traits) {
        printf("on_callback_alloc ==> %d traits:\n", n_traits);
        for(i = 0; i < n_traits; i++) {
            h2m_alloc_trait_key_t key = traits[i].key;
            if (key == h2m_atk_mem_alignment || key == h2m_atk_access_prio || key == h2m_atk_access_stride) {
                int val = traits[i].value.i;
                printf("on_callback_alloc ==> %s = %d\n", h2m_alloc_trait_key_t_str[key], val);
            } else {
                char val_str[255];
                h2m_alloc_trait_value_t_str(traits[i].value.atv, val_str);
                printf("on_callback_alloc ==> %s = %s\n", h2m_alloc_trait_key_t_str[key], val_str);
            }
        }
    }
    // ============================================================
}

static void
on_callback_alloc_on_nodes(
    size_t size, 
    h2m_placement_decision_t pl,
    unsigned long nodemask,
    int n_traits, 
    const h2m_alloc_trait_t traits[])
{
    int i;
    h2m_tool_data_t *thread_data  = func_get_thread_data();

    char str_mem_space[50];
    char str_mem_partition[50];
    h2m_alloc_trait_value_t_str(pl.mem_space, str_mem_space);
    h2m_alloc_trait_value_t_str(pl.mem_partition, str_mem_partition);
    const char *str_mem_prio = h2m_alloc_trait_key_t_str[pl.mem_prio];

    printf("on_callback_alloc_on_nodes ==> thread_data=" DPxMOD ";size=%zu;pl(alignment=%zu, mem_space=%s, mem_prio=%s, mem_partition=%s);nodemask=%lu,n_traits=%d;traits=" DPxMOD "\n", DPxPTR(thread_data), size, pl.alignment, str_mem_space, str_mem_prio, str_mem_partition, nodemask, n_traits, DPxPTR(traits));
}

static void
on_callback_free(void* ptr)
{
    printf("on_callback_free ==> ptr=" DPxMOD "\n", DPxPTR(ptr));
}

static void
on_callback_update_traits(
    void *ptr, 
    size_t size, 
    int n_traits, 
    const h2m_alloc_trait_t traits[])
{
    printf("on_callback_update_traits ==> ptr=" DPxMOD ";size=%zu;n_traits=%d;traits=" DPxMOD "\n", DPxPTR(ptr), size, n_traits, DPxPTR(traits));
}

static void
on_callback_apply_migration(h2m_tool_migration_status_t status)
{
    printf("on_callback_apply_migration ==> status=%s\n", h2m_tool_migration_status_t_str[status]);
}

static void
on_callback_apply_migration_alloc(
    void** allocations,
    int n_allocs,
    h2m_tool_migration_status_t status)
{
    printf("on_callback_apply_migration_alloc ==> allocations=" DPxMOD ";n_allocs=%d;status=%s\n", DPxPTR(allocations), n_allocs, h2m_tool_migration_status_t_str[status]);
}

// TODO/FIXME: implement missing async migration callbacks

static h2m_placement_decision_t
on_callback_allocation_strategy(
    size_t size, 
    int n_traits, 
    const h2m_alloc_trait_t traits[],
    int *out_err)
{
    printf("on_callback_allocation_strategy ==> size=%zu;n_traits=%d;traits=" DPxMOD "\n", size, n_traits, DPxPTR(traits));

    // get default values and respect prescriptive traits
    h2m_placement_decision_t pl = h2m_get_default_placement();
    // Note: insert your code here to customize the placement decision based on traits
    pl = h2m_apply_prescriptive_traits(pl, n_traits, traits);

    *out_err = H2M_SUCCESS;
    return pl;
}

static h2m_alloc_order_entry_t*
on_callback_commit_strategy(
    const h2m_declaration_t handles[],
    size_t n_handles,
    int *out_err)
{
    h2m_tool_data_t *thread_data  = func_get_thread_data();
    printf("on_callback_commit_strategy ==> thread_data=" DPxMOD ";handles=" DPxMOD ";n_handles=%zu\n", DPxPTR(thread_data), DPxPTR(handles), n_handles);
    *out_err = H2M_SUCCESS;
    // default: use FIFO strategy for ordering allocations
    h2m_alloc_order_entry_t* ret = (h2m_alloc_order_entry_t*) malloc(sizeof(h2m_alloc_order_entry_t)*n_handles);
    for(int i = 0; i < n_handles; i++) {
        h2m_declaration_t h = handles[i];
        ret[i].decl = h;
        ret[i].pl   = h2m_call_allocation_strategy(h->size, h->n_traits, h->traits, out_err);
        if(*out_err != H2M_SUCCESS) {
            return ret;
        }
    }
    *out_err = H2M_SUCCESS;
    return ret;
}

static h2m_migration_decision_t*
on_callback_migration_strategy(int n_ele, const h2m_alloc_info_t *elements, int* out_n_decisions)
{
    printf("on_callback_migration_strategy ==> n_ele=%d;elements=" DPxMOD "\n", n_ele, DPxPTR(elements));

    if(n_ele <= 0) {
        *out_n_decisions = 0;
        return NULL;
    }

    h2m_migration_decision_t *decisions = (h2m_migration_decision_t *) malloc(sizeof(h2m_migration_decision_t) * n_ele);
    int ctr = 0;

    // get current resource usage and limits (your algorithm might need to consider that)
    h2m_capacity_usage_info_t cap_usage = func_get_capacity_usage_info();

    for(int i = 0; i < n_ele; i++) {
        // only if traits define memory space
        int mem_traits_found = 0;
        h2m_placement_decision_t pl = h2m_get_default_placement();

        for (int k = 0; k < elements[i].n_traits; k++) {
            if (elements[i].traits[k].key == h2m_atk_req_mem_space || elements[i].traits[k].key == h2m_atk_pref_mem_space) {
                pl.mem_space        = elements[i].traits[k].value.atv;
                pl.mem_prio         = elements[i].traits[k].key;
                mem_traits_found    = 1;
                continue;
            }
            if (elements[i].traits[k].key == h2m_atk_mem_partition) {
                pl.mem_partition    = elements[i].traits[k].value.atv;
                if (!mem_traits_found) {
                    pl.mem_space    = elements[i].cur_mem_space;
                }
                mem_traits_found    = 1;
                continue;
            }
        }

        if (mem_traits_found) {
            if (elements[i].cur_mem_space != pl.mem_space || 
                elements[i].cur_mem_partition != pl.mem_partition) {

                // TODO/FIXME: alter order to be exploit capacity usage information properly
                // 1. consider movements to LARGE_CAP
                // 2. consider movements to LOW_LAT or HBW (which might be limited)
                
                decisions[ctr].alloc_info   = elements[i];
                decisions[ctr].placement    = pl;
                ctr++;
            }
        }
    }
    
    *out_n_decisions = ctr;
    if(ctr) {
        return decisions;
    } else {
        free(decisions);
        return NULL;
    }
}

static void
on_callback_move_memory_mbind(
    const h2m_movement_info_t *m_info,
    int n_movements,
    h2m_tool_movement_status_t status)
{
    printf("on_callback_move_memory_mbind ==> m_info=" DPxMOD ";n_movements=%d;status=%s\n", DPxPTR(m_info), n_movements, h2m_tool_movement_status_t_str[status]);
}

static void
on_callback_move_memory_movepages(
    const h2m_movement_info_t *m_info,
    int n_movements,
    h2m_tool_movement_status_t status)
{
    printf("on_callback_move_memory_movepages ==> m_info=" DPxMOD ";n_movements=%d;status=%s\n", DPxPTR(m_info), n_movements, h2m_tool_movement_status_t_str[status]);
}

#define register_callback_t(name, type)                                         \
do{                                                                             \
    type f_##name = &on_##name;                                                 \
    if (func_set_callback(h2m_##name, (h2m_tool_callback_t)f_##name) != h2m_tool_set_always)   \
        printf("0: Could not register callback '" #name "'\n");                 \
} while(0)

#define register_callback(name) register_callback_t(name, h2m_tool_##name##_t)

int tool_initialize(
    h2m_tool_function_lookup_t lookup,
    h2m_tool_data_t *tool_data)
{
    func_set_callback               = (h2m_tool_set_callback_t)             lookup("h2m_tool_set_callback");
    func_get_callback               = (h2m_tool_get_callback_t)             lookup("h2m_tool_get_callback");
    func_get_thread_data            = (h2m_tool_get_thread_data_t)          lookup("h2m_tool_get_thread_data");
    func_get_capacity_usage_info    = (h2m_tool_get_capacity_usage_info_t)  lookup("h2m_tool_get_capacity_usage_info");
    func_get_mem_characteristics    = (h2m_tool_get_mem_characteristics_t)  lookup("h2m_tool_get_mem_characteristics");

    register_callback(callback_thread_init);
    register_callback(callback_post_init_serial);
    register_callback(callback_thread_finalize);
    register_callback(callback_verify_traits);
    register_callback(callback_declare_alloc);
    register_callback(callback_commit_allocs);
    register_callback(callback_alloc);
    register_callback(callback_alloc_on_nodes);
    register_callback(callback_update_traits);
    register_callback(callback_free);
    register_callback(callback_apply_migration);
    register_callback(callback_apply_migration_alloc);
    register_callback(callback_allocation_strategy);
    register_callback(callback_commit_strategy);
    register_callback(callback_migration_strategy);
    register_callback(callback_move_memory_mbind);
    register_callback(callback_move_memory_movepages);

    // =====================================================================
    // ===== Example how to access memory characteristics (if available)
    // =====================================================================
    // h2m_memory_characteristics_t ch = func_get_mem_characteristics();
    // print_mem_characteristics(ch);

    return 1; //success
}

void tool_finalize(h2m_tool_data_t *tool_data)
{
    printf("0: tool_finalize\n");
}

#ifdef __cplusplus
extern "C" {
#endif
h2m_tool_start_result_t* h2m_tool_start(const char *version)
{
    printf("Starting tool with H2M version: %s\n", version);
    static h2m_tool_start_result_t result = {&tool_initialize, &tool_finalize, 0};
    return &result;
}
#ifdef __cplusplus
}
#endif
