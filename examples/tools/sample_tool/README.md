# Sample Tool

* Implements all callbacks that the H2M Tools Interface provides

## Notes

* To run this tool it is necessary to compile H2M with `TOOL_SUPPORT=1`
* Execute `export H2M_TOOL_LIBRARIES=/path/to/tool.so` to enable load of the tool at runtime

