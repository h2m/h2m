#include <h2m.h>
#include <h2m_tools.h>

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/syscall.h>
#include <unistd.h>

static h2m_tool_set_callback_t              func_set_callback;
static h2m_tool_get_callback_t              func_get_callback;
static h2m_tool_get_thread_data_t           func_get_thread_data;
static h2m_tool_get_capacity_usage_info_t   func_get_capacity_usage_info;

int cmp_by_size(const void *a, const void *b) {
    const h2m_declaration_t *first  = (h2m_declaration_t *)a;
    const h2m_declaration_t *second = (h2m_declaration_t *)b;
    return (*first)->size < (*second)->size;
}

static h2m_alloc_order_entry_t*
on_callback_commit_strategy(
    const h2m_declaration_t handles[],
    size_t n_handles,
    int *out_err)
{
    // get current capacity usage and limit once
    h2m_capacity_usage_info_t usage = func_get_capacity_usage_info();

    // copy pointer array and sort
    h2m_declaration_t* cpy_handles = (h2m_declaration_t*) malloc(sizeof(h2m_declaration_t)*n_handles);
    memcpy(cpy_handles, handles, sizeof(h2m_declaration_t)*n_handles);
    qsort(cpy_handles, n_handles, sizeof(h2m_declaration_t), cmp_by_size);

    // === DEBUG ===
    // printf("Original Handles:\n");
    // for(int i = 0; i < n_handles; i++) {
    //     printf("Idx=%5d; handles=" DPxMOD "; size=%zu\n", i, DPxPTR(handles[i]), handles[i]->size);
    // }
    // printf("Sorting Result:\n");
    // for(int i = 0; i < n_handles; i++) {
    //     printf("Idx=%5d; cpy_handles=" DPxMOD "; size=%zu\n", i, DPxPTR(cpy_handles[i]), cpy_handles[i]->size);
    // }
    // === DEBUG ===

    h2m_alloc_order_entry_t* ret = (h2m_alloc_order_entry_t*) malloc(sizeof(h2m_alloc_order_entry_t)*n_handles);
    for(int i = 0; i < n_handles; i++) {
        h2m_declaration_t h = cpy_handles[i];
        ret[i].decl = h;
        ret[i].pl = h2m_call_allocation_strategy(h->size, h->n_traits, h->traits, out_err);
    }

    // cleanup memory again
    free(cpy_handles);

    *out_err = H2M_SUCCESS;
    return ret;
}

#define register_callback_t(name, type)                                         \
do{                                                                             \
    type f_##name = &on_##name;                                                 \
    if (func_set_callback(h2m_##name, (h2m_tool_callback_t)f_##name) != h2m_tool_set_always)   \
        printf("0: Could not register callback '" #name "'\n");                 \
} while(0)

#define register_callback(name) register_callback_t(name, h2m_tool_##name##_t)

int tool_initialize(
    h2m_tool_function_lookup_t lookup,
    h2m_tool_data_t *tool_data)
{
    func_set_callback               = (h2m_tool_set_callback_t)             lookup("h2m_tool_set_callback");
    func_get_callback               = (h2m_tool_get_callback_t)             lookup("h2m_tool_get_callback");
    func_get_thread_data            = (h2m_tool_get_thread_data_t)          lookup("h2m_tool_get_thread_data");
    func_get_capacity_usage_info    = (h2m_tool_get_capacity_usage_info_t)  lookup("h2m_tool_get_capacity_usage_info");

    register_callback(callback_commit_strategy);

    return 1; //success
}

void tool_finalize(h2m_tool_data_t *tool_data)
{
    printf("0: tool_finalize\n");
}

#ifdef __cplusplus
extern "C" {
#endif
h2m_tool_start_result_t* h2m_tool_start(const char *version)
{
    printf("Starting tool (commit-strategy_size) with H2M version: %s\n", version);
    static h2m_tool_start_result_t result = {&tool_initialize, &tool_finalize, 0};
    return &result;
}
#ifdef __cplusplus
}
#endif
